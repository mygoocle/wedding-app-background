define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sdcmarry/carousel/index' + location.search,
                    add_url: 'sdcmarry/carousel/add',
                    edit_url: 'sdcmarry/carousel/edit',
                    del_url: 'sdcmarry/carousel/del',
                    multi_url: 'sdcmarry/carousel/multi',
                    import_url: 'sdcmarry/carousel/import',
                    table: 'sdcmarry_carousel',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'category', title: __('Category_id'),
                            formatter:function (value,row,index){
                                console.log(row)
                                return row.category_text
                            },
                            addclass: 'selectpage',
                            extend: '  data-source="sdcmarry/carousel/selectpage" data-primary-key="key" data-field="value"'
                        },
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'url', title: __('Url'), operate: 'LIKE', formatter: Table.api.formatter.url},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});