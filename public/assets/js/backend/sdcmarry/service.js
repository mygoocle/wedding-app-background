define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sdcmarry/service/index' + location.search,
                    add_url: 'sdcmarry/service/add',
                    edit_url: 'sdcmarry/service/edit',
                    del_url: 'sdcmarry/service/del',
                    multi_url: 'sdcmarry/service/multi',
                    import_url: 'sdcmarry/service/import',
                    table: 'sdcmarry_service',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'category', title: __('Category'), 
                            formatter:function (value,row,index){
                                return row.category_text
                            },
                            addclass: 'selectpage',
                            extend: '  data-source="sdcmarry/service/selectpage" data-primary-key="key" data-field="value"'
                        },
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'desc_images', title: __('Desc_images'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'description', title: __('Description'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                        {field: 'main_image', title: __('Main_image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'old_price', title: __('Old_price'), operate:'BETWEEN'},
                        {field: 'new_price', title: __('New_price'), operate:'BETWEEN'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});