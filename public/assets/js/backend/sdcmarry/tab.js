define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sdcmarry/tab/index' + location.search,
                    add_url: 'sdcmarry/tab/add',
                    edit_url: 'sdcmarry/tab/edit',
                    del_url: 'sdcmarry/tab/del',
                    multi_url: 'sdcmarry/tab/multi',
                    import_url: 'sdcmarry/tab/import',
                    table: 'sdcmarry_tab',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'category', title: __('Category_id'),
                            formatter:function (value,row,index){
                                return row.category_text
                            },
                            addclass: 'selectpage',
                            extend: '  data-source="sdcmarry/tab/selectpage" data-primary-key="key" data-field="value"'
                        },
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'path', title: __('Path'), operate: 'LIKE'},
                        {field: 'options', title: __('Options'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});