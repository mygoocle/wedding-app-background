define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sdcmarry/post/index' + location.search,
                    add_url: 'sdcmarry/post/add',
                    edit_url: 'sdcmarry/post/edit',
                    del_url: 'sdcmarry/post/del',
                    multi_url: 'sdcmarry/post/multi',
                    import_url: 'sdcmarry/post/import',
                    table: 'sdcmarry_post',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'category', title: __('Category'), operate: 'LIKE',
                            formatter: function (value,row,index){
                                return row.category_text
                            },
                            addclass: 'selectpage',
                            extend: 'data-source="sdcmarry/post/selectpage" data-primary-key="key" data-field="value"'
                        },
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'main_image', title: __('Main_image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'author', title: __('Author'), operate: 'LIKE'},
                        {field: 'is_post', title: __('Is_post'), searchList: {"0":__('Is_post 0'),"1":__('Is_post 1')}, formatter: Table.api.formatter.normal},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});