<?php

return [
    'autoload' => false,
    'hooks' => [
        'sms_send' => [
            'cmcctts',
        ],
        'sms_check' => [
            'cmcctts',
        ],
        'testhook' => [
            'mydemo',
            'sdcmarry',
        ],
        'config_init' => [
            'third',
        ],
    ],
    'route' => [
        '/third$' => 'third/index/index',
        '/third/connect/[:platform]' => 'third/index/connect',
        '/third/callback/[:platform]' => 'third/index/callback',
        '/third/bind/[:platform]' => 'third/index/bind',
        '/third/unbind/[:platform]' => 'third/index/unbind',
    ],
    'priority' => [],
];
