<?php

return [
    'Id'            => 'Id',
    'Category'      => '分类',
    'Title'         => '标题',
    'Content'       => '内容',
    'Main_image'    => '主图',
    'Swiper_images' => '多图',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间'
];
