<?php

return [
    'Id'          => 'Id',
    'Category'    => '服务分类',
    'Title'       => '标题',
    'Desc_images' => '多图介绍',
    'Content'     => '内容',
    'Description' => '简介',
    'Status'      => '状态',
    'Status 0'    => '隐藏',
    'Status 1'    => '正常',
    'Main_image'  => '主图',
    'Old_price'   => '原价',
    'New_price'   => '现价',
    'Createtime'  => '创建时间',
    'Updatetime'  => '创建时间'
];
