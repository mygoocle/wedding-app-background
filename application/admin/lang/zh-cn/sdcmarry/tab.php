<?php

return [
    'Id'          => 'Id',
    'Category_id' => '分类',
    'Title'       => '标题',
    'Image'       => '图标',
    'Path'        => '路径',
    'Options'     => '附加选项'
];
