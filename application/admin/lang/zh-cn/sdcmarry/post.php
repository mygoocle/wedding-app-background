<?php

return [
    'Id'          => 'Id',
    'Category'    => '分类',
    'Title'       => '标题',
    'Main_image'  => '主图',
    'Description' => '简介',
    'Author'      => '作者',
    'Content'     => '内容',
    'Is_post'     => '状态',
    'Is_post 0'   => '未发布',
    'Is_post 1'   => '已发布',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
