<?php

return [
    'Id'         => 'Id',
    'User_id'    => '用户id',
    'Service_id' => '服务id',
    'Title'      => '服务名',
    'Username'   => '姓名',
    'Mobile'     => '联系电话',
    'Orderno'    => '订单号',
    'Price' => '价格',
    'Ordertime'  => '预约时间',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
