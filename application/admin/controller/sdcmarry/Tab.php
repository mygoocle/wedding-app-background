<?php

namespace app\admin\controller\sdcmarry;

use addons\sdcmarry\model\Tab as ModelTab;
use app\common\controller\Backend;

/**
 * 标签导航
 *
 * @icon fa fa-circle-o
 */
class Tab extends Backend
{
    
    /**
     * Tab模型对象
     * @var \app\admin\model\sdcmarry\Tab
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelTab();

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function selectpage()
    {
       $config = get_addon_config('sdcmarry');
       $result = [];
       foreach($config['tab'] as $key=>$value){
           array_push($result,['key'=>$key,'value'=>$value]);
       }
       return json([
           'list' => $result,
           'total' => count($result)
       ]);
    }
}
