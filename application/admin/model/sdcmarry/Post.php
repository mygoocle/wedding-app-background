<?php

namespace app\admin\model\sdcmarry;

use think\Model;


class Post extends Model
{

    

    

    // 表名
    protected $name = 'sdcmarry_post';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_post_text'
    ];
    

    
    public function getIsPostList()
    {
        return ['0' => __('Is_post 0'), '1' => __('Is_post 1')];
    }


    public function getIsPostTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_post']) ? $data['is_post'] : '');
        $list = $this->getIsPostList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
