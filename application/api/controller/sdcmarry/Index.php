<?php
namespace app\api\controller\sdcmarry;

use app\common\controller\Api;
use think\Config;

class Index extends Api
{
    protected $noNeedRight = ['*'];
    protected $noLoginRight = ['*'];

    public function test()
    {
        return Config::get('sdcmarry.mp');
    }
}