<?php

namespace addons\sdcmarry\model;

use think\Model;


class Post extends Model
{

    

    

    // 表名
    protected $name = 'sdcmarry_post';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_post_text',
        'category_text',
        'main_image_cdn'
    ];

    public function getCategoryTextAttr($value,$data)
    {
        if(! isset($data['category'])){
            return $value;
        }
        $config = get_addon_config('sdcmarry');
        return isset($config['post'][$data['category']]) 
        ? $config['post'][$data['category']] : '-';
    }

    
    public function getIsPostList()
    {
        return ['0' => __('Is_post 0'), '1' => __('Is_post 1')];
    }


    public function getIsPostTextAttr($value, $data)
    {
        if(! isset($data['is_post'])){
            return $value;
        }
        $value = $value ? $value : (isset($data['is_post']) ? $data['is_post'] : '');
        $list = $this->getIsPostList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getMainImageCdnAttr($value,$data)
    {
        return $value ? $value : (isset($data['main_image']) ? cdnurl($data['main_image'],true) : '');
    }

}
