<?php

namespace addons\sdcmarry\model;

use think\Model;


class Example extends Model
{

    // 表名
    protected $name = 'sdcmarry_example';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'category_text',
        'main_image_cdn',
        'swiper_images_cdn'
    ];
    
    public function getCategoryTextAttr($value,$data)
    {
        if(! isset($data['category'])){
            return $value;
        }
        $config = get_addon_config('sdcmarry');
        return isset($config['example'][$data['category']]) 
        ? $config['example'][$data['category']] : '-';
    }
    
    public function getMainImageCdnAttr($value,$data)
    {
        return $value ? $value : (isset($data['main_image']) ? cdnurl($data['main_image'],true) : '');
    }


    public function getSwiperImagesCdnAttr($value,$data)
    {
        if(empty($data['swiper_images']))
            return $data['swiper_images'];
        $images = [];
        foreach(explode(',',$data['swiper_images']) as $item){
            array_push($images,cdnurl($item,true));
        }
        return $images;
    }



}
