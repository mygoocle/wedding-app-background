<?php

namespace addons\sdcmarry\model;

use think\Model;


class Service extends Model
{

    

    

    // 表名
    protected $name = 'sdcmarry_service';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'category_text',
        'main_image_cdn'
    ];
    

    
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getCategoryTextAttr($value,$data)
    {
        $config = get_addon_config('sdcmarry');
        return isset($config['service'][$data['category']]) 
        ? $config['service'][$data['category']] : '-';
    }

    public function getMainImageCdnAttr($value,$data)
    {
        if(empty($data['main_image'])){
            return '';
        }
        return cdnurl($data['main_image'],true);
    }

}
