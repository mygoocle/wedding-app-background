<?php

namespace addons\sdcmarry\model;

use app\common\model\Category;
use think\Model;


class Carousel extends Model
{

    // 表名
    protected $name = 'sdcmarry_carousel';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'category_text'
    ];

    public function getCategoryTextAttr($value,$data)
    {
        $config = get_addon_config('sdcmarry');
        return isset($config['carousel'][$data['category']]) 
        ? $config['carousel'][$data['category']] : '-';
    }

}
