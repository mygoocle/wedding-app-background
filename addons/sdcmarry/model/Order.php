<?php

namespace addons\sdcmarry\model;

use think\Model;


class Order extends Model
{

    

    

    // 表名
    protected $name = 'sdcmarry_order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'ordertime_text'
    ];
    

    



    public function getOrdertimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['ordertime']) ? $data['ordertime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setOrdertimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
