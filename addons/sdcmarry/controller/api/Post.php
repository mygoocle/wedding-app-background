<?php
namespace addons\sdcmarry\controller\api;

use addons\sdcmarry\model\Post as ModelPost;
use think\Validate;

class Post extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    private $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelPost();
    }

    public function index()
    {
        // 已发布的文章
        $where = [
            'is_post' => 1
        ];
        if($categoryType = $this->request->request('category')){
            $where['category'] = $categoryType;
        }
        $posts = $this->model
            ->where($where)
            ->field('content',true)
            ->order('id',"DESC")
            ->paginate();
        $this->success(__('Request success'),$posts);
    }

    public function show()
    {
        $id = $this->request->request('id');
        if(! Validate::checkRule($id,'require|number')){
            $this->error(__("Id param must required"));
        }
        $post = $this->model
            ->where(['id'=>$id,'is_post' => 1])
            ->find();
        if(! $post){
            $this->error(__('Not found post'));
        }
        $this->success(__('Request success'),$post);
    }
}