<?php
/**
 * 导航标签控制器
 */
namespace addons\sdcmarry\controller\api;

use addons\sdcmarry\model\Tab as ModelTab;
use think\Validate;

class Tab extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelTab();
    }
    
    public function index()
    {
        $category = $this->request->request('category');
        if(! Validate::checkRule($category,'require')){
            $this->error(__('Category must required'));
        }
        $data = $this->model
            ->where('category',$category)
            ->select();
        foreach($data as $key => $value){
            $data[$key]['image'] = cdnurl($value['image'],true);
        }
        $this->success(__('Request success'),$data);
    }
}