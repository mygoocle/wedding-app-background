<?php
namespace addons\sdcmarry\controller\api;

use addons\sdcmarry\model\Example as ModelExample;
use think\Validate;

// 案例控制器
class Example extends Api
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = ['*'];
    private $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelExample();
    }

    public function index()
    {
        // 已发布的文章
        $where = [
        ];
        if($categoryType = $this->request->request('category')){
            $where['category'] = $categoryType;
        }
        $examples = $this->model
            ->where($where)
            ->field('content',true)
            ->order('id',"DESC")
            ->paginate();
        $this->success(__('Request success'),$examples);
    }

    public function show()
    {
        $id = $this->request->request('id');
        if(! Validate::checkRule($id,'require|number')){
            $this->error(__("Id param must required"));
        }
        $example = $this->model
            ->where(['id'=>$id])
            ->find();
        if(! $example){
            $this->error(__('Not found post'));
        }
        $this->success(__('Request success'),$example);
    }
}