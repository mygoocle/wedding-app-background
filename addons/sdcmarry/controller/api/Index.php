<?php

namespace addons\sdcmarry\controller\api;

use app\common\controller\Api;
use think\Collection;

class Index extends Api
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    public function index()
    {
    }

    public function config()
    {
        $config = get_addon_config('sdcmarry');
        $only = ['tel','address','wechat','email','about_image'];
        $result = [];
        foreach($only as $item){
            if(array_key_exists($item,$config))
                $result[$item] = $config[$item];
        }
        $result['about_image'] = cdnurl($result['about_image'],true);
        $this->success(__('Request success'),$result);
    }

}
