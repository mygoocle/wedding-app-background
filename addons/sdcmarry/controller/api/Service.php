<?php
namespace addons\sdcmarry\controller\api;

use addons\sdcmarry\model\Service as ModelService;

class Service extends Api
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = ['*'];
    private $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelService();
    }

    public function index()
    {
        $result = $this->model
            ->where('status',1)
            ->paginate(5);
        $this->success(__('Request success'),$result);
    }
}