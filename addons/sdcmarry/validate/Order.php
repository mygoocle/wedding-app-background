<?php
namespace addons\sdcmarry\validate;

use think\Validate;

class Order extends Validate
{
    protected $rule = [
        'service_id' => 'require|number',
        'title' => 'require|max:90',
        'username' => 'require|max:10',
        'mobile' => 'require',
        'remark' => 'max:255',
    ];

    protected $message = [
        'remark.max' => '备注不能超过255个子'
    ];
}