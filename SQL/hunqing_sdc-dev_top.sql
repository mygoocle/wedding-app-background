-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 18, 2020 at 12:28 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hunqing_sdc-dev_top`
--

-- --------------------------------------------------------

--
-- Table structure for table `fa_admin`
--

CREATE TABLE `fa_admin` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员表';

--
-- Dumping data for table `fa_admin`
--

INSERT INTO `fa_admin` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`, `loginfailure`, `logintime`, `loginip`, `createtime`, `updatetime`, `token`, `status`) VALUES
(1, 'admin', 'Admin', 'd7020c4b95e5bdafa20268b0840f4c4b', '713890', '/assets/img/avatar.png', 'admin@admin.com', 0, 1602907403, '127.0.0.1', 1492186163, 1602907403, '75925402-0a35-4614-91b4-9d8cf2a61ef9', 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_admin_log`
--

CREATE TABLE `fa_admin_log` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '日志标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员日志表';

--
-- Dumping data for table `fa_admin_log`
--

INSERT INTO `fa_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(1, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php', 'Login', '{\"url\":\"\\/luKUFZaHjE.php\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"z3yg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601869156),
(2, 1, 'admin', '/luKUFZaHjE.php/addon/install', '插件管理', '{\"name\":\"cmcctts\",\"force\":\"0\",\"uid\":\"32430\",\"token\":\"***\",\"version\":\"1.0.1\",\"faversion\":\"1.2.0.20201001_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601906668),
(3, 1, 'admin', '/luKUFZaHjE.php/addon/install', '插件管理', '{\"name\":\"recharge\",\"force\":\"0\",\"uid\":\"32430\",\"token\":\"***\",\"version\":\"1.0.8\",\"faversion\":\"1.2.0.20201001_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601906727),
(4, 1, 'admin', '/luKUFZaHjE.php/addon/install', '插件管理', '{\"name\":\"command\",\"force\":\"0\",\"uid\":\"32430\",\"token\":\"***\",\"version\":\"1.0.6\",\"faversion\":\"1.2.0.20201001_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601906746),
(5, 1, 'admin', '/luKUFZaHjE.php/addon/install', '插件管理', '{\"name\":\"third\",\"force\":\"0\",\"uid\":\"32430\",\"token\":\"***\",\"version\":\"1.2.0\",\"faversion\":\"1.2.0.20201001_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601907193),
(6, 1, 'admin', '/luKUFZaHjE.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601907988),
(7, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"disable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601908300),
(8, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601908340),
(9, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"disable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601908342),
(10, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601908527),
(11, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"disable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601908612),
(12, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68', 1601909408),
(13, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php', '登录', '{\"url\":\"\\/luKUFZaHjE.php\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"xqe3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601953536),
(14, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601953575),
(15, 1, 'admin', '/luKUFZaHjE.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"categorytype\":\"{&quot;default&quot;:&quot;Default&quot;,&quot;page&quot;:&quot;Page&quot;,&quot;article&quot;:&quot;Article&quot;,&quot;Carousel&quot;:&quot;sdcmarry_carousel&quot;}\",\"configgroup\":\"{&quot;basic&quot;:&quot;Basic&quot;,&quot;email&quot;:&quot;Email&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;,&quot;user&quot;:&quot;User&quot;,&quot;example&quot;:&quot;Example&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957276),
(16, 1, 'admin', '/luKUFZaHjE.php/category/add?dialog=1', '分类管理 / 添加', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"type\":\"Carousel\",\"pid\":\"0\",\"name\":\"首页轮播图\",\"nickname\":\"首页轮播图\",\"image\":\"\",\"keywords\":\"\",\"description\":\"\",\"weigh\":\"0\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957601),
(17, 1, 'admin', '/luKUFZaHjE.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"categorytype\":\"{&quot;default&quot;:&quot;Default&quot;,&quot;page&quot;:&quot;Page&quot;,&quot;article&quot;:&quot;Article&quot;,&quot;sdcmarry_carousel&quot;:&quot;Carousel&quot;}\",\"configgroup\":\"{&quot;basic&quot;:&quot;Basic&quot;,&quot;email&quot;:&quot;Email&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;,&quot;user&quot;:&quot;User&quot;,&quot;example&quot;:&quot;Example&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957651),
(18, 1, 'admin', '/luKUFZaHjE.php/category/add?dialog=1', '分类管理 / 添加', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"type\":\"sdcmarry_carousel\",\"pid\":\"0\",\"name\":\"首页轮播图\",\"nickname\":\"首页轮播图\",\"image\":\"\",\"keywords\":\"\",\"description\":\"\",\"weigh\":\"0\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957685),
(19, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957717),
(20, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/carousel/add?dialog=1', 'sdcmarry / 轮播图 / 添加', '{\"dialog\":\"1\",\"row\":{\"category_id\":\"15\",\"title\":\"轮播图1\",\"image\":\"\\/uploads\\/20201006\\/497f4a60d1f89a443d4b4022606e282b.jpg\",\"url\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957720),
(21, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957729),
(22, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/carousel/add?dialog=1', 'sdcmarry / 轮播图 / 添加', '{\"dialog\":\"1\",\"row\":{\"category_id\":\"15\",\"title\":\"轮播图2\",\"image\":\"\\/uploads\\/20201006\\/e1d8c85d9804cc3a2f95777e6e5597f9.jpg\",\"url\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957744),
(23, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957762),
(24, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/carousel/add?dialog=1', 'sdcmarry / 轮播图 / 添加', '{\"dialog\":\"1\",\"row\":{\"category_id\":\"15\",\"title\":\"轮播图3\",\"image\":\"\\/uploads\\/20201006\\/f073e13efa01849d5e64f7ea47cc6269.jpg\",\"url\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601957764),
(25, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;首页轮播图&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601962407),
(26, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;首页轮播图&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;首页导航&quot;,&quot;test&quot;:&quot;测试导航&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601966709),
(27, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601970542),
(28, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"淘宝客\",\"image\":\"\\/uploads\\/20201006\\/00803bb295bc445ea7339ff474762829.jpg\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601970544),
(29, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"disable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601971060),
(30, 1, 'admin', '/luKUFZaHjE.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"sdcmarry\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601971130),
(31, 1, 'admin', '/luKUFZaHjE.php/addon/install', '插件管理', '{\"name\":\"summernote\",\"force\":\"0\",\"uid\":\"32430\",\"token\":\"***\",\"version\":\"1.0.4\",\"faversion\":\"1.2.0.20201001_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601987663),
(32, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997862),
(33, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/edit/ids/5?dialog=1', 'sdcmarry / 标签导航 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"摄像拍照\",\"image\":\"\\/uploads\\/20201006\\/69653bb996a590296507e4e1c45c9b5a.png\",\"path\":\"\",\"options\":\"\"},\"ids\":\"5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997865),
(34, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997893),
(35, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"现场布置\",\"image\":\"\\/uploads\\/20201006\\/12fa23594787473531b746cd26af98a9.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997895),
(36, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997916),
(37, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"婚车服饰\",\"image\":\"\\/uploads\\/20201006\\/bbf11d4a95d99120ad0ca6c04ae431fd.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997918),
(38, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997943),
(39, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"婚礼服装\",\"image\":\"\\/uploads\\/20201006\\/6be7a0b21482725b247ac7d0eaf07dce.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997945),
(40, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997967),
(41, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"最新动态\",\"image\":\"\\/uploads\\/20201006\\/aeff78cc2662833f583ec84ee2644afe.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997969),
(42, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997990),
(43, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"服务预约\",\"image\":\"\\/uploads\\/20201006\\/bc833438bebd09b84aad4d873f3caaaf.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601997993),
(44, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601998025),
(45, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"留言咨询\",\"image\":\"\\/uploads\\/20201006\\/94a7f5ff63784391ab5e90572c26051a.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601998031),
(46, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601998048),
(47, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"联系我们\",\"image\":\"\\/uploads\\/20201006\\/08aa32ce64e40aee2987c226d67034ad.png\",\"path\":\"\",\"options\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 1601998050),
(48, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', 'Login', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"cuax\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602209016),
(49, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212633),
(50, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212792),
(51, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/service/add?dialog=1', 'sdcmarry / 服务 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"main\",\"title\":\"爱的承诺-预约大礼包\",\"desc_images\":\"\\/uploads\\/20201009\\/8890254019c5c3b8c9e9c99ca4269dbc.jpg\",\"content\":\"123\",\"description\":\"场地布置、主持人、蛋糕婚车10辆\",\"status\":\"0\",\"main_image\":\"\\/uploads\\/20201009\\/8890254019c5c3b8c9e9c99ca4269dbc.jpg\",\"old_price\":\"2600\",\"new_price\":\"2000\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212826),
(52, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/service/edit/ids/1?dialog=1', 'sdcmarry / 服务 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"main\",\"title\":\"爱的承诺-预约大礼包\",\"desc_images\":\"\\/uploads\\/20201009\\/8890254019c5c3b8c9e9c99ca4269dbc.jpg\",\"content\":\"123\",\"description\":\"场地布置、主持人、蛋糕婚车10辆\",\"status\":\"1\",\"main_image\":\"\\/uploads\\/20201009\\/8890254019c5c3b8c9e9c99ca4269dbc.jpg\",\"old_price\":\"2600.00\",\"new_price\":\"2000.00\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212837),
(53, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212870),
(54, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212906),
(55, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/service/add?dialog=1', 'sdcmarry / 服务 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"main\",\"title\":\"教堂婚礼套餐\",\"desc_images\":\"\\/uploads\\/20201009\\/890dda29f738474d23f8a8ce10b2b056.jpg\",\"content\":\"123\",\"description\":\"场地布置、主持人、蛋糕婚车10辆\",\"status\":\"1\",\"main_image\":\"\\/uploads\\/20201009\\/890dda29f738474d23f8a8ce10b2b056.jpg\",\"old_price\":\"2000\",\"new_price\":\"1600\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212925),
(56, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212970),
(57, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212982),
(58, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/service/add?dialog=1', 'sdcmarry / 服务 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"main\",\"title\":\"豪华预约服务\",\"desc_images\":\"\\/uploads\\/20201009\\/fca931712e97cef40e68a99ffa142f80.jpg\",\"content\":\"123\",\"description\":\"场地布置、主持人、蛋糕婚车10辆\",\"status\":\"1\",\"main_image\":\"\\/uploads\\/20201009\\/fca931712e97cef40e68a99ffa142f80.jpg\",\"old_price\":\"3888\",\"new_price\":\"1888\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602212994),
(59, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602256559),
(60, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"2020年婚纱流行趋势 现在看还来得及！\",\"main_image\":\"\\/uploads\\/20201009\\/8bb2b2e75f47d7f9a696e512907177db.jpg\",\"description\":\"对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。\",\"author\":\"DanceLynx\",\"content\":\"对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。虽然婚纱看起来都是万变不离其宗，不管什么时候都是白白的，会有很多薄纱与蕾丝。但其实如果你仔细观察就会发现，表面看似相近的婚纱，其实每一件都有其独到的不同。今天就来为大家解读一下2020的婚纱流行趋势，照着这些款式和元素去挑选，你绝对是朋友圈里最酷的新娘！流行趋势之披风婚纱Monique Lhuillier Bridal其实将披风的设计运用到婚纱上，很多大牌婚纱都推出过带有披风的婚礼服。虽然款式不尽相同，但它们每一件都能让穿上它的新娘顿时气场骤升。Zuhair Murad披风婚纱也被设计为两种类型，一种是披肩式的拖地长披风，非常帅气。如果你是气质女王型，可以通过这种大披风来增强视觉存在感，让新娘整体气势更强。Pronovias不过在选择这种婚纱时，除了需要你强大的气场外，更重要的是要瘦!否则上身后宽宽的线条会让新娘的身材看上去无比魁梧，而这种形容词应该没有人会想用在自己的婚礼上吧。Celia Kritharioti In loveJenny Packham当然，如果你担心过长的披风会拉低你的比例，或者对自己身材不自信无法驾驭上面的婚纱，那么较短的或者材质轻薄的披风肯定能满足你的要求。\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602256564),
(61, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257556),
(62, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/edit/ids/1?dialog=1', 'sdcmarry / 文章 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"2020年婚纱流行趋势 现在看还来得及！\",\"main_image\":\"\\/uploads\\/20201009\\/6fb06c1b10982d5eae781f1748ada5fe.jpg\",\"description\":\"对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。\",\"author\":\"DanceLynx\",\"content\":\"对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。虽然婚纱看起来都是万变不离其宗，不管什么时候都是白白的，会有很多薄纱与蕾丝。但其实如果你仔细观察就会发现，表面看似相近的婚纱，其实每一件都有其独到的不同。今天就来为大家解读一下2020的婚纱流行趋势，照着这些款式和元素去挑选，你绝对是朋友圈里最酷的新娘！流行趋势之披风婚纱Monique Lhuillier Bridal其实将披风的设计运用到婚纱上，很多大牌婚纱都推出过带有披风的婚礼服。虽然款式不尽相同，但它们每一件都能让穿上它的新娘顿时气场骤升。Zuhair Murad披风婚纱也被设计为两种类型，一种是披肩式的拖地长披风，非常帅气。如果你是气质女王型，可以通过这种大披风来增强视觉存在感，让新娘整体气势更强。Pronovias不过在选择这种婚纱时，除了需要你强大的气场外，更重要的是要瘦!否则上身后宽宽的线条会让新娘的身材看上去无比魁梧，而这种形容词应该没有人会想用在自己的婚礼上吧。Celia Kritharioti In loveJenny Packham当然，如果你担心过长的披风会拉低你的比例，或者对自己身材不自信无法驾驭上面的婚纱，那么较短的或者材质轻薄的披风肯定能满足你的要求。\",\"is_post\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257557),
(63, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257619),
(64, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\",\"main_image\":\"\\/uploads\\/20201009\\/4dc1d65de0be85512daaa45f52c2e384.jpg\",\"description\":\"日前，网曝张若昀与唐艺昕将于6月26、27日在爱尔兰结婚。张若昀与唐艺昕将于本月底在国外举办婚礼，伴娘团由宋茜、沈梦辰等组成。随后张若昀父亲张健向媒体证实：张若昀唐艺昕月底办婚礼！\",\"author\":\"DanceLynx\",\"content\":\"日前，网曝张若昀与唐艺昕将于6月26、27日在爱尔兰结婚。张若昀与唐艺昕将于本月底在国外举办婚礼，伴娘团由宋茜、沈梦辰等组成。随后张若昀父亲张健向媒体证实：张若昀唐艺昕月底办婚礼！盼星星盼月亮，终于盼到这对情侣给我等吃瓜群众发糖啦！更重要的是，他们连结婚地点的选择都选择了不允许离婚的国家——爱尔兰。毕竟他们的爱情真的如爱尔兰的婚姻寓意一样，纵使外界言语纷纷，我仍爱你如初。公开撒糖的唯两次，一次公布恋情，一次结婚。2017年8月，张若昀发文高调公开恋情，表白唐艺昕：“时光赐给我们盗不走的爱人，而你赐给我时光。”并且附上唐艺昕亲吻自己侧脸的亲密照片。唐艺昕则甜蜜回复：“时光往复，爱你如初。”此前二人多次同行被拍，热爱后也多次大方公开撒糖，如今终于收成正果，恭喜恭喜！说起娱乐圈的情侣，张若昀唐艺昕这一对可以说是十分让人羡慕了！两人在一起这么多年感情一直很稳定，颜值也超高，并且在娱乐圈也很低调，不少朋友都说十分看好这对情侣。他俩人虽然是在2017年8月公开的恋情，但早在2011年张若昀就在微博上晒出关于“她”的备忘录，看来两人的恋情在那个时候就已确定，至今已经过去8年。月末临近，也就是快到了张若昀和唐艺昕结婚的时间，其实在去年的8月份，也就是差不多是他们相识的时间8月2号，有网友就曝出了，二人已经领证结婚，但是双方都没有给予正面回应，这次看来终于是石锤了。此次还有个亮点就是，此次的伴娘团是由宋茜和沈梦辰等人组成，宋茜和沈梦辰也是唐艺昕的好朋友，两人甜美形象和高颜值也将会成为婚礼的看点。对此有网友纷纷开始催婚两人，沈梦辰还好一点，因为沈梦辰毕竟和杜海涛已经传出了恋情。结婚喜讯也将提上日程，至于宋茜在婚姻上面就显得很安静了。网友开始疯狂催婚了宋茜了，这届网友真的很皮呢！说起来，唐艺昕跟张若昀的缘分还是来自于一个误会，张若昀曾在微博公开过与唐艺昕的相识经过，里面有些情节就像偶像剧一样，张若昀唐艺昕的遇见是在一个加油站，因为大雨张若昀的车在红绿灯不小心追尾唐艺昕的车。俩人的缘分就此展开，并且一跑就是八年时光。让彼此都对对方的一切了如指掌。如今他们的好事临近，相信婚礼也一定会很隆重。\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257654),
(65, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257688),
(66, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"年末节日大大增加情侣分手率，父母该“插手”孩子婚姻？\",\"main_image\":\"\\/uploads\\/20201009\\/325b6975698a34756cb4ce43ab4255e0.jpg\",\"description\":\"小可爱们，大家看了今天的热搜吗？#年末节日会大大增加情侣分手的几率# 好阔怕！\\r\\n \\r\\n其实事情是这样的：\\r\\n恋爱6年，26岁的小叶与比她大2岁的学长小李，计划今年春节步入婚姻殿堂。\\r\\n但双方家长见面后，在聘礼和婚房问题上出现了矛盾，小李甚至坚定地站在了他母亲那边，小叶也因此&quot;不想嫁了&quot;……\",\"author\":\"DanceLynx\",\"content\":\"小可爱们，大家看了今天的热搜吗？#年末节日会大大增加情侣分手的几率# 好阔怕！&amp;nbsp;其实事情是这样的：恋爱6年，26岁的小叶与比她大2岁的学长小李，计划今年春节步入婚姻殿堂。但双方家长见面后，在聘礼和婚房问题上出现了矛盾，小李甚至坚定地站在了他母亲那边，小叶也因此&quot;不想嫁了&quot;……&amp;nbsp;对此，专家表示，春节这个特殊节点，会促使小情侣重新审视两人的关系，再加上旁人的意见，就容易导致情侣感情破裂而分手。网友们则说：&amp;nbsp;现在很大一部分的男生啊，真是没能耐没本事还会为自己的无能，找各种花花借口。妈宝男啊？&amp;nbsp;真真的，你们这些男人，穷的时候女人跟了你们有几个以后不出轨的。呵呵，穷的时候嫌弃女人物质，有钱以后又看花眼了！&amp;nbsp;这个不一定吧，我男友也没房没车，他家里无法满足这些物质条件。我们也快大学毕业了，不过未来都比较有计划。他考了很多证书，很努力很上进而且懂事。现在穷不可怕，可怕的是穷还堕落如果毕业他向我求婚，我会嫁给他的！&amp;nbsp;就因为一个“去谁家过年”的问题就拆散了成千上万对情侣……&amp;nbsp;庆幸分手了！每每想起来都觉得真是无比正确的选择，这样的人，真的是在找保姆。他会要求你牺牲所有利益，满足他的家人，让他的家人得到很好的照顾。但就是不会考虑你的感受。嫁过去，你能做的只有不断的付出，不断的付出付出付出&amp;nbsp;&amp;nbsp;看完这则新闻，小艾只想说春节就是小型异地恋，不仅要防着对方的青梅竹马妖艳贱货，还得小心三大姨六大婶的“危言耸听”&amp;nbsp;但，年末节日真的会大大增加情侣分手的几率吗？经不住考验的不是节日，是爱情本身&amp;nbsp;同时，我们能理解父母长辈为孩子的幸福考虑，可是，长辈们在当孩子“第三者”前，真的不担心孩子以后找不着对象吗？\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257721),
(67, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257732),
(68, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\",\"main_image\":\"\\/uploads\\/20201009\\/b0cfdf3fb1e6e005825ab098f71bb3a4.jpg\",\"description\":\"国庆这几天，当大家的朋友圈都在晒各种大型旅行摄影展时，婚礼人的朋友圈晒图可能是这样\",\"author\":\"DanceLynx\",\"content\":\"国庆这几天，当大家的朋友圈都在晒各种大型旅行摄影展时，婚礼人的朋友圈晒图可能是这样在婚礼界做了许久，每当看到伙伴们早出晚归，辛勤忙碌，就想有一种画出他们日常工作状态的灵感。婚礼人是一个新兴职业，对于大众而言也许仍然陌生，而我们需要让客人们与社会大众了解这是一个需要分工缜密、专业精细、体力充沛的职业，体会到我们有一颗成人之美的心。&amp;nbsp;于是，一幅真实描绘婚礼人一天的极限挑战图诞生了做婚礼，外界看来似乎开心快乐，其实肩负重重压力，经常需要通宵达旦，疲惫不堪，但是一看到自己的付出，换来新人们及其家庭满意的答谢与笑容，任何辛劳便会一扫而尽，一种满满幸福与成就感油然而生。&amp;nbsp;有人曾说过，一个好的结婚典礼代表着一段美好婚姻的开始。我们在做的是一个家庭刚开头的事，这项工作也许不仅仅是一个典礼，而是开启了一段美丽人生。&amp;nbsp;每每到了一年的婚礼旺季，婚礼人熬夜是必须的。熬夜避免不了，但素材君偷偷告诉你，熬夜有技巧！献给最近经常熬夜身体透支的婚礼人们！&amp;nbsp;熬夜的您，千万记住：&amp;nbsp;1、晚饭不能吃太饱。熬夜时要吃热的东西，但不要吃泡面来填饱肚子，以免火气太大。最好尽量以清粥小菜、水果、土司来充饥。&amp;nbsp;2、开始熬夜前，来一颗维他命B群营养丸。维他命B能够解除疲劳，增强人体抗压力。&amp;nbsp;3、注意保暖，不要冻着肚子。&amp;nbsp;4、一定要有足够多的白开水。&amp;nbsp;5、熬夜时，应时时做深长呼吸。&amp;nbsp;6、提神饮料，最好以绿茶为主，可以提神，又可以消除体内多余的自由基，让您神清气爽。但是胃肠不好的人，最好改喝枸杞子泡水的茶，可以解压，还可以明目呢！但要注意应饮热的，浓度不要太高，以免伤胃。&amp;nbsp;7、早饭一定要吃饱，一定不要吃凉的食物。&amp;nbsp;8、凌晨7、8点钟，若要睡觉，一定要收心，即使不睡觉，也要坐在椅子上收心。&amp;nbsp;婚礼诚可贵，身体更重要。也祝各位婚礼人国庆愉快！您辛苦了！\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257788),
(69, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257810),
(70, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"800元请的跟妆师，居然只值50块？\",\"main_image\":\"\\/uploads\\/20201009\\/f2b8591a6ca7ab1af0ca9d9900950c25.jpg\",\"description\":\"『 先声明，这篇文章可能会得罪一些婚庆商家，但以下套路，全部来自真实报道和新人反馈。行业鱼龙混杂，我们见过很多非常优秀、有创造力、有行业道德的婚庆公司，但黑暗的一面也不能闭眼漠视。我们杜绝的是不良婚庆公司的忽悠，并非抹黑行业。无脑喷的，一概不理，谢谢。』\",\"author\":\"DanceLynx\",\"content\":\"『 先声明，这篇文章可能会得罪一些婚庆商家，但以下套路，全部来自真实报道和新人反馈。行业鱼龙混杂，我们见过很多非常优秀、有创造力、有行业道德的婚庆公司，但黑暗的一面也不能闭眼漠视。我们杜绝的是不良婚庆公司的忽悠，并非抹黑行业。无脑喷的，一概不理，谢谢。』&amp;nbsp;马上“十一”长假来了，很多妹子们都选择了在长假举办婚礼，小编也每天积极的为你们收集备婚干货，最近在逛iWed婚礼网新娘论坛的时候，看到了很多妹子吐槽自己被婚庆公司坑，看的小编十分的揪心啊。&amp;nbsp;@不P图的人妻：跟我们在初期接洽的时候说的特别好，会给我们做个方案，还有布置的效果图，还有提前一两周的时候提醒我们，给我们流程表和策划，balabala的，然而……什么都没有，在婚礼前两天的时候，我整整催了三十多次，才发了个当天时间安排给我。&amp;nbsp;@微笑：婚期五个月前就定了，眼看婚礼就四十几天了，婚庆公司一点动静也没有，每次都是我联系他，他说忙，第二天联系我，然后第二天又没有联系，还要我去联系他，什么鬼啊？很想发火了！&amp;nbsp;甚至有妹子在婚庆套路后忍无可忍，婚前40天宁可牺牲意向金也要换婚庆。&amp;nbsp;@KCc：筹备婚礼初期遇到了不靠谱婚庆公司，被套路了，套路1：被拖到离婚礼还有两个月的时间才看到一个所谓的“婚礼全案”。套路2：给的两版方案严重超预算，基础版方案超70%，升级版方案超110%，还好没有超200%，是不是算手下留情了。&amp;nbsp;沟通几次无果，期间也纠结了两周左右时间，曾考虑是否要妥协，两眼一摸黑，把报价单给签了，可是想到这个团队整个过程中如此不专业且敷衍的态度，给付意向金前后承诺的东西完全对不上号，这样的团队实在无法让自己放心把婚礼托付于他们，最终只能忍痛舍弃意向金更换了婚庆。&amp;nbsp;并且在昨日，小编看到一篇来自《北京晨报》的报道——《支付800元聘请化妆师，婚庆公司花50元找来外包人员》。&amp;nbsp;一看标题小编就知道，又有妹子被不良婚庆公司给坑了，小编今天就拿报道中的一些案例，为你们盘点不良婚庆公司的“七宗罪”。&amp;nbsp;希望你把这篇分享给同样在备婚的准新娘们。&amp;nbsp;外聘人员需谨慎&amp;nbsp;吴女士在婚礼当天还遭遇了“魔鬼化妆师”。吴女士说，结婚当天，婚庆公司带来了“专业”化妆师，来给她化妆。本以为化妆师会让她更加漂亮，睁开眼却吓了她一跳。“我当时睁眼后都要崩溃了，绿色和蓝色相间的眼影，还有一个血红血红的嘴唇，真的很土，还不如我自己化的。”吴女士称自己和化妆师沟通后才知道，她出了800元请化妆师，但是这名化妆师是婚庆公司花了50元找来的外包人员。“我当时气得都不知道说什么好了，只能把脸洗了自己重新化妆。”一定要提前咨询哪些人员是外聘的，有些公司很可能部门不齐全，只有策划师、执行、业务人员，其余人员都需要临时从外面聘请，婚庆公司其中一块收入就是赚取这些人员的差价。更离谱的公司是策划、执行都是一个人，刚拉完业务就要去采购，再从外面聘请司仪、摄影摄像，效果可想而知。&amp;nbsp;小编建议你们自己另外请四大金刚，很多独立的四大金刚工作室都发展得很成熟，这样不仅避免了婚庆公司从中赚差价，还能选到自己喜欢风格的司仪、摄影摄像。&amp;nbsp;个性化婚礼却都是亲兄弟&amp;nbsp;准备在假期举办婚礼的林先生说，近一个月已经去了多家婚庆公司，但发现他们的方案都差不多，越来越多雷同的婚礼被照搬，“几家婚庆公司给的方案都雷同，换汤不换药，后来找了懂行的朋友一问，原来几家公司的灯光和舞台搭建都是承包给同一家公司做的，怪不得看着都像是兄弟姐妹，长得差不多。这样一来，所谓个性化婚礼，根本就是无稽之谈。”&amp;nbsp;优秀的婚庆公司，会在和新人的交流过程中，观察新人的喜好和特点，找到适合新人的婚礼主题，及时给到新人以切实可行的一些婚礼策划及筹备建议。而不是照搬照抄，如果你想要一场属于你们的个性化婚礼，一定要多沟通，有经验的婚庆公司是可以给你创意想法的。&amp;nbsp;做盗图党很容易&amp;nbsp;同样是上面的案例，小编还见过盗图党。所谓盗图党就是直接抠掉别人的logo说是自家作品，有些还堂而皇之加上自己logo。不但网上挂出来的案例都是盗图，在实体店里也说这些案例就是他们做的，说地天花乱坠头头是道。这里放一个抠掉别人logo的案例，去水印真没你想象的复杂。所以不要轻易相信所谓的现场图。&amp;nbsp;有原创实力的婚礼设计公司，有着完整的方案设计，像现在婚庆公司一般都会给你手绘或者电脑做出的现场效果图。但有些坑爹公司连效果图也能盗来，所以，可以提出看婚庆公司工作人员朋友圈中的婚礼视频，更真实的看到他们的作品效果。&amp;nbsp;套餐 = 套路？&amp;nbsp;小强向记者感叹，自己被婚庆公司坑得很惨。“当时合同中只写明了‘背景布置’四个字和5000元套餐价格，没有写清楚背景里面都包括什么。我当时想当然地认为，这个5000元就是我们图片上的布置内容，包括鲜花、气球等物品，老板也说可以做成这种效果。可是，等到合同签好了，老板才跟我们说这个价格的背景就只是一块红色的布和上面的飘纱，如果想要加灯光、气球鲜花等装饰，就得加钱，而且还不便宜。为了达到效果，我又多花了3900元。”&amp;nbsp;一般婚庆公司给的场地布置的套餐费用看起来都是很划算的，因为可以反复利用很多布景、签到台等材料。&amp;nbsp;但现在用的最多的则是定制+套餐的模式，会先给你一个套餐，然后会和你说这个套餐对于你的婚礼是不够的，为了更好的效果，可能你要在灯光上面加射灯，你要加一些什么什么，来让你选择。&amp;nbsp;有很多姑娘很纠结，我到底是不是真的要加这些材料，那么这时你要做的就是问的更细致一些，让他给你看两种案例，一种是加这个材料和不加这个材料的效果，主要的区别在哪里？主要用在哪里？让自己知道这钱到底花的值不值得。&amp;nbsp;策划和现场大不同&amp;nbsp;周女士说，布置当天呈现出的花门、花亭和策划时看的照片完全都不一样。“原本计划的是圆形花门，上面飘丝带。但实物出来就是一个塑料门，上面摆了一些塑料花束夹杂着几根鲜花，几根包装用的彩带用胶条贴在上面，显得特别廉价。我和爱人都非常着急，找到婚庆公司负责人让他们马上整改。但是，这名负责人却说，合同上只写明了花门花亭的价格，没有写样式，他没办法改。”周女士十分无奈，“当时签合同时，婚庆公司老板说样式没办法写在合同上，请我们放心，绝对不会出错。结果婚礼当天看到的结果竟然是假花夹杂着鲜花。如果使用假花，我们婚礼的费用会减少将近7000元。虽然我们很气愤，但是没办法，这婚还得结啊。”&amp;nbsp;小编想和你们想和你们强调一点，合同是唯一可以保障你们权益的凭证。无论婚庆公司签合同前如何服务周到贴心，也不要对细节有所松懈。小编建议，在签合同时要有示例图片为证或在合同中明确鲜花数量、面积等细节☟花艺是真花还是假花？真花花量多少？是进口花还是国产？背景板是泡雕还是kt板材还是雪弗板还是喷绘？舞台是镜面板还是镜面地毯？搭建是龙门架还是普通桁架？布艺是普通布还是绒布？灯光是染色灯par灯摇头灯还是光束灯？可能搞花样的地方太多。&amp;nbsp;合同写清楚，你再放心的去准备别的事情。&amp;nbsp;折扣！折扣！&amp;nbsp;许多商家会经常做折扣活动，赶上一些大促活动也是“乱花渐欲迷人眼”，小编并不是说做折扣的一定是不好的，而是你要在其中“货比三家”，多看细节，多比较，多询问，不要为了贪小便宜吃大亏。&amp;nbsp;小钱也要算计你&amp;nbsp;“婚庆推荐的摄像师很健谈，我们看了他拿出来以前拍的成品感觉也很好，而且双机位的价格相当划算，所以当场就敲定下来了。没想到等到取光盘的时候，婚庆告诉我们后期制作刻光盘要加800块钱才能给我们。虽然结婚大钱都花了那么多，800块钱也不算特别多，但让我们心里多少有点被人算计的感觉，总觉得不舒服。”&amp;nbsp;结婚本来就是一件烧钱事，无良商家也是利用了新人的这个心理，让很多新人吃了哑巴亏，所以小编要再次强调：合！同！写！清！楚！除了合同上的钱，还有哪些是需要额外付款的，别大婚当天高兴过头了，让这些“小钱”变“巨款”。&amp;nbsp;最后小编想说，选一家靠谱的婚庆公司太重要了，如果新人有预算，还是找口碑比较好的中大型婚庆策划公司，这些公司价格虽然高，但最起码有保证。如果预算不够多，那就要在合同上下足功夫，一时的仔细，会为你的婚礼省心不少。还是那句话，合同是唯一能够保障你们权益的凭证。\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257841),
(71, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257932),
(72, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"中日婚姻观大不同:中国看重车房 日女性重价值观\",\"main_image\":\"\\/uploads\\/20201009\\/ff8d87fabf6805571f76f00bc5d64017.jpg\",\"description\":\"日媒称，在中国达到适婚年龄即20岁左右时，基本都会马上结婚生子。中国人比日本人还要重视家庭，不过在结婚生子方面，中国人也饱受来自“四面八方”的压力，因为中国男性在结婚时要有房和车，这是“必须条件”，而日本女性更重视的是男性的卫生和价值观。\",\"author\":\"DanceLynx\",\"content\":\"日媒称，在中国达到适婚年龄即20岁左右时，基本都会马上结婚生子。中国人比日本人还要重视家庭，不过在结婚生子方面，中国人也饱受来自“四面八方”的压力，因为中国男性在结婚时要有房和车，这是“必须条件”，而日本女性更重视的是男性的卫生和价值观。据日本research网站报道，中日男女在交往时所重视的东西大有不同。当下，日本社会过度追求物质享受的风气已“偃旗息鼓”。由于经济不景气，在这种环境下成长的日本年轻人并不过分追求物质，买车的话则首选耗油量较小的小型车，买房也选择贷款。一般来说，年轻人不会在车房方面向家里提出购买要求。&amp;nbsp;日媒称，在中国，结婚时男方要有房车，这是“必须条件”。也就是说，经济能力是女性结婚时非常看重的条件，时常比男性的性格或者双方是否拥有共同价值观等方面还要重要。&amp;nbsp;但是日本女性重视的东西和中国女性大不相同。比起男性到底拥有多少财产，日本女性更重视的是男性的卫生和价值观，比如男性应每天整理发型和胡须。虽然日本女性也认为稳定的收入很重要，但是对于一个日本职场的女性来说，“人生价值观、性格和金钱观念同自己比较契合才是最重要的。”两人有共同语言，生活才能安定。据日本CCC网站的一个调查结果显示，当日本女性动结婚想法时主要考虑三个因素。第1，时机成熟（33.6%）；第2，年龄不饶人（10.2%）；第3，非他莫属（9.5%）。结婚条件前3名分别是：性格、价值观、脾气好。&amp;nbsp;日媒称，确实日本男性注重自身卫生的意识比中国男性高，但也并不是说中国男性不卫生。但是中国男性结婚时要确保拥有房和车，一般婚后还要还高额贷款，生活并不那么轻松。数据来源：中国产业信息网站&amp;nbsp;据此前数据调查显示，2016年中国女性择偶收入要求占比为：67.06%的单身女性要求男性收入在5000-10000元，25.02%的单身女性要求男性收入过万元。相比之下，男性对于女性的收入要求普遍较低，80%的单身男性表示对方收入低于5000元也可以接受。&amp;nbsp;据此前报道，某知名婚恋网站发布中国人婚恋状况调查报告，这份收集了7万余份样本的调查报告显示，独处时间太长以及圈子太小，是单身主要原因。报告显示，71.8%的女性认为男性有房才适合结婚，需要有车的比例为17.8%。男性关心前三位的是外在相貌、身体健康、情感经历，女性则更看重对方的经济条件、身体健康和职业。\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257969),
(73, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602257992),
(74, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"移动互联网给85后婚礼带来了哪些改变\",\"main_image\":\"\\/uploads\\/20201009\\/b1f80c0e7260e037d2cee61ff107eeef.jpg\",\"description\":\"结婚季来袭，85后的年轻人们，结婚时最流行什么? 记者调查并采访了北京地区的100对新人，调查发现随着互联网技术及文化的发展，请柬、礼金、婚礼形式方面显现出新技术、新手段、新途径的趋势。其中，移动支付产品因其便捷性和潮流性备受推崇，成为现象级“婚礼利器”。\",\"author\":\"DanceLynx\",\"content\":\"结婚季来袭，85后的年轻人们，结婚时最流行什么? 记者调查并采访了北京地区的100对新人，调查发现随着互联网技术及文化的发展，请柬、礼金、婚礼形式方面显现出新技术、新手段、新途径的趋势。其中，移动支付产品因其便捷性和潮流性备受推崇，成为现象级“婚礼利器”。现象一：超八成新人采用微信电子请柬 快递伴手礼渐成新时尚&amp;nbsp;相较于过去的纸质版大红色请柬，承载内容全、个性色彩浓的iWed婚礼网微信请柬成为婚礼请柬的“新宠”。调查数据显示，100对新人中，83对新人选择了电子请柬和纸质版两种版请柬形式，13对新人只寄出请柬，其余被访新人则选择寄出喜糖或伴手礼等实物请柬。&amp;nbsp;(婚礼请柬形式发生改变)除请柬外，随请柬一起寄出伴手礼也逐渐成为新时尚，伴手礼也不局限于当地特产，而引申为精心为亲友准备的礼物。随婚礼请柬寄出的伴手礼包括喜糖、当地特产、永生花等有婚礼特色的喜庆物品。&amp;nbsp;现象二：“份子钱”通过移动支付转账 礼金也能去理财&amp;nbsp;说到婚礼就不得不提“份子钱”，来宾包个红包并在名册上登记以象征给新人带去祝福，不同城市对礼金金额也有“约定俗成”的标准。而随着移动支付的发展， “份子钱”的接、收方式正在发生改变。&amp;nbsp;在采访中记者发现，100对被访新人中，64对新人都收到过通过线上转账而来的“份子钱”。而提到希望通过哪种方式收到礼金时，21%的新人提到社交属性更强的微信，22%的新人仍然会根据习惯选择支付宝，32%的新人希望收到现金，1%的新人希望银行转账或其他形式。显然，移动支付已经成为“份子钱”往来的主力。&amp;nbsp;(不同礼金收入方式在新人心中的认可度)&amp;nbsp;现象三：婚礼直播陡然兴起 消费理念再度升级&amp;nbsp;在全民直播的当下，明星婚礼直播也提供了一种创新思路。记者发现，司仪兼职主播已经悄然成风，甚至有新人利用打赏功能再创造一笔收入。婚礼直播不仅可以弥补无法到场的亲友观看婚礼仪式的遗憾，直播的回放功能还可以支持他们日后在手机端观看全程，是一种美好的体验和纪念。&amp;nbsp;(视频直播APP种类繁多)&amp;nbsp;综上所述，在互联网和年轻群体的双重影响下，和结婚有关的诸多传统消费都有了新的发展。其中最颠覆传统的当属移动支付对“份子钱”的改变，而这也是85后、90后婚礼相对于老一辈最大的不同之处。此外，新技术、新形式的发展也促进婚礼向科技范发展，而互联网带来的便捷同时影响新人们的婚礼消费。\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602258021),
(75, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602258048),
(76, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/post/add?dialog=1', 'sdcmarry / 文章 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"news\",\"title\":\"婚礼舞台架突然倒塌 安全意识低酿悲剧\",\"main_image\":\"\\/uploads\\/20201009\\/c09c430c465f97531f581113611b2899.jpg\",\"description\":\"不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。\",\"author\":\"DanceLynx\",\"content\":\"不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。作为新郎新娘至亲入座的主桌位于离舞台最近的区域，恰巧在这个钢架正下方，坐在主桌上的7人因躲闪不及被倒下的钢架砸中，由于钢架非常重，待公安、消防、安检、120急救等部门赶到现场后，伤者才被救出送往医院，据了解有3名伤者被送往了无锡市人民医院，其余4名则被送至第四人民医院。&amp;nbsp;随后，记者来到四院，此时几名伤者正在急诊室内接受治疗，其中两名伤者头部受伤，不过幸运的是没有直接砸中头部，都是被架子蹭到。据一名伤者说，他所坐的位置是背对着舞台的，因此并不知道发生了何事，听到有人发出尖叫抬起头，才发现架子倒下来砸在面前的餐桌上，而自己也被蹭到受了伤。伤者的家属小明（化名）告诉记者，当晚自己和妈妈，外婆，外公四人一起作为新娘的亲眷坐在了主桌，当时婚礼接近尾声，婚庆公司在收拾舞台设备时不慎发生了事故，当时因为自己和外公坐的离舞台稍远的位置因此未被钢架砸中，而母亲和外婆却没能逃过一劫，新娘也因此事件当场情绪失控。&amp;nbsp;据了解，此次事故共造成七人受伤，其中三人骨折，其余四人均为皮外伤，所幸七人都无生命危险，事故具体原因正在进一步调查中。记者从医院了解到，事故发生后有多人受伤，伤势最重的被砸骨折，大多数人是皮外伤，并无生命危险。目前，事故原因仍在调查中。&amp;nbsp;婚礼执行过程中的安全问题，一直都牵动着婚礼人的神经，特别是使用了众多器材设备的大型婚礼中，稍微出点马虎大意，都会酿出不可挽回的损失。对此我们希望婚庆公司一定要有安全意识。酒店与婚庆要各司其职，安全问题，重于泰山！\",\"is_post\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.70', 1602258063),
(77, 0, 'Unknown', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', '', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"qwie\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602340720),
(78, 0, 'Unknown', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', '', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"k6mn\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602340729),
(79, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', 'Login', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"udtu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602340735),
(80, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;首页导航&quot;,&quot;post&quot;:&quot;文章列表导航&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u6700\\\\u65b0\\\\u8d44\\\\u8baf&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602340831),
(81, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"post\",\"title\":\"婚姻百科\",\"image\":\"\",\"path\":\"\",\"options\":\"hybk\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602340983),
(82, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u5bfc\\\\u822a&quot;,&quot;post&quot;:&quot;\\\\u6587\\\\u7ae0\\\\u5217\\\\u8868\\\\u5bfc\\\\u822a&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;结婚攻略&quot;,&quot;hybk&quot;:&quot;婚姻百科&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602341096),
(83, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"post\",\"title\":\"结婚攻略\",\"image\":\"\",\"path\":\"\",\"options\":\"news\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602341224),
(84, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602480604),
(85, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602480612),
(86, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602480612),
(87, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602480612),
(88, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"现场布置\",\"main_image\":\"\\/uploads\\/20201012\\/6fb06c1b10982d5eae781f1748ada5fe.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/6fb06c1b10982d5eae781f1748ada5fe.jpg,\\/uploads\\/20201012\\/4dc1d65de0be85512daaa45f52c2e384.jpg,\\/uploads\\/20201012\\/325b6975698a34756cb4ce43ab4255e0.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602480620),
(89, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481131),
(90, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481148),
(91, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481148),
(92, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481148),
(93, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/1?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"现场布置\",\"main_image\":\"\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg,\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg,\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481155),
(94, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481197),
(95, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481208),
(96, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481208),
(97, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481208),
(98, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"拍照摄像\",\"main_image\":\"\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg,\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg,\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481209),
(99, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481235),
(100, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481245),
(101, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481245),
(102, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481246),
(103, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚车装饰\",\"main_image\":\"\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg,\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg,\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481247),
(104, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481269),
(105, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481275),
(106, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481275),
(107, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481276),
(108, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚礼奏乐\",\"main_image\":\"\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg,\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481278),
(109, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481296),
(110, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481305),
(111, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481305),
(112, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481305);
INSERT INTO `fa_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(113, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"marry\",\"title\":\"婚礼活动\",\"main_image\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg,\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481308),
(114, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481340),
(115, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481346),
(116, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481346),
(117, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481346),
(118, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"marry\",\"title\":\"婚车服务\",\"main_image\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg,\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481348),
(119, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481371),
(120, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481383),
(121, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481383),
(122, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481383),
(123, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"marry\",\"title\":\"份子钱\",\"main_image\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481386),
(124, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481433),
(125, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481439),
(126, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481439),
(127, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481440),
(128, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"marry\",\"title\":\"婚礼致辞\",\"main_image\":\"\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602481442),
(129, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;首页导航&quot;,&quot;post&quot;:&quot;文章列表导航&quot;,&quot;example&quot;:&quot;案例导航&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u7ed3\\\\u5a5a\\\\u653b\\\\u7565&quot;,&quot;hybk&quot;:&quot;\\\\u5a5a\\\\u59fb\\\\u767e\\\\u79d1&quot;}\",\"example\":\"{&quot;live&quot;:&quot;\\\\u73b0\\\\u573a\\\\u6848\\\\u4f8b&quot;,&quot;marry&quot;:&quot;\\\\u5a5a\\\\u793c\\\\u6848\\\\u4f8b&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602482409),
(130, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx920f9d94128458ff&quot;,&quot;app_secret&quot;:&quot;93ed4e7b6bc3d0ee8d9c56f2390b8a8a&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u5bfc\\\\u822a&quot;,&quot;post&quot;:&quot;\\\\u6587\\\\u7ae0\\\\u5217\\\\u8868\\\\u5bfc\\\\u822a&quot;,&quot;example&quot;:&quot;\\\\u6848\\\\u4f8b\\\\u5bfc\\\\u822a&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u7ed3\\\\u5a5a\\\\u653b\\\\u7565&quot;,&quot;hybk&quot;:&quot;\\\\u5a5a\\\\u59fb\\\\u767e\\\\u79d1&quot;}\",\"example\":\"{&quot;live&quot;:&quot;\\\\u73b0\\\\u573a\\\\u6848\\\\u4f8b&quot;,&quot;marry&quot;:&quot;\\\\u5a5a\\\\u793c\\\\u6848\\\\u4f8b&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602482441),
(131, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"example\",\"title\":\"婚礼案例\",\"image\":\"\",\"path\":\"\",\"options\":\"marry\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602482523),
(132, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/add?dialog=1', 'sdcmarry / 标签导航 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"example\",\"title\":\"现场案例\",\"image\":\"\",\"path\":\"\",\"options\":\"live\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602482554),
(133, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602491938),
(134, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602491946),
(135, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602491946),
(136, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602491946),
(137, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/add?dialog=1', 'sdcmarry / 案例 / 添加', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"完美婚姻\",\"main_image\":\"\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602491947),
(138, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/9?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"完美婚姻\",\"main_image\":\"\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\",\"content\":\"不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。作为新郎新娘至亲入座的主桌位于离舞台最近的区域，恰巧在这个钢架正下方，坐在主桌上的7人因躲闪不及被倒下的钢架砸中，由于钢架非常重，待公安、消防、安检、120急救等部门赶到现场后，伤者才被救出送往医院，据了解有3名伤者被送往了无锡市人民医院，其余4名则被送至第四人民医院。&amp;nbsp;随后，记者来到四院，此时几名伤者正在急诊室内接受治疗，其中两名伤者头部受伤，不过幸运的是没有直接砸中头部，都是被架子蹭到。据一名伤者说，他所坐的位置是背对着舞台的，因此并不知道发生了何事，听到有人发出尖叫抬起头，才发现架子倒下来砸在面前的餐桌上，而自己也被蹭到受了伤。伤者的家属小明（化名）告诉记者，当晚自己和妈妈，外婆，外公四人一起作为新娘的亲眷坐在了主桌，当时婚礼接近尾声，婚庆公司在收拾舞台设备时不慎发生了事故，当时因为自己和外公坐的离舞台稍远的位置因此未被钢架砸中，而母亲和外婆却没能逃过一劫，新娘也因此事件当场情绪失控。&amp;nbsp;据了解，此次事故共造成七人受伤，其中三人骨折，其余四人均为皮外伤，所幸七人都无生命危险，事故具体原因正在进一步调查中。记者从医院了解到，事故发生后有多人受伤，伤势最重的被砸骨折，大多数人是皮外伤，并无生命危险。目前，事故原因仍在调查中。&amp;nbsp;婚礼执行过程中的安全问题，一直都牵动着婚礼人的神经，特别是使用了众多器材设备的大型婚礼中，稍微出点马虎大意，都会酿出不可挽回的损失。对此我们希望婚庆公司一定要有安全意识。酒店与婚庆要各司其职，安全问题，重于泰山！\"},\"ids\":\"9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500564),
(139, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/8?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚礼致辞\",\"main_image\":\"\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\",\"content\":\"高速隧道办婚礼引关注！据了解，9月20日上午10时31分，包茂高速安康段凤凰山隧道出口100米处发生一起单方交通事故。车祸致高速路双向拥堵，于是一对新人为赶吉时，在隧道内办“特殊婚礼”。据悉，为保障安全以及不影响他人的出行，婚礼整个仪式只进行了十分钟，过路司机纷纷当起了婚礼嘉宾。有网友表示仪式十分钟感动一辈子！&amp;nbsp;高速路上遭遇堵车，想必每个人都不会有好心情。如果是遇到结婚这样的人生大事，堵车更是让人糟心。9月20日，安康一对新人因堵车担心错过良辰吉时，在高速隧道内举办的一场“温情”婚礼，却让不少网友大呼“浪漫”，并纷纷送上祝福。&amp;nbsp;车祸致高速路双向拥堵&amp;nbsp;两新人隧道内办“特殊婚礼”&amp;nbsp;9月20日上午10时31分，包茂高速安康段凤凰山隧道出口100米处发生一起单方交通事故。辆由紫阳驶往安康方向的重型半挂车与中央护栏碰撞后横在了路中间，致使车头和车尾分离，车内人员受伤。&amp;nbsp;随后高速交警赶赴现场处理，由于事故导致该处路段两个方向以及应急车道彻底被堵死，施救难度比较大，直至12时10分，救援人员清理出一个车道单边放行。下午2时20分，该处路段彻底恢复畅通。&amp;nbsp;11时30分左右，凤凰山隧道内已堵了近100辆车，见苦等一个小时仍未通车，加上隧道内空气污浊，让不少司机都非常苦闷。可接下来的一幕，让大家抓走了眼球，也让堵在现场的司机赵先生觉得既新奇又感动。&amp;nbsp;“当时我在后面堵着，见前面有不少人围在一起，便凑了上去原来是一队婚车也困在隧道车流中，眼看马上快到约定的婚礼开场时间，一对新人迫于无奈，便在司仪以及朋友见证下临时办了一场婚礼，现场不少过往司机也被邀请参加了这场特别的婚礼，并用手机记录下他们的幸福瞬间。”&amp;nbsp;赵先生感慨地说， 他是头一次遇到这样特别的婚礼，“我想告诉他们，两个人过一生肯定会有许多坎坷，这点小小的堵车不算什么，反倒会让他们今生难忘，就当是迈向婚姻生活的第一步，祝福他们能够白头到老。”&amp;nbsp;过路司机当起婚礼嘉宾&amp;nbsp;陌生人祝福中甜蜜相拥&amp;nbsp;随后，华商报记者辗转联系到负责此次婚礼策划的安康市艾纱婚礼策划公司负责人小吴，据当时在现场的他介绍，自己曾策划过许多次婚礼，但在高速路隧道内举办婚礼还是头一遭。新郎是安康市汉滨区人，新娘子是紫阳县人。&amp;nbsp;当日早上五点多，新郎便乘坐婚车去接新娘，不料返程时因前方发生车祸，被堵在了隧道内。当时觉得距离12点办仪式的时间还早，大家心态都比较轻松，新郎还和几个朋友蹲在隧道内玩了一会扑克。&amp;nbsp;不料一个多小时后，隧道仍未疏通，按照当地习俗，新人必须在中午12点前办仪式，于是他们商量并征得新人同意后，便举行了一个简单婚礼仪式，“当时为了保证安全，以及不影响其他车辆，仪式缩短为10多分钟了。”&amp;nbsp;在司仪主持下，新郎从婚车上将新娘接下。“无论贫穷与富贵，无论疾病与健康，你是否都愿意一生守护着他(她)?”&amp;nbsp;在司仪的询问下，新郎新娘“我愿意”的承诺清脆又响亮。&amp;nbsp;随后，两人在四五十位亲朋以及受邀作为嘉宾的过往司机祝福和掌声中，紧紧地拥抱在一起。12时10分，经过紧张疏通，救援人员清理出一个车道单边放行，这对新人成功赶到位于安康市高新区的酒店成功举办了婚礼。&amp;nbsp;“当时我在办仪式的酒店，没赶上婚礼现场有些遗憾，但从网上看到朋友发的现场照片以及网友祝福后非常感动，太有纪念意义了。”&amp;nbsp;下午4时许，新郎的朋友小田告诉华商报记者，两位新人看到网友的祝福后都很激动，也很感谢大家，但由于办完婚礼后比较劳累，因此婉拒了记者的采访要求。&amp;nbsp;网友评论：&amp;nbsp;@897有话说：&amp;nbsp;有一句话叫做 “只要心中有海，哪里都是马尔代夫”，但今天这场特别的婚礼让我们看到——“只要心中有爱，在哪里都能与子偕老”。因故堵住的隧道里，婚礼虽然简陋，但这一份爱未必简单。一群人，两行车，无数闪光灯，就是对这两位新人最大的祝福!&amp;nbsp;@刻骨铭心：最浪漫最美的最有纪念意义的婚礼，祝福你们!&amp;nbsp;&amp;nbsp;@老吴：在困难当中接下的海誓山盟，才显得更加难能可贵!相信在未来的日子，新娘一定会记得隧道灯光下，新郎的甜蜜誓言。\"},\"ids\":\"8\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500593),
(140, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/7?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"份子钱\",\"main_image\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg,\\/uploads\\/20201012\\/f2fe55b616856a0930d71f64f6299857.jpg\",\"content\":\"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp;据数据分析，在未来五年，我国结婚人口总体呈下降趋势。这表明人口红利带来的爆发式增长已经不再，结婚行业也不例外。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 从27日、28日在上海世博展览馆举办的第34届婚芭莎·中国婚博会上获悉，目前上海新人结一次婚的平均总花费（不包括婚房、婚车）已攀升到34万元左右。&amp;nbsp;新人结婚平均花34万&amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 每年分四季在上海、北京、广州、天津、武汉等五大城市巡回举办的中国婚博会是目前全球最大规模的一站式结婚消费展，仅在上海一地每年就举办四次，单次展出面积在5万-7万平方米，因此也被称为上海新人结婚走势的风向标和晴雨表。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 婚博会相关负责人表示，除去购买婚房、婚车的最大开销外，通常意义上所指的结婚消费主要包括八个方面，即新房装修、婚纱摄影、结婚礼服、婚戒首饰、喜糖喜酒、婚庆服务、酒店喜宴、蜜月旅行等。在八大消费中，支出最大头是婚宴。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 从在沪举办的最近四届中国婚博会统计情况来看，总到场人数分别为103500人、115100人、147600人和135100人，总订单数分别为36600份、42500份、53500份和48900份，下单总额分别达到7.94亿、8.68亿、11.6亿和10.623亿元。约有近六成观展新人选择在婚博会现场下订单。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 总体来看，婚纱摄影、婚戒首饰和婚宴已经成为上海人的“结婚标配”，80%以上的结婚用户都会选择这三样；其次为婚庆服务和婚纱礼服，各有平均58%和42%的用户会选择；受中国传统“酒桌文化”影响，婚宴消费成为结婚“最大头”，占到整体花费的65%以上。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 今年1-6月，本次婚博会主办方对近年来5万名上海准新人进行了调查，调查对象包括最近几届参加婚博会的新人和部分上海地区线上会员，调查结果显示，目前上海新人结一次婚的平均总花费（不包括婚房、婚车）已攀升到34万元左右，比武汉、成都、苏州等二三线城市贵出将近一倍。&amp;nbsp;租婚纱定制妈妈装成潮流&amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 走进本次中国婚博会婚纱礼服的主展区，记者看见有很多观展新人驻足。一对在父母陪同下前来参观的准新人告诉记者，如今作为结婚主力人群的85、90后上海新人眼光更高，从出门纱、结婚礼服到敬酒装起码要三四套，在迎接宾客、给亲朋好友敬酒时都是近距离接触，所以他们更倾向于花五六千租几套设计感和面料都上档次的婚纱。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 在面料和款式上，采用进口面料和手工定制的重工蕾丝以及大胆追求性感的露背婚纱成为主流，而新郎礼服则普遍以定制为主，“妈妈装”也开始大行其道。一位参展商表示，这是因为如今的上海新人越来越重视为父母装扮，考虑到妈妈们的体形普遍比较富态，很难租到合适的礼服。&amp;nbsp;酒店纷免“婚庆进场费”&amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 今年婚博会传递出的另一大变化就是婚车和旅拍的走红，一辆辆挂着沪牌的宝马五系、七系、劳斯莱斯婚车成为很多婚庆公司和酒店展台上的“重量级展品”。“上海人结婚喜欢讲排场。”某婚庆公司参展人员告诉记者，现在车况好、带沪牌的婚车最抢手，因为可上高架还不受早晚高峰的限制，而且色彩统一，组成车队很气派。而旅拍则以东南亚海岛和欧洲最受欢迎。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 记者注意到，根据我国每年出生人口的数据和结婚年龄分布综合分析，2015年全国结婚人口在1200万对左右，至2020年时这一数字会下滑至1000万对以下。在未来五年，结婚人口总体呈下降趋势。这表明人口红利带来的爆发式增长已经不再，结婚行业也不例外。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 据主办方透露，参加本届中国婚博会的新人预计在3.5万对左右，其中首日参观新人为2.5万对，首日下单总额为4.23亿元，要比前两年高峰时的5万对下降不少。上海新人初婚年龄的推迟也是结婚人数下降的一个原因，加上近两年沪上涌现出20多家一站式婚礼会所，对星级酒店稳固的婚宴市场带来不小的冲击。受此影响，沪上20多家知名的高星级酒店首次在婚博会上宣布对结婚新人免收2000-5000元的“婚庆进场费”，在黄金档期上的选择面也更加宽裕。&amp;nbsp;“小蜜蜂”现场拉客要被罚&amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 历届婚博会上导购人员（俗称“小蜜蜂”）强行拉客甚至大打出手的现象也屡受诟病。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 婚博会相关负责人陈赟表示，主办方今年对婚博会的所有观众、参展商和400多位展商雇佣的“小蜜蜂”全部采取实名制登记报备制度，每一名报备过的导购人员的身份信息都有存档，且需佩戴导购证上岗。主办方划出明确的导购框和点位，导购人员不得逾越导购框拉拽观众。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; “我们这次在展馆内配备了300多名戴着 值勤 字样红袖章的保安，负责驻点值守和流动巡视。”一旦发生违规情况，“除了清除出馆外，还将通过身份证登记信息对相关参展商开具警告单，遭到多次警告的商家将被罚款。”陈赟透露。\"},\"ids\":\"7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500629),
(141, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/6?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚车服务\",\"main_image\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg,\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,\\/uploads\\/20201012\\/aa4db94b8d3ba21079908f472972ae0b.jpg\",\"content\":\"&amp;nbsp;&amp;nbsp; &amp;nbsp; 近年来，“土豪婚礼”、“土豪婚车”越来越受到市民的关注。27日上午，济南街头也出现了一支堪称“土豪”的迎亲队伍，一支由150余辆私家车组成的迎亲队伍出现在清河北路与历山北路附近。队伍以一辆捷豹打头，随后是10辆凯迪拉克、3辆奔驰、120辆形色各异的私家车，还有20辆统一规格的厢式货车。&amp;nbsp;　　知情人透露，迎亲队伍从尚品清河小区出发，经过泺口服装城后向东沿清河北路一路到达历山北路黄台电厂产业园，车辆所到之处无不引起交通拥堵。10:20左右，记者在清河北路看到结婚车队，每辆车都贴有一张心形号码牌，从1号到150余号。整个车队从清河北路绵延至历山北路，长约至少1公里。记者观察，整个车队过路口红绿灯足足用了半个多小时，同时，路口造成大范围拥堵。车队一位司机告诉记者，迎亲的新郎为某物流公司的员工，所有车辆都是公司同事“友情赞助”，不需要任何费用，“连婚车都是借我们老板的，不花钱。”&amp;nbsp;　　随后，记者从新郎王国建处得到证实，车队的所有车辆确实都是公司同事的私家车，“以前家里条件不好，后来才慢慢得到改善，家里也不想铺张浪费，大家就想借着大喜的日子热闹热闹，兄弟姐妹们就想了这么个招儿，不存在炫富啥的。”面对这个“超长”迎亲队伍，市民王先生和李女士都觉得很热闹，“挺好，结婚就得热热闹闹的。再说了，公司的车又花不了几个钱，这跟那种‘土豪车队’不一样。”&amp;nbsp;　　当然，也有不少市民持反对意见，家住清河北路附近的唐女士表示，即使车队花不了多少钱，如此迎亲也太浪费，而且对交通造成拥堵，“我们当年那么简单，没有婚车没有豪华酒宴，现在不是一样很幸福吗？”&amp;nbsp;　　相关链接&amp;nbsp;　　婚车的变迁：从花轿到加长林肯&amp;nbsp;　　●最传统：花轿、马车&amp;nbsp;　　虽然现在流行的汽车接亲同婚纱一样，是从西方舶来的婚庆礼仪，可论起婚车，最老资格的可是中国传统婚礼的花轿。&amp;nbsp;　　●最环保：自行车&amp;nbsp;　　缝纫机、手表、收音机、自行车，上世纪六七十年代家庭争配的“三响一转”，悄悄弥补了公共交通的不发达，也改变了新娘子的座驾。&amp;nbsp;　　●最高调：拖拉机、卡车&amp;nbsp;　　上世纪七十年代，机械化扩大了人们的生活半径。有头有脸的家长会努力跟合作社借台手扶拖拉机，县城和农村的新娘子们也能享受到高科技。&amp;nbsp;　　●最团结：吉普车、面包车、卡车&amp;nbsp;　　七十年代初的吉普车，七十年代末的面包车，八十年代前期和中期的面包车，这些公车私用往往会动用了男女双方所有的社会关系，才有望拼出两辆来。&amp;nbsp;　　●最拉风：老红旗、桑塔纳、老尼桑进口车等等&amp;nbsp;　　从八十年代末到九十年代，国产的老红旗，进口的桑塔纳，尼桑等车型成为首批婚庆轿车。&amp;nbsp;　　●最常见：加长林肯、奔驰、奥迪&amp;nbsp;　　当选择不再被视为选择，随着私家车市场的繁荣和婚车租赁业的兴起，二十一世纪的婚车故事太多，比如，加长林肯、奔驰、奥迪。\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500661),
(142, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/5?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚礼活动\",\"main_image\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg,\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg\",\"content\":\"&amp;nbsp;&amp;nbsp; &amp;nbsp;水下婚礼、自行车婚礼、公交车婚礼、骑马婚礼等，现在的年轻人越来越追求个性化的婚礼。昨日，又一场个性化的婚礼在西安上空上演，一对新婚小夫妻乘坐直升机在西安上空飞行20多分钟，高空宣言让亲友见证他们的甜蜜爱情。据知情人透露，这场婚礼仅租直升机的费用就在6万元，整场婚礼下来估计得20多万元。&amp;nbsp;&amp;nbsp; &amp;nbsp; 不得不承认的是，随着生活水平逐渐提高，结婚越来越奢侈，排场越来越大。现在的婚礼越来越演变成为“金钱”打造的舞台了。可话说回来，平心而论，现在一场婚礼要有绚丽的舞台、复杂的程序，花费自然也很高。对于即将步入婚宴殿堂且经济条件富有的新人来说，隆重而又奢华的婚礼容易统一大家的意见，也是象征着今后美满生活的开始。对于经济条件一般的人说，奢侈的婚礼势必给新人及其家庭造成一定的经济压力。因此，笔者认为我们应正确认识和对待奢侈婚礼，尊重个人消费方式的自由。&amp;nbsp;&amp;nbsp; &amp;nbsp; 过去，对于奢侈婚礼的我们谈都不敢谈，更不要说是动辄上万元的婚礼消费。而今，我们不仅敢谈，而且有能力消费奢。这是观念的转变、生活方式的转变。毕竟，结婚是两个人以及两个家庭的大事，操办婚礼绝都不会草率行事，中国的传统观念对于婚姻非常看重，承载了众多亲友的关注、企盼和祝福，对这个新家庭投入的多寡，彰显了当事人的重视程度。当然，婚礼只是一个形式、一个过程，与钻戒、婚纱、喜宴、新房相比，对这段婚姻的期许、祈福和维护更为重要，幸福的本质是朴实的，不是做给别人看的，不必为了所谓的“面子”而承受奢侈婚礼带来的经济压力。&amp;nbsp;　　基于此，笔者认为对于在婚礼上花费这个问题上，既不能一味地鼓励，也不能一棍子打死，而应该遵从“大道自然”。从新人的角度讲，要倡导广大新人们根据自家的财力制定婚礼的水准，简朴的裸婚和奢华的排场都无可厚非，量力而行才是最重要的。\"},\"ids\":\"5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500689),
(143, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/4?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚礼奏乐\",\"main_image\":\"\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg,\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg,\\/uploads\\/20201012\\/4a2d3208e64899131da88a57601094bb.jpg\",\"content\":\"&amp;nbsp;&amp;nbsp; &amp;nbsp; 台湾美魔女46岁伊能静21日在泰国普吉岛同小10岁的内地演员秦昊结婚，举行婚礼的现场照曝光，地点位于海边附近，配合周围树木同草地的环境，极具阳光气息，婚礼在乐古浪度假区举行，据请柬上显示，二人21日上午11点迎亲敬茶，下午两点外景拍摄，4点半举行婚礼仪式，夜晚7点半举行婚宴，10点半就有餐后酒会。&amp;nbsp;&amp;nbsp; &amp;nbsp; 昨日，伊能静发文分享婚礼现场母亲致辞等感人情景，并致谢所有亲友，感谢“一直接纳我如己出的秦先生家人”。&amp;nbsp;&amp;nbsp; &amp;nbsp; 伊能静称：“谢谢媒体朋友的关心，谢谢爱我的你们，如果没有你们，我不可能有今天的生活，也不可能在坑坑洞洞里依然相信善念的珍贵。我多希望能把自己的感恩，透过各种方式让你们知道，但最后也只能说谢谢。”&amp;nbsp;&amp;nbsp; &amp;nbsp; 网友纷纷评论：“一定要幸福”“难得你还保有对这个世界的信任，支持你”。&amp;nbsp;&amp;nbsp; &amp;nbsp; 伊能静发长微博原文：&amp;nbsp;&amp;nbsp; &amp;nbsp; 今天晚宴，爸爸妈妈在台上说，看着我们的婚礼，脑海里划过的却是秦先生从小到大的每个场景。&amp;nbsp;&amp;nbsp; &amp;nbsp; 然后是我母亲上台，说了感谢，身体微微地颤抖，她一向害羞，说完两句就哽咽。&amp;nbsp;&amp;nbsp; &amp;nbsp; 我也掉泪了，想到父亲不在的遗憾，母亲独立照顾我们的辛苦。这一天更多时候，我看到的是亲人的爱和温暖。&amp;nbsp;&amp;nbsp; &amp;nbsp; 谢谢妈妈、谢谢姐姐、谢谢闺蜜、谢谢挚友，谢谢帮助我们完成梦想的工作伙伴，更谢谢一直接纳我如己出的秦先生家人，能孝顺他们是我的福气。&amp;nbsp;&amp;nbsp; &amp;nbsp; 谢谢媒体朋友的关心，谢谢爱我的你们，如果没有你们，我不可能有今天的生活，也不可能在坑坑洞洞里依然相信善念的珍贵。&amp;nbsp;&amp;nbsp; &amp;nbsp; 我多希望能把自己的感恩，透过各种方式让你们知道，但最后也只能说谢谢。\"},\"ids\":\"4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500712),
(144, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/3?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"婚车装饰\",\"main_image\":\"\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg,\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg,\\/uploads\\/20201012\\/efbb65a3030ef826d8cd77cc6bbe3766.jpg\",\"content\":\"&amp;nbsp; &amp;nbsp; 据台湾媒体报道，周杰伦与昆凌1月18日在英国塞尔比教堂举行梦幻世纪婚礼，王子与公主的婚礼让各界相当羡慕，当时他也邀请了50位亲友参加，并豪气包下住宿机票，总花费2300万台币。由于外婆年纪大行动不便，加上台湾亲友众多，两人将于下周返台补请喜宴。在台北婚宴确定为2月9日在台北W Hotel举行，这场宾客将以演艺圈大哥大姐长辈为主，并且“不收礼金”。&amp;nbsp;&amp;nbsp; &amp;nbsp; 周杰伦与昆凌大婚后，人还在欧洲度蜜月，台湾这边的婚礼则以紧锣密鼓在筹备，2月9日的婚礼，预定邀请名单有张小燕、江蕙、张菲等，至于是否有邀请吴宗宪，他所属的杰威尔音乐表示，“宾客名单都由周妈妈全权决定。”&amp;nbsp;&amp;nbsp; &amp;nbsp; 先前一度传出，除了这场台湾的婚礼外，周杰伦另会邀请平辈举行结婚派对，杰威尔音乐则否认这个说法，表示他只会个别邀请他们吃饭，不会有结婚派对。至于台湾婚宴的风格，则会以轻松为主，并且不收礼金，也不邀请媒体进行拍摄，主要和朋友分享一下喜悦。\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500735),
(145, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/2?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"拍照摄像\",\"main_image\":\"\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg,\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg,\\/uploads\\/20201012\\/84725de86c6f1133e1d87836c81102c2.jpg\",\"content\":\"&amp;nbsp;&amp;nbsp; 用微信祝愿墙代替蛋糕塔，用二维码代替纸质请柬，自制主题微电影代替鲜花、“杯塔”、泡泡机……婚礼剔除高成本环节、突出新意，成为不少津城新人今秋办喜事的首选。市婚庆服务行业协会统计数据显示，明天起，本市进入“金九银十”婚庆高峰，10月底前全市将有超32000对新人举办婚礼。可喜的是，其中近半数准新人更加注重婚礼的创意，令婚礼成本大幅“瘦身”。&amp;nbsp;&amp;nbsp; &amp;nbsp; “我和爱人用丙烯颜料在T恤衫上画画，制作个性服装代替高档礼服；用无烟冷焰火代替花筒礼炮；提前联系车友接亲，不设豪华迎亲车队。在不降低亲友体验的同时，尽可能地压缩成本。”今天下午，就职于本市某金融机构的张强将和妻子小王举行结婚典礼。考察了多个婚庆机构后，二人最终选择的婚庆服务套餐价格不足6000元。小张告诉记者：“这两年参加了不少亲友的结婚典礼，花费都在一两万，甚至还有更贵的。我和爱人都不想砸钱讲排场办婚礼，办创意婚礼虽然要多花些心思，但亲力亲为的婚礼会给我们留下更多甜美的回忆。”&amp;nbsp;&amp;nbsp; &amp;nbsp; 据介绍，今年下半年的结婚高峰主要集中在9月20日至28日、10月2日至18日。婚礼数量虽多，但婚庆成本却在不断“降温”。据婚礼策划人张越介绍，不少婚宴的规模都压缩到10桌以下，“新人们的普遍要求是典礼形式简单、场面简约、有纪念意义，因此有创意的婚庆服务机构更有竞争力”。&amp;nbsp;&amp;nbsp; &amp;nbsp; 市婚庆服务行业协会潘树辉会长说，在全社会倡导节俭的大环境下，婚礼从大到小、花销由多变少，是一种健康向上的社会趋势。“这样的婚礼虽说环节和场面精简了，但突出亲情、凸显温情，杜绝了攀比之风和不必要的浪费，更利于新人婚后生活的和谐。”\"},\"ids\":\"2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500769),
(146, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/example/edit/ids/1?dialog=1', 'sdcmarry / 案例 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"live\",\"title\":\"现场布置\",\"main_image\":\"\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg\",\"swiper_images\":\"\\/uploads\\/20201012\\/7612bbee3314b71b6ef32eddbb14883f.jpg,\\/uploads\\/20201012\\/e8e9201aa563b03e5086dfd8584bb7e7.jpg,\\/uploads\\/20201012\\/a47d396fab6cf4af8f222d453ae6d185.jpg\",\"content\":\"&amp;nbsp; &amp;nbsp; 昨天下午，汤唯金泰勇宣布，两人正式结为夫妻。汤唯与韩籍夫婿金泰勇希望婚礼保持低调，但对于传统的婚嫁习俗仍是非常重视，包括派礼饼，量身定做礼服等重要环节。&amp;nbsp; &amp;nbsp; 嫁喜礼饼设计概念&amp;nbsp;&amp;nbsp; &amp;nbsp; 新人的嫁喜礼饼礼盒设计简单而别具心思。标志设计概念源于中国传统的双“囍”鸳鸯剪纸图案，并将一对新人的英文名字“TW”(代表汤唯Tang Wei) 及“TY”(代表泰勇Tae Yong)融入“囍”字当中。礼盒采用传统婚嫁喜事的红袍色为主调，配以烫金双囍标志。“囍”字上方两口化为心心图案，下方两口化为“W”及“Y”，而“W”及“Y”中间刚好是二合为一的“T”，左右拼成“TW”及“TY”，二人有如天作之合。“囍”字下面缀以鸳鸯图案，一对鸳鸯的嘴部恰巧形成一个心型图案，象征二人心心相印。而“W”、“T”、“Y”三字刚巧是左右对称，若以“T”字为旋转轴心，“W”及“Y”字盘旋下相互对称，二人翩翩起舞，融为一体，寓意二人一双一对，彼此爱念生生不息、无穷无尽。嫁喜礼饼从细节中展现一对新人爱意洋溢的幸福感觉。&amp;nbsp;&amp;nbsp; &amp;nbsp; 新郎度身订造结婚礼服&amp;nbsp;&amp;nbsp; &amp;nbsp; 准新郎金泰勇亦密锣紧鼓地为大日子做足准备，特地去到曾为前美国总统尼克逊(Richard M. Nixon)、布殊(George H. W. Bush)及荷里活巨星瑞安纳度-狄卡比奥(Leonardo DiCaprio)、马龙-白兰度(Marlon Brando) 订制洋服的本地高级男士订制洋服品牌度身订造结婚礼服。礼服的细节、设计及取材皆一丝不苟，除了以品牌首屈一指的度身订造服务所制成外，采用的面料亦为最优质。在度身期间，在场各人皆从金泰勇脸上的幸福笑容中，感受到其将为人夫的喜悦及兴奋心情。&amp;nbsp;&amp;nbsp; &amp;nbsp; 汤唯与金泰勇因对电影的热诚而结缘，继而成为夫妇，决以伴侣身份继续追逐电影梦。\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602500793),
(147, 0, 'Unknown', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php', '', '{\"url\":\"\\/luKUFZaHjE.php\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"onpa\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602733330),
(148, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php', '登录', '{\"url\":\"\\/luKUFZaHjE.php\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"5djh\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602733336),
(149, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"mp\":\"{&quot;app_id&quot;:&quot;wx6416b591e4dfe7f9&quot;,&quot;app_secret&quot;:&quot;47fbeb620154b6856db356dbe7d78ccf&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u5bfc\\\\u822a&quot;,&quot;post&quot;:&quot;\\\\u6587\\\\u7ae0\\\\u5217\\\\u8868\\\\u5bfc\\\\u822a&quot;,&quot;example&quot;:&quot;\\\\u6848\\\\u4f8b\\\\u5bfc\\\\u822a&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u7ed3\\\\u5a5a\\\\u653b\\\\u7565&quot;,&quot;hybk&quot;:&quot;\\\\u5a5a\\\\u59fb\\\\u767e\\\\u79d1&quot;}\",\"example\":\"{&quot;live&quot;:&quot;\\\\u73b0\\\\u573a\\\\u6848\\\\u4f8b&quot;,&quot;marry&quot;:&quot;\\\\u5a5a\\\\u793c\\\\u6848\\\\u4f8b&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602733400),
(150, 1, 'admin', '/luKUFZaHjE.php/user/user/del', '会员管理 / 会员管理 / 删除', '{\"action\":\"del\",\"ids\":\"2\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602735150),
(151, 1, 'admin', '/luKUFZaHjE.php/user/user/edit/ids/3?dialog=1', '会员管理 / 会员管理 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"id\":\"3\",\"group_id\":\"1\",\"username\":\"u3\",\"nickname\":\"summer\",\"password\":\"***\",\"email\":\"u3@hunqing.sdc-dev.top\",\"mobile\":\"\",\"avatar\":\"https:\\/\\/thirdwx.qlogo.cn\\/mmopen\\/vi_32\\/ricV7H8iayA8eFqsPyRjAcdEZnzibbfq44ibPIDWDdo37Aa792N85uaPrZ6refgSMNuJCxAAK1YIztyZWiaIrDSwrug\\/132\",\"level\":\"1\",\"gender\":\"0\",\"birthday\":\"\",\"bio\":\"\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2020-10-15 12:38:37\",\"logintime\":\"2020-10-15 18:42:25\",\"loginip\":\"127.0.0.1\",\"loginfailure\":\"0\",\"joinip\":\"127.0.0.1\",\"jointime\":\"2020-10-15 12:11:15\",\"status\":\"normal\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602776991),
(152, 1, 'admin', '/luKUFZaHjE.php/user/user/edit/ids/3?dialog=1', '会员管理 / 会员管理 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"id\":\"3\",\"group_id\":\"1\",\"username\":\"u3\",\"nickname\":\"summer\",\"password\":\"***\",\"email\":\"u3@hunqing.sdc-dev.top\",\"mobile\":\"\",\"avatar\":\"https:\\/\\/thirdwx.qlogo.cn\\/mmopen\\/vi_32\\/ricV7H8iayA8eFqsPyRjAcdEZnzibbfq44ibPIDWDdo37Aa792N85uaPrZ6refgSMNuJCxAAK1YIztyZWiaIrDSwrug\\/132\",\"level\":\"1\",\"gender\":\"0\",\"birthday\":\"\",\"bio\":\"\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2020-10-15 12:38:37\",\"logintime\":\"2020-10-15 18:42:25\",\"loginip\":\"127.0.0.1\",\"loginfailure\":\"0\",\"joinip\":\"127.0.0.1\",\"jointime\":\"2020-10-15 12:11:15\",\"status\":\"normal\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602777000),
(153, 1, 'admin', '/luKUFZaHjE.php/user/user/edit/ids/3?dialog=1', '会员管理 / 会员管理 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"id\":\"3\",\"group_id\":\"1\",\"username\":\"u333\",\"nickname\":\"summer\",\"password\":\"***\",\"email\":\"u3@hunqing.sdc-dev.top\",\"mobile\":\"15500785170\",\"avatar\":\"https:\\/\\/thirdwx.qlogo.cn\\/mmopen\\/vi_32\\/ricV7H8iayA8eFqsPyRjAcdEZnzibbfq44ibPIDWDdo37Aa792N85uaPrZ6refgSMNuJCxAAK1YIztyZWiaIrDSwrug\\/132\",\"level\":\"1\",\"gender\":\"0\",\"birthday\":\"\",\"bio\":\"\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2020-10-15 12:38:37\",\"logintime\":\"2020-10-15 18:42:25\",\"loginip\":\"127.0.0.1\",\"loginfailure\":\"0\",\"joinip\":\"127.0.0.1\",\"jointime\":\"2020-10-15 12:11:15\",\"status\":\"normal\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602777034),
(154, 1, 'admin', '/luKUFZaHjE.php/user/user/edit/ids/3?dialog=1', '会员管理 / 会员管理 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"id\":\"3\",\"group_id\":\"1\",\"username\":\"u333\",\"nickname\":\"summer\",\"password\":\"***\",\"email\":\"u3@hunqing.sdc-dev.top\",\"mobile\":\"\",\"avatar\":\"https:\\/\\/thirdwx.qlogo.cn\\/mmopen\\/vi_32\\/ricV7H8iayA8eFqsPyRjAcdEZnzibbfq44ibPIDWDdo37Aa792N85uaPrZ6refgSMNuJCxAAK1YIztyZWiaIrDSwrug\\/132\",\"level\":\"1\",\"gender\":\"0\",\"birthday\":\"\",\"bio\":\"\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2020-10-15 12:38:37\",\"logintime\":\"2020-10-15 18:42:25\",\"loginip\":\"127.0.0.1\",\"loginfailure\":\"0\",\"joinip\":\"127.0.0.1\",\"jointime\":\"2020-10-15 12:11:15\",\"status\":\"normal\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602777046),
(155, 1, 'admin', '/luKUFZaHjE.php/user/user/del', '会员管理 / 会员管理 / 删除', '{\"action\":\"del\",\"ids\":\"3\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778444),
(156, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/third/del', 'sdcmarry / 第三方登录管理 / 删除', '{\"action\":\"del\",\"ids\":\"1\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778450),
(157, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/third/del', 'sdcmarry / 第三方登录管理 / 删除', '{\"action\":\"del\",\"ids\":\"2\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778507),
(158, 1, 'admin', '/luKUFZaHjE.php/user/user/del', '会员管理 / 会员管理 / 删除', '{\"action\":\"del\",\"ids\":\"4\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778515),
(159, 1, 'admin', '/luKUFZaHjE.php/user/user/del', '会员管理 / 会员管理 / 删除', '{\"action\":\"del\",\"ids\":\"1\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778701),
(160, 1, 'admin', '/luKUFZaHjE.php/user/user/del', '会员管理 / 会员管理 / 删除', '{\"action\":\"del\",\"ids\":\"5\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778705),
(161, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/third/del', 'sdcmarry / 第三方登录管理 / 删除', '{\"action\":\"del\",\"ids\":\"3\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602778713),
(162, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', 'Login', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"vpaf\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602814978),
(163, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/order/del', 'sdcmarry / 订单管理 / 删除', '{\"action\":\"del\",\"ids\":\"1\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1602846356),
(164, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php%2Fdashboard%3Fref%3Daddtabs', 'Login', '{\"url\":\"\\/luKUFZaHjE.php\\/dashboard?ref=addtabs\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"keln\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602905330),
(165, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/order/del', 'sdcmarry / 订单管理 / 删除', '{\"action\":\"del\",\"ids\":\"13,12,11,10,9,8,7,6,5,4\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602905343),
(166, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/order/del', 'sdcmarry / 订单管理 / 删除', '{\"action\":\"del\",\"ids\":\"3,2\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602905351),
(167, 1, 'admin', '/luKUFZaHjE.php/index/login?url=%2FluKUFZaHjE.php', '登录', '{\"url\":\"\\/luKUFZaHjE.php\",\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"NNJX\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', 1602907403),
(168, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"tel\":\"0971-2434510\",\"address\":\"青海省西宁市湟源县蒙古道村\",\"wechat\":\"xandy-zx\",\"email\":\"admin@sdc-studio.cn\",\"mp\":\"{&quot;app_id&quot;:&quot;wx6416b591e4dfe7f9&quot;,&quot;app_secret&quot;:&quot;47fbeb620154b6856db356dbe7d78ccf&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u5bfc\\\\u822a&quot;,&quot;post&quot;:&quot;\\\\u6587\\\\u7ae0\\\\u5217\\\\u8868\\\\u5bfc\\\\u822a&quot;,&quot;example&quot;:&quot;\\\\u6848\\\\u4f8b\\\\u5bfc\\\\u822a&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u7ed3\\\\u5a5a\\\\u653b\\\\u7565&quot;,&quot;hybk&quot;:&quot;\\\\u5a5a\\\\u59fb\\\\u767e\\\\u79d1&quot;}\",\"example\":\"{&quot;live&quot;:&quot;\\\\u73b0\\\\u573a\\\\u6848\\\\u4f8b&quot;,&quot;marry&quot;:&quot;\\\\u5a5a\\\\u793c\\\\u6848\\\\u4f8b&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602917148),
(169, 1, 'admin', '/luKUFZaHjE.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602948000),
(170, 1, 'admin', '/luKUFZaHjE.php/addon/config?name=sdcmarry&dialog=1', '插件管理 / 配置', '{\"name\":\"sdcmarry\",\"dialog\":\"1\",\"row\":{\"tel\":\"0971-2434510\",\"address\":\"青海省西宁市湟源县蒙古道村\",\"wechat\":\"xandy-zx\",\"email\":\"admin@sdc-studio.cn\",\"about_image\":\"\\/uploads\\/20201017\\/8890254019c5c3b8c9e9c99ca4269dbc.jpg\",\"mp\":\"{&quot;app_id&quot;:&quot;wx6416b591e4dfe7f9&quot;,&quot;app_secret&quot;:&quot;47fbeb620154b6856db356dbe7d78ccf&quot;,&quot;scope&quot;:&quot;snsapi_userinfo&quot;}\",\"carousel\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u8f6e\\\\u64ad\\\\u56fe&quot;}\",\"tab\":\"{&quot;home&quot;:&quot;\\\\u9996\\\\u9875\\\\u5bfc\\\\u822a&quot;,&quot;post&quot;:&quot;\\\\u6587\\\\u7ae0\\\\u5217\\\\u8868\\\\u5bfc\\\\u822a&quot;,&quot;example&quot;:&quot;\\\\u6848\\\\u4f8b\\\\u5bfc\\\\u822a&quot;}\",\"service\":\"{&quot;main&quot;:&quot;\\\\u4e3b\\\\u8981\\\\u670d\\\\u52a1&quot;}\",\"post\":\"{&quot;news&quot;:&quot;\\\\u7ed3\\\\u5a5a\\\\u653b\\\\u7565&quot;,&quot;hybk&quot;:&quot;\\\\u5a5a\\\\u59fb\\\\u767e\\\\u79d1&quot;}\",\"example\":\"{&quot;live&quot;:&quot;\\\\u73b0\\\\u573a\\\\u6848\\\\u4f8b&quot;,&quot;marry&quot;:&quot;\\\\u5a5a\\\\u793c\\\\u6848\\\\u4f8b&quot;}\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602948003),
(171, 1, 'admin', '/luKUFZaHjE.php/sdcmarry/tab/edit/ids/12?dialog=1', 'sdcmarry / 标签导航 / 编辑', '{\"dialog\":\"1\",\"row\":{\"category\":\"home\",\"title\":\"联系我们\",\"image\":\"\\/uploads\\/20201006\\/08aa32ce64e40aee2987c226d67034ad.png\",\"path\":\"\\/pages\\/about\\/index\",\"options\":\"\"},\"ids\":\"12\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38', 1602948346);

-- --------------------------------------------------------

--
-- Table structure for table `fa_area`
--

CREATE TABLE `fa_area` (
  `id` int(10) NOT NULL COMMENT 'ID',
  `pid` int(10) DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简称',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) DEFAULT NULL COMMENT '层级 0 1 2 省市区县',
  `pinyin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纬度'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='地区表';

-- --------------------------------------------------------

--
-- Table structure for table `fa_attachment`
--

CREATE TABLE `fa_attachment` (
  `id` int(20) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件名称',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件 sha1编码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件表';

--
-- Dumping data for table `fa_attachment`
--

INSERT INTO `fa_attachment` (`id`, `admin_id`, `user_id`, `url`, `imagewidth`, `imageheight`, `imagetype`, `imageframes`, `filename`, `filesize`, `mimetype`, `extparam`, `createtime`, `updatetime`, `uploadtime`, `storage`, `sha1`) VALUES
(1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 'qrcode.png', 21859, 'image/png', '', 1499681848, 1499681848, 1499681848, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6'),
(2, 1, 0, '/uploads/20201006/497f4a60d1f89a443d4b4022606e282b.jpg', '750', '300', 'jpg', 0, 'carousel.jpg', 111865, 'image/jpeg', '', 1601957717, 1601957717, 1601957717, 'local', '4480f4d643e6939461ce448051d36657a0fcf90a'),
(3, 1, 0, '/uploads/20201006/e1d8c85d9804cc3a2f95777e6e5597f9.jpg', '750', '300', 'jpg', 0, 'carousel_1.jpg', 36468, 'image/jpeg', '', 1601957729, 1601957729, 1601957729, 'local', 'e092119f30f8ca20d7fd3b6005120cbe54509985'),
(4, 1, 0, '/uploads/20201006/f073e13efa01849d5e64f7ea47cc6269.jpg', '750', '300', 'jpg', 0, 'carousel_2.jpg', 38682, 'image/jpeg', '', 1601957762, 1601957762, 1601957762, 'local', '89c747b535e97036f4fbe3880002fe351745ecd3'),
(5, 1, 0, '/uploads/20201006/00803bb295bc445ea7339ff474762829.jpg', '95', '87', 'jpg', 0, '首页_03.jpg', 1981, 'image/jpeg', '', 1601970542, 1601970542, 1601970542, 'local', '5f957f80f1577b8af1aac73625249e6e82c7f75e'),
(6, 1, 0, '/uploads/20201006/69653bb996a590296507e4e1c45c9b5a.png', '84', '86', 'png', 0, '微信截图_20201006231624_03.png', 7451, 'image/png', '', 1601997862, 1601997862, 1601997862, 'local', 'b01ec82f955aa7b6c11924c5db0b3fcce6bc0a20'),
(7, 1, 0, '/uploads/20201006/12fa23594787473531b746cd26af98a9.png', '84', '86', 'png', 0, '微信截图_20201006231624_05.png', 8036, 'image/png', '', 1601997893, 1601997893, 1601997893, 'local', '7a825d3c70e82cc400b4c4b32f4e3f75726fc1fe'),
(8, 1, 0, '/uploads/20201006/bbf11d4a95d99120ad0ca6c04ae431fd.png', '84', '86', 'png', 0, '微信截图_20201006231624_07.png', 7695, 'image/png', '', 1601997916, 1601997916, 1601997916, 'local', '6ae338016930fcb717f7a98da3e4fd69487120e8'),
(9, 1, 0, '/uploads/20201006/6be7a0b21482725b247ac7d0eaf07dce.png', '84', '86', 'png', 0, '微信截图_20201006231624_09.png', 7682, 'image/png', '', 1601997943, 1601997943, 1601997943, 'local', '755dd88dceee6bc675d8b912b3b1b88d242897dd'),
(10, 1, 0, '/uploads/20201006/aeff78cc2662833f583ec84ee2644afe.png', '84', '86', 'png', 0, '微信截图_20201006231624_16.png', 7653, 'image/png', '', 1601997967, 1601997967, 1601997967, 'local', 'b997008943f2026ac55a2526bb8c4635f4aa9cb0'),
(11, 1, 0, '/uploads/20201006/bc833438bebd09b84aad4d873f3caaaf.png', '84', '86', 'png', 0, '微信截图_20201006231624_18.png', 7636, 'image/png', '', 1601997990, 1601997990, 1601997990, 'local', '431420001944955df4201016748bdb1d30250033'),
(12, 1, 0, '/uploads/20201006/94a7f5ff63784391ab5e90572c26051a.png', '84', '86', 'png', 0, '微信截图_20201006231624_19.png', 7640, 'image/png', '', 1601998025, 1601998025, 1601998025, 'local', 'af602e917415ac4c248e1c96a77a83438173f582'),
(13, 1, 0, '/uploads/20201006/08aa32ce64e40aee2987c226d67034ad.png', '84', '86', 'png', 0, '微信截图_20201006231624_20.png', 7729, 'image/png', '', 1601998048, 1601998048, 1601998048, 'local', 'ce86ed6768a7004d183c257b98f09a1cdd40b0c6'),
(14, 1, 0, '/uploads/20201009/8890254019c5c3b8c9e9c99ca4269dbc.jpg', '750', '374', 'jpg', 0, 'pexels-jill-wellington-302552.jpg', 52608, 'image/jpeg', '', 1602212633, 1602212633, 1602212633, 'local', 'f7b27fbdba852dde08293207dc86876f46cecdd9'),
(15, 1, 0, '/uploads/20201009/890dda29f738474d23f8a8ce10b2b056.jpg', '750', '374', 'jpg', 0, 'pexels-freestocksorg-70737.jpg', 58124, 'image/jpeg', '', 1602212870, 1602212870, 1602212870, 'local', 'c7a7d25645d49ec00ed83a2e367da0185402b806'),
(16, 1, 0, '/uploads/20201009/fca931712e97cef40e68a99ffa142f80.jpg', '750', '374', 'jpg', 0, 'pexels-neosiam-579902.jpg', 21428, 'image/jpeg', '', 1602212970, 1602212970, 1602212970, 'local', '3d704864eafac1721ecc86ddb5fa1b47f913891b'),
(17, 1, 0, '/uploads/20201009/8bb2b2e75f47d7f9a696e512907177db.jpg', '231', '237', 'jpg', 0, 'timg.jpg', 8348, 'image/jpeg', '', 1602256559, 1602256559, 1602256559, 'local', '71439aa88c2d7666f71ffab6502e4d99d6ea9d7f'),
(18, 1, 0, '/uploads/20201009/6fb06c1b10982d5eae781f1748ada5fe.jpg', '460', '305', 'jpg', 0, 'list (1).jpg', 29213, 'image/jpeg', '', 1602257556, 1602257556, 1602257556, 'local', '94947ef58e3acfe9628dba004dddd37c2bf162be'),
(19, 1, 0, '/uploads/20201009/4dc1d65de0be85512daaa45f52c2e384.jpg', '460', '307', 'jpg', 0, 'list (2).jpg', 41862, 'image/jpeg', '', 1602257619, 1602257619, 1602257619, 'local', '65c9e88d4218d72ff72679530bfe0e5cb8a95431'),
(20, 1, 0, '/uploads/20201009/325b6975698a34756cb4ce43ab4255e0.jpg', '460', '307', 'jpg', 0, 'list (3).jpg', 37533, 'image/jpeg', '', 1602257688, 1602257688, 1602257688, 'local', 'c889dd2104a4c8753f26551a7939d4ab7e3e2ac8'),
(21, 1, 0, '/uploads/20201009/b0cfdf3fb1e6e005825ab098f71bb3a4.jpg', '460', '307', 'jpg', 0, 'list (4).jpg', 32993, 'image/jpeg', '', 1602257732, 1602257732, 1602257732, 'local', 'cf82429942ee08fbda49226ff5729f19426aa490'),
(22, 1, 0, '/uploads/20201009/f2b8591a6ca7ab1af0ca9d9900950c25.jpg', '460', '307', 'jpg', 0, 'list (5).jpg', 22601, 'image/jpeg', '', 1602257810, 1602257810, 1602257810, 'local', 'ebc77ed51d6f020c577563237ba84fcf90150b40'),
(23, 1, 0, '/uploads/20201009/ff8d87fabf6805571f76f00bc5d64017.jpg', '460', '307', 'jpg', 0, 'list (6).jpg', 24045, 'image/jpeg', '', 1602257932, 1602257932, 1602257932, 'local', '7c4314d0df8235a61597fba06ebbc3512f8870dd'),
(24, 1, 0, '/uploads/20201009/b1f80c0e7260e037d2cee61ff107eeef.jpg', '460', '307', 'jpg', 0, 'list (7).jpg', 22842, 'image/jpeg', '', 1602257992, 1602257992, 1602257992, 'local', '6a73aaa5fc21bc0dc320290fc956a5bf6435cfd4'),
(25, 1, 0, '/uploads/20201009/c09c430c465f97531f581113611b2899.jpg', '460', '307', 'jpg', 0, 'list (8).jpg', 18174, 'image/jpeg', '', 1602258048, 1602258048, 1602258048, 'local', 'c07e796b5376f1d786f99b27c66bc196fa0c0b38'),
(26, 1, 0, '/uploads/20201012/6fb06c1b10982d5eae781f1748ada5fe.jpg', '460', '305', 'jpg', 0, 'list (1).jpg', 29213, 'image/jpeg', '', 1602480604, 1602480604, 1602480604, 'local', '94947ef58e3acfe9628dba004dddd37c2bf162be'),
(27, 1, 0, '/uploads/20201012/4dc1d65de0be85512daaa45f52c2e384.jpg', '460', '307', 'jpg', 0, 'list (2).jpg', 41862, 'image/jpeg', '', 1602480612, 1602480612, 1602480612, 'local', '65c9e88d4218d72ff72679530bfe0e5cb8a95431'),
(28, 1, 0, '/uploads/20201012/325b6975698a34756cb4ce43ab4255e0.jpg', '460', '307', 'jpg', 0, 'list (3).jpg', 37533, 'image/jpeg', '', 1602480612, 1602480612, 1602480612, 'local', 'c889dd2104a4c8753f26551a7939d4ab7e3e2ac8'),
(29, 1, 0, '/uploads/20201012/7612bbee3314b71b6ef32eddbb14883f.jpg', '307', '307', 'jpg', 0, 'list-(2).jpg', 39016, 'image/jpeg', '', 1602481131, 1602481131, 1602481131, 'local', 'cff000154b92c0ce4b984df7f8ba05a85c51b9a6'),
(30, 1, 0, '/uploads/20201012/e8e9201aa563b03e5086dfd8584bb7e7.jpg', '307', '307', 'jpg', 0, 'list-(3).jpg', 32706, 'image/jpeg', '', 1602481148, 1602481148, 1602481148, 'local', '2a59650d1e6d1d985e1057b66d0539f297130fd2'),
(31, 1, 0, '/uploads/20201012/a47d396fab6cf4af8f222d453ae6d185.jpg', '307', '307', 'jpg', 0, 'list-(4).jpg', 27738, 'image/jpeg', '', 1602481148, 1602481148, 1602481148, 'local', '169e175333aed4bd947d3bec5182bfcbb06fd6f5'),
(32, 1, 0, '/uploads/20201012/84725de86c6f1133e1d87836c81102c2.jpg', '307', '307', 'jpg', 0, 'list-(5).jpg', 22931, 'image/jpeg', '', 1602481208, 1602481208, 1602481208, 'local', 'a97d27d581762f0d49f983ea22503c064c2130f2'),
(33, 1, 0, '/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg', '307', '307', 'jpg', 0, 'list-(6).jpg', 24451, 'image/jpeg', '', 1602481246, 1602481246, 1602481246, 'local', 'a068bca32edadecae7e44ce2bd56f9d8fd0ce59a'),
(34, 1, 0, '/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg', '307', '307', 'jpg', 0, 'list-(7).jpg', 21902, 'image/jpeg', '', 1602481276, 1602481276, 1602481276, 'local', '8c4e0ee872fdcef3a89d878370440dacebd79d98'),
(35, 1, 0, '/uploads/20201012/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg', '307', '307', 'jpg', 0, 'list-(8).jpg', 18712, 'image/jpeg', '', 1602481305, 1602481305, 1602481305, 'local', '4a2cfea14810285c77cd999bb9a81acab451bb0f'),
(36, 1, 0, '/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg', '345', '345', 'jpg', 0, 'list.jpg', 19173, 'image/jpeg', '', 1602481346, 1602481346, 1602481346, 'local', '1ffee13feae9a9c7fdf17a4380e0e7a92988ec7f'),
(37, 1, 0, '/uploads/20201012/f2fe55b616856a0930d71f64f6299857.jpg', '374', '374', 'jpg', 0, 'pexels-freestocksorg-70737-恢复的.jpg', 27552, 'image/jpeg', '', 1602481383, 1602481383, 1602481383, 'local', 'f59cbdd58261748fcad9d3c95ca0cab741bb57e1'),
(38, 1, 0, '/uploads/20201017/8890254019c5c3b8c9e9c99ca4269dbc.jpg', '750', '374', 'jpg', 0, 'pexels-jill-wellington-302552.jpg', 52608, 'image/jpeg', '', 1602948000, 1602948000, 1602948000, 'local', 'f7b27fbdba852dde08293207dc86876f46cecdd9');

-- --------------------------------------------------------

--
-- Table structure for table `fa_auth_group`
--

CREATE TABLE `fa_auth_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分组表';

--
-- Dumping data for table `fa_auth_group`
--

INSERT INTO `fa_auth_group` (`id`, `pid`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, 0, 'Admin group', '*', 1490883540, 149088354, 'normal'),
(2, 1, 'Second group', '13,14,16,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,1,9,10,11,7,6,8,2,4,5', 1490883540, 1505465692, 'normal'),
(3, 2, 'Third group', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,5', 1490883540, 1502205322, 'normal'),
(4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1490883540, 1502205350, 'normal'),
(5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1490883540, 1502205344, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_auth_group_access`
--

CREATE TABLE `fa_auth_group_access` (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限分组表';

--
-- Dumping data for table `fa_auth_group_access`
--

INSERT INTO `fa_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fa_auth_rule`
--

CREATE TABLE `fa_auth_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('menu','file') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图标',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '条件',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='节点表';

--
-- Dumping data for table `fa_auth_rule`
--

INSERT INTO `fa_auth_rule` (`id`, `type`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', 'Dashboard tips', 1, 1497429920, 1497429920, 143, 'normal'),
(2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', 1, 1497429920, 1497430169, 137, 'normal'),
(3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', 'Category tips', 1, 1497429920, 1497429920, 119, 'normal'),
(4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', 'Addon tips', 1, 1502035509, 1502035509, 0, 'normal'),
(5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', 1, 1497429920, 1497430092, 99, 'normal'),
(6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', 'Config tips', 1, 1497429920, 1497430683, 60, 'normal'),
(7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', 'Attachment tips', 1, 1497429920, 1497430699, 53, 'normal'),
(8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', 1, 1497429920, 1497429920, 34, 'normal'),
(9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', 'Admin tips', 1, 1497429920, 1497430320, 118, 'normal'),
(10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', 'Admin log tips', 1, 1497429920, 1497430307, 113, 'normal'),
(11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', 'Group tips', 1, 1497429920, 1497429920, 109, 'normal'),
(12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', 'Rule tips', 1, 1497429920, 1497430581, 104, 'normal'),
(13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 136, 'normal'),
(14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 135, 'normal'),
(15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 133, 'normal'),
(16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 134, 'normal'),
(17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 132, 'normal'),
(18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 52, 'normal'),
(19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 51, 'normal'),
(20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 50, 'normal'),
(21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 49, 'normal'),
(22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 48, 'normal'),
(23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', 'Attachment tips', 0, 1497429920, 1497429920, 59, 'normal'),
(24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 58, 'normal'),
(25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 57, 'normal'),
(26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 56, 'normal'),
(27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 55, 'normal'),
(28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 54, 'normal'),
(29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 33, 'normal'),
(30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 32, 'normal'),
(31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 31, 'normal'),
(32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 30, 'normal'),
(33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 29, 'normal'),
(34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 28, 'normal'),
(35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', 'Category tips', 0, 1497429920, 1497429920, 142, 'normal'),
(36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 141, 'normal'),
(37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 140, 'normal'),
(38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 139, 'normal'),
(39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 138, 'normal'),
(40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', 'Admin tips', 0, 1497429920, 1497429920, 117, 'normal'),
(41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 116, 'normal'),
(42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 115, 'normal'),
(43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 114, 'normal'),
(44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', 'Admin log tips', 0, 1497429920, 1497429920, 112, 'normal'),
(45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 111, 'normal'),
(46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 110, 'normal'),
(47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', 'Group tips', 0, 1497429920, 1497429920, 108, 'normal'),
(48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 107, 'normal'),
(49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 106, 'normal'),
(50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 105, 'normal'),
(51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', 'Rule tips', 0, 1497429920, 1497429920, 103, 'normal'),
(52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 102, 'normal'),
(53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 101, 'normal'),
(54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 100, 'normal'),
(55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', 'Addon tips', 0, 1502035509, 1502035509, 0, 'normal'),
(56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(85, 'file', 0, 'command', '在线命令管理', 'fa fa-terminal', '', '', 1, 1601906746, 1601906746, 0, 'normal'),
(86, 'file', 85, 'command/index', '查看', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(87, 'file', 85, 'command/add', '添加', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(88, 'file', 85, 'command/detail', '详情', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(89, 'file', 85, 'command/execute', '运行', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(90, 'file', 85, 'command/del', '删除', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(91, 'file', 85, 'command/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1601906746, 1601906746, 0, 'normal'),
(92, 'file', 0, 'third', '第三方登录管理', 'fa fa-users', '', '', 1, 1601907193, 1601907193, 0, 'normal'),
(93, 'file', 92, 'third/index', '查看', 'fa fa-circle-o', '', '', 0, 1601907193, 1601907193, 0, 'normal'),
(94, 'file', 92, 'third/del', '删除', 'fa fa-circle-o', '', '', 0, 1601907193, 1601907193, 0, 'normal'),
(95, 'file', 0, 'sdcmarry', 'sdcmarry', 'fa fa-list', '', '', 1, 1601956928, 1601956928, 0, 'normal'),
(96, 'file', 95, 'sdcmarry/carousel', '轮播图', 'fa fa-circle-o', '', '', 1, 1601956928, 1601956928, 0, 'normal'),
(97, 'file', 96, 'sdcmarry/carousel/import', 'Import', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(98, 'file', 96, 'sdcmarry/carousel/index', '查看', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(99, 'file', 96, 'sdcmarry/carousel/add', '添加', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(100, 'file', 96, 'sdcmarry/carousel/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(101, 'file', 96, 'sdcmarry/carousel/del', '删除', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(102, 'file', 96, 'sdcmarry/carousel/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1601956928, 1601956928, 0, 'normal'),
(103, 'file', 95, 'sdcmarry/tab', '标签导航', 'fa fa-circle-o', '', '', 1, 1601961141, 1601961141, 0, 'normal'),
(104, 'file', 103, 'sdcmarry/tab/import', 'Import', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(105, 'file', 103, 'sdcmarry/tab/index', '查看', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(106, 'file', 103, 'sdcmarry/tab/add', '添加', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(107, 'file', 103, 'sdcmarry/tab/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(108, 'file', 103, 'sdcmarry/tab/del', '删除', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(109, 'file', 103, 'sdcmarry/tab/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1601961141, 1601961141, 0, 'normal'),
(110, 'file', 95, 'sdcmarry/service', '服务', 'fa fa-circle-o', '', '', 1, 1601972779, 1601972779, 0, 'normal'),
(111, 'file', 110, 'sdcmarry/service/import', 'Import', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(112, 'file', 110, 'sdcmarry/service/index', '查看', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(113, 'file', 110, 'sdcmarry/service/add', '添加', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(114, 'file', 110, 'sdcmarry/service/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(115, 'file', 110, 'sdcmarry/service/del', '删除', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(116, 'file', 110, 'sdcmarry/service/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1601972779, 1601972779, 0, 'normal'),
(117, 'file', 95, 'sdcmarry/post', '文章', 'fa fa-circle-o', '', '', 1, 1602255444, 1602255444, 0, 'normal'),
(118, 'file', 117, 'sdcmarry/post/import', 'Import', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(119, 'file', 117, 'sdcmarry/post/index', '查看', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(120, 'file', 117, 'sdcmarry/post/add', '添加', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(121, 'file', 117, 'sdcmarry/post/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(122, 'file', 117, 'sdcmarry/post/del', '删除', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(123, 'file', 117, 'sdcmarry/post/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1602255444, 1602255444, 0, 'normal'),
(124, 'file', 95, 'sdcmarry/example', '案例', 'fa fa-circle-o', '', '', 1, 1602479676, 1602479676, 0, 'normal'),
(125, 'file', 124, 'sdcmarry/example/import', 'Import', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(126, 'file', 124, 'sdcmarry/example/index', '查看', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(127, 'file', 124, 'sdcmarry/example/add', '添加', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(128, 'file', 124, 'sdcmarry/example/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(129, 'file', 124, 'sdcmarry/example/del', '删除', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(130, 'file', 124, 'sdcmarry/example/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1602479676, 1602479676, 0, 'normal'),
(131, 'file', 95, 'sdcmarry/third', '第三方登录管理', 'fa fa-circle-o', '', '', 1, 1602734404, 1602734404, 0, 'normal'),
(132, 'file', 131, 'sdcmarry/third/import', 'Import', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(133, 'file', 131, 'sdcmarry/third/index', '查看', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(134, 'file', 131, 'sdcmarry/third/add', '添加', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(135, 'file', 131, 'sdcmarry/third/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(136, 'file', 131, 'sdcmarry/third/del', '删除', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(137, 'file', 131, 'sdcmarry/third/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1602734404, 1602734404, 0, 'normal'),
(138, 'file', 95, 'sdcmarry/order', '订单管理', 'fa fa-circle-o', '', '', 1, 1602814730, 1602814730, 0, 'normal'),
(139, 'file', 138, 'sdcmarry/order/import', 'Import', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal'),
(140, 'file', 138, 'sdcmarry/order/index', '查看', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal'),
(141, 'file', 138, 'sdcmarry/order/add', '添加', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal'),
(142, 'file', 138, 'sdcmarry/order/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal'),
(143, 'file', 138, 'sdcmarry/order/del', '删除', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal'),
(144, 'file', 138, 'sdcmarry/order/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1602814730, 1602814730, 0, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_category`
--

CREATE TABLE `fa_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分类表';

--
-- Dumping data for table `fa_category`
--

INSERT INTO `fa_category` (`id`, `pid`, `type`, `name`, `nickname`, `flag`, `image`, `keywords`, `description`, `diyname`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 0, 'page', '官方新闻', 'news', 'recommend', '/assets/img/qrcode.png', '', '', 'news', 1495262190, 1495262190, 1, 'normal'),
(2, 0, 'page', '移动应用', 'mobileapp', 'hot', '/assets/img/qrcode.png', '', '', 'mobileapp', 1495262244, 1495262244, 2, 'normal'),
(3, 2, 'page', '微信公众号', 'wechatpublic', 'index', '/assets/img/qrcode.png', '', '', 'wechatpublic', 1495262288, 1495262288, 3, 'normal'),
(4, 2, 'page', 'Android开发', 'android', 'recommend', '/assets/img/qrcode.png', '', '', 'android', 1495262317, 1495262317, 4, 'normal'),
(5, 0, 'page', '软件产品', 'software', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1495262336, 1499681850, 5, 'normal'),
(6, 5, 'page', '网站建站', 'website', 'recommend', '/assets/img/qrcode.png', '', '', 'website', 1495262357, 1495262357, 6, 'normal'),
(7, 5, 'page', '企业管理软件', 'company', 'index', '/assets/img/qrcode.png', '', '', 'company', 1495262391, 1495262391, 7, 'normal'),
(8, 6, 'page', 'PC端', 'website-pc', 'recommend', '/assets/img/qrcode.png', '', '', 'website-pc', 1495262424, 1495262424, 8, 'normal'),
(9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1495262456, 1495262456, 9, 'normal'),
(10, 7, 'page', 'CRM系统 ', 'company-crm', 'recommend', '/assets/img/qrcode.png', '', '', 'company-crm', 1495262487, 1495262487, 10, 'normal'),
(11, 7, 'page', 'SASS平台软件', 'company-sass', 'recommend', '/assets/img/qrcode.png', '', '', 'company-sass', 1495262515, 1495262515, 11, 'normal'),
(12, 0, 'test', '测试1', 'test1', 'recommend', '/assets/img/qrcode.png', '', '', 'test1', 1497015727, 1497015727, 12, 'normal'),
(13, 0, 'test', '测试2', 'test2', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1497015738, 1497015738, 13, 'normal'),
(14, 0, 'Carousel', '首页轮播图', '首页轮播图', '', '', '', '', '', 1601957601, 1601957601, 14, 'normal'),
(15, 0, 'sdcmarry_carousel', '首页轮播图', '首页轮播图', '', '', '', '', '', 1601957685, 1601957685, 15, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_command`
--

CREATE TABLE `fa_command` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型',
  `params` varchar(1500) NOT NULL DEFAULT '' COMMENT '参数',
  `command` varchar(1500) NOT NULL DEFAULT '' COMMENT '命令',
  `content` text COMMENT '返回结果',
  `executetime` int(10) UNSIGNED DEFAULT NULL COMMENT '执行时间',
  `createtime` int(10) UNSIGNED DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) UNSIGNED DEFAULT NULL COMMENT '更新时间',
  `status` enum('successed','failured') NOT NULL DEFAULT 'failured' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线命令表';

-- --------------------------------------------------------

--
-- Table structure for table `fa_config`
--

CREATE TABLE `fa_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量名',
  `group` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '分组',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text COLLATE utf8mb4_unicode_ci COMMENT '变量值',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '变量字典数据',
  `rule` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '扩展属性',
  `setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统配置';

--
-- Dumping data for table `fa_config`
--

INSERT INTO `fa_config` (`id`, `name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`, `setting`) VALUES
(1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', 'My Website', '', 'required', '', ''),
(2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '', '', '', '', ''),
(3, 'cdnurl', 'basic', 'Cdn url', '如果全站静态资源使用第三方云储存请配置该值', 'string', '', '', '', '', ''),
(4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '1.0.1', '', 'required', '', ''),
(5, 'timezone', 'basic', 'Timezone', '', 'string', 'Asia/Shanghai', '', 'required', '', ''),
(6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', '', ''),
(7, 'languages', 'basic', 'Languages', '', 'array', '{\"backend\":\"zh-cn\",\"frontend\":\"zh-cn\"}', '', 'required', '', ''),
(8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', '', ''),
(9, 'categorytype', 'dictionary', 'Category type', '', 'array', '{\"default\":\"Default\",\"page\":\"Page\",\"article\":\"Article\",\"sdcmarry_carousel\":\"Carousel\"}', '', '', '', NULL),
(10, 'configgroup', 'dictionary', 'Config group', '', 'array', '{\"basic\":\"Basic\",\"email\":\"Email\",\"dictionary\":\"Dictionary\",\"user\":\"User\",\"example\":\"Example\"}', '', '', '', NULL),
(11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '1', '[\"请选择\",\"SMTP\",\"Mail\"]', '', '', ''),
(12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', 'smtp.qq.com', '', '', '', ''),
(13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '465', '', '', '', ''),
(14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', '10000', '', '', '', ''),
(15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码）', 'string', 'password', '', '', '', ''),
(16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '2', '[\"无\",\"TLS\",\"SSL\"]', '', '', ''),
(17, 'mail_from', 'email', 'Mail from', '', 'string', '10000@qq.com', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `fa_ems`
--

CREATE TABLE `fa_ems` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='邮箱验证码表';

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_carousel`
--

CREATE TABLE `fa_sdcmarry_carousel` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `category` varchar(30) NOT NULL COMMENT '分类',
  `title` varchar(30) NOT NULL COMMENT '标题',
  `image` varchar(191) NOT NULL COMMENT '图片',
  `url` varchar(191) NOT NULL COMMENT '路径'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='轮播图';

--
-- Dumping data for table `fa_sdcmarry_carousel`
--

INSERT INTO `fa_sdcmarry_carousel` (`id`, `category`, `title`, `image`, `url`) VALUES
(1, 'home', '轮播图1', '/uploads/20201006/497f4a60d1f89a443d4b4022606e282b.jpg', ''),
(2, 'home', '轮播图2', '/uploads/20201006/e1d8c85d9804cc3a2f95777e6e5597f9.jpg', ''),
(3, 'home', '轮播图3', '/uploads/20201006/f073e13efa01849d5e64f7ea47cc6269.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_example`
--

CREATE TABLE `fa_sdcmarry_example` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `category` varchar(30) NOT NULL COMMENT '分类',
  `title` varchar(90) NOT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `main_image` varchar(191) DEFAULT NULL COMMENT '主图',
  `swiper_images` varchar(191) DEFAULT NULL COMMENT '多图',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `updatetime` int(11) NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='案例';

--
-- Dumping data for table `fa_sdcmarry_example`
--

INSERT INTO `fa_sdcmarry_example` (`id`, `category`, `title`, `content`, `main_image`, `swiper_images`, `createtime`, `updatetime`) VALUES
(1, 'live', '现场布置', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"汤唯金泰勇喜帖曝光 精心打造礼饼礼服\" src=\"http://www.520iwed.com/uploadfile/2014/0820/20140820114616112.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 277px; height: 396px;\"></div><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 昨天下午，汤唯金泰勇宣布，两人正式结为夫妻。汤唯与韩籍夫婿金泰勇希望婚礼保持低调，但对于传统的婚嫁习俗仍是非常重视，包括派礼饼，量身定做礼服等重要环节。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><br><img alt=\"汤唯金泰勇喜帖曝光 精心打造礼饼礼服\" src=\"http://www.520iwed.com/uploadfile/2014/0820/20140820114637603.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 392px; height: 397px;\"><br><br><img alt=\"汤唯金泰勇喜帖曝光 精心打造礼饼礼服\" src=\"http://www.520iwed.com/uploadfile/2014/0820/20140820114649964.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 402px; height: 397px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 嫁喜礼饼设计概念</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 新人的嫁喜礼饼礼盒设计简单而别具心思。标志设计概念源于中国传统的双“囍”鸳鸯剪纸图案，并将一对新人的英文名字“TW”(代表汤唯Tang Wei) 及“TY”(代表泰勇Tae Yong)融入“囍”字当中。礼盒采用传统婚嫁喜事的红袍色为主调，配以烫金双囍标志。“囍”字上方两口化为心心图案，下方两口化为“W”及“Y”，而“W”及“Y”中间刚好是二合为一的“T”，左右拼成“TW”及“TY”，二人有如天作之合。“囍”字下面缀以鸳鸯图案，一对鸳鸯的嘴部恰巧形成一个心型图案，象征二人心心相印。而“W”、“T”、“Y”三字刚巧是左右对称，若以“T”字为旋转轴心，“W”及“Y”字盘旋下相互对称，二人翩翩起舞，融为一体，寓意二人一双一对，彼此爱念生生不息、无穷无尽。嫁喜礼饼从细节中展现一对新人爱意洋溢的幸福感觉。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 新郎度身订造结婚礼服</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 准新郎金泰勇亦密锣紧鼓地为大日子做足准备，特地去到曾为前美国总统尼克逊(Richard M. Nixon)、布殊(George H. W. Bush)及荷里活巨星瑞安纳度-狄卡比奥(Leonardo DiCaprio)、马龙-白兰度(Marlon Brando) 订制洋服的本地高级男士订制洋服品牌度身订造结婚礼服。礼服的细节、设计及取材皆一丝不苟，除了以品牌首屈一指的度身订造服务所制成外，采用的面料亦为最优质。在度身期间，在场各人皆从金泰勇脸上的幸福笑容中，感受到其将为人夫的喜悦及兴奋心情。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 汤唯与金泰勇因对电影的热诚而结缘，继而成为夫妇，决以伴侣身份继续追逐电影梦。</div>', '/uploads/20201012/7612bbee3314b71b6ef32eddbb14883f.jpg', '/uploads/20201012/7612bbee3314b71b6ef32eddbb14883f.jpg,/uploads/20201012/e8e9201aa563b03e5086dfd8584bb7e7.jpg,/uploads/20201012/a47d396fab6cf4af8f222d453ae6d185.jpg', 1602480620, 1602500793),
(2, 'live', '拍照摄像', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;&nbsp; 用微信祝愿墙代替蛋糕塔，用二维码代替纸质请柬，自制主题微电影代替鲜花、“杯塔”、泡泡机……婚礼剔除高成本环节、突出新意，成为不少津城新人今秋办喜事的首选。市婚庆服务行业协会统计数据显示，明天起，本市进入“金九银十”婚庆高峰，10月底前全市将有超32000对新人举办婚礼。可喜的是，其中近半数准新人更加注重婚礼的创意，令婚礼成本大幅“瘦身”。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; “我和爱人用丙烯颜料在T恤衫上画画，制作个性服装代替高档礼服；用无烟冷焰火代替花筒礼炮；提前联系车友接亲，不设豪华迎亲车队。在不降低亲友体验的同时，尽可能地压缩成本。”今天下午，就职于本市某金融机构的张强将和妻子小王举行结婚典礼。考察了多个婚庆机构后，二人最终选择的婚庆服务套餐价格不足6000元。小张告诉记者：“这两年参加了不少亲友的结婚典礼，花费都在一两万，甚至还有更贵的。我和爱人都不想砸钱讲排场办婚礼，办创意婚礼虽然要多花些心思，但亲力亲为的婚礼会给我们留下更多甜美的回忆。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 据介绍，今年下半年的结婚高峰主要集中在9月20日至28日、10月2日至18日。婚礼数量虽多，但婚庆成本却在不断“降温”。据婚礼策划人张越介绍，不少婚宴的规模都压缩到10桌以下，“新人们的普遍要求是典礼形式简单、场面简约、有纪念意义，因此有创意的婚庆服务机构更有竞争力”。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 市婚庆服务行业协会潘树辉会长说，在全社会倡导节俭的大环境下，婚礼从大到小、花销由多变少，是一种健康向上的社会趋势。“这样的婚礼虽说环节和场面精简了，但突出亲情、凸显温情，杜绝了攀比之风和不必要的浪费，更利于新人婚后生活的和谐。”</div>', '/uploads/20201012/e8e9201aa563b03e5086dfd8584bb7e7.jpg', '/uploads/20201012/a47d396fab6cf4af8f222d453ae6d185.jpg,/uploads/20201012/e8e9201aa563b03e5086dfd8584bb7e7.jpg,/uploads/20201012/84725de86c6f1133e1d87836c81102c2.jpg', 1602481209, 1602500769),
(3, 'live', '婚车装饰', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 据台湾媒体报道，周杰伦与昆凌1月18日在英国塞尔比教堂举行梦幻世纪婚礼，王子与公主的婚礼让各界相当羡慕，当时他也邀请了50位亲友参加，并豪气包下住宿机票，总花费2300万台币。由于外婆年纪大行动不便，加上台湾亲友众多，两人将于下周返台补请喜宴。在台北婚宴确定为2月9日在台北W Hotel举行，这场宾客将以演艺圈大哥大姐长辈为主，并且“不收礼金”。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><br><img alt=\"继世纪婚礼之后周杰伦9日台北办婚宴邀请前辈\" src=\"http://www.520iwed.com/uploadfile/2015/0201/20150201103126438.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 288px;\"><br>&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 周杰伦与昆凌大婚后，人还在欧洲度蜜月，台湾这边的婚礼则以紧锣密鼓在筹备，2月9日的婚礼，预定邀请名单有张小燕、江蕙、张菲等，至于是否有邀请吴宗宪，他所属的杰威尔音乐表示，“宾客名单都由周妈妈全权决定。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 先前一度传出，除了这场台湾的婚礼外，周杰伦另会邀请平辈举行结婚派对，杰威尔音乐则否认这个说法，表示他只会个别邀请他们吃饭，不会有结婚派对。至于台湾婚宴的风格，则会以轻松为主，并且不收礼金，也不邀请媒体进行拍摄，主要和朋友分享一下喜悦。</div>', '/uploads/20201012/a47d396fab6cf4af8f222d453ae6d185.jpg', '/uploads/20201012/84725de86c6f1133e1d87836c81102c2.jpg,/uploads/20201012/a47d396fab6cf4af8f222d453ae6d185.jpg,/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg', 1602481247, 1602500735),
(4, 'live', '婚礼奏乐', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"伊能静泰国婚礼后发博文致谢\" src=\"http://www.520iwed.com/uploadfile/2015/0322/20150322095444732.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 508px; height: 363px;\"><br>&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 台湾美魔女46岁伊能静21日在泰国普吉岛同小10岁的内地演员秦昊结婚，举行婚礼的现场照曝光，地点位于海边附近，配合周围树木同草地的环境，极具阳光气息，婚礼在乐古浪度假区举行，据请柬上显示，二人21日上午11点迎亲敬茶，下午两点外景拍摄，4点半举行婚礼仪式，夜晚7点半举行婚宴，10点半就有餐后酒会。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 昨日，伊能静发文分享婚礼现场母亲致辞等感人情景，并致谢所有亲友，感谢“一直接纳我如己出的秦先生家人”。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 伊能静称：“谢谢媒体朋友的关心，谢谢爱我的你们，如果没有你们，我不可能有今天的生活，也不可能在坑坑洞洞里依然相信善念的珍贵。我多希望能把自己的感恩，透过各种方式让你们知道，但最后也只能说谢谢。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 网友纷纷评论：“一定要幸福”“难得你还保有对这个世界的信任，支持你”。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 伊能静发长微博原文：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 今天晚宴，爸爸妈妈在台上说，看着我们的婚礼，脑海里划过的却是秦先生从小到大的每个场景。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 然后是我母亲上台，说了感谢，身体微微地颤抖，她一向害羞，说完两句就哽咽。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 我也掉泪了，想到父亲不在的遗憾，母亲独立照顾我们的辛苦。这一天更多时候，我看到的是亲人的爱和温暖。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 谢谢妈妈、谢谢姐姐、谢谢闺蜜、谢谢挚友，谢谢帮助我们完成梦想的工作伙伴，更谢谢一直接纳我如己出的秦先生家人，能孝顺他们是我的福气。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 谢谢媒体朋友的关心，谢谢爱我的你们，如果没有你们，我不可能有今天的生活，也不可能在坑坑洞洞里依然相信善念的珍贵。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 我多希望能把自己的感恩，透过各种方式让你们知道，但最后也只能说谢谢。</div>', '/uploads/20201012/84725de86c6f1133e1d87836c81102c2.jpg', '/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg,/uploads/20201012/84725de86c6f1133e1d87836c81102c2.jpg,/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg', 1602481278, 1602500712),
(5, 'live', '婚礼活动', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"婚礼简朴或奢华 量力而行才是最重要\" src=\"http://www.520iwed.com/uploadfile/2015/0618/20150618114229815.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 400px; height: 267px;\"><br>&nbsp;</div><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp;水下婚礼、自行车婚礼、公交车婚礼、骑马婚礼等，现在的年轻人越来越追求个性化的婚礼。昨日，又一场个性化的婚礼在西安上空上演，一对新婚小夫妻乘坐直升机在西安上空飞行20多分钟，高空宣言让亲友见证他们的甜蜜爱情。据知情人透露，这场婚礼仅租直升机的费用就在6万元，整场婚礼下来估计得20多万元。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 不得不承认的是，随着生活水平逐渐提高，结婚越来越奢侈，排场越来越大。现在的婚礼越来越演变成为“金钱”打造的舞台了。可话说回来，平心而论，现在一场婚礼要有绚丽的舞台、复杂的程序，花费自然也很高。对于即将步入婚宴殿堂且经济条件富有的新人来说，隆重而又奢华的婚礼容易统一大家的意见，也是象征着今后美满生活的开始。对于经济条件一般的人说，奢侈的婚礼势必给新人及其家庭造成一定的经济压力。因此，笔者认为我们应正确认识和对待奢侈婚礼，尊重个人消费方式的自由。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 过去，对于奢侈婚礼的我们谈都不敢谈，更不要说是动辄上万元的婚礼消费。而今，我们不仅敢谈，而且有能力消费奢。这是观念的转变、生活方式的转变。毕竟，结婚是两个人以及两个家庭的大事，操办婚礼绝都不会草率行事，中国的传统观念对于婚姻非常看重，承载了众多亲友的关注、企盼和祝福，对这个新家庭投入的多寡，彰显了当事人的重视程度。当然，婚礼只是一个形式、一个过程，与钻戒、婚纱、喜宴、新房相比，对这段婚姻的期许、祈福和维护更为重要，幸福的本质是朴实的，不是做给别人看的，不必为了所谓的“面子”而承受奢侈婚礼带来的经济压力。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　基于此，笔者认为对于在婚礼上花费这个问题上，既不能一味地鼓励，也不能一棍子打死，而应该遵从“大道自然”。从新人的角度讲，要倡导广大新人们根据自家的财力制定婚礼的水准，简朴的裸婚和奢华的排场都无可厚非，量力而行才是最重要的。</div>', '/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg', '/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg,/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg,/uploads/20201012/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg', 1602481308, 1602500689),
(6, 'live', '婚车服务', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"史上最“土豪”的迎亲队伍 150余辆私家车组成\" src=\"http://www.520iwed.com/uploadfile/2015/0628/20150628112006313.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 402px; height: 282px;\"><br>&nbsp;</div><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; 近年来，“土豪婚礼”、“土豪婚车”越来越受到市民的关注。27日上午，济南街头也出现了一支堪称“土豪”的迎亲队伍，一支由150余辆私家车组成的迎亲队伍出现在清河北路与历山北路附近。队伍以一辆捷豹打头，随后是10辆凯迪拉克、3辆奔驰、120辆形色各异的私家车，还有20辆统一规格的厢式货车。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　知情人透露，迎亲队伍从尚品清河小区出发，经过泺口服装城后向东沿清河北路一路到达历山北路黄台电厂产业园，车辆所到之处无不引起交通拥堵。10:20左右，记者在清河北路看到结婚车队，每辆车都贴有一张心形号码牌，从1号到150余号。整个车队从清河北路绵延至历山北路，长约至少1公里。记者观察，整个车队过路口红绿灯足足用了半个多小时，同时，路口造成大范围拥堵。车队一位司机告诉记者，迎亲的新郎为某物流公司的员工，所有车辆都是公司同事“友情赞助”，不需要任何费用，“连婚车都是借我们老板的，不花钱。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　随后，记者从新郎王国建处得到证实，车队的所有车辆确实都是公司同事的私家车，“以前家里条件不好，后来才慢慢得到改善，家里也不想铺张浪费，大家就想借着大喜的日子热闹热闹，兄弟姐妹们就想了这么个招儿，不存在炫富啥的。”面对这个“超长”迎亲队伍，市民王先生和李女士都觉得很热闹，“挺好，结婚就得热热闹闹的。再说了，公司的车又花不了几个钱，这跟那种‘土豪车队’不一样。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　当然，也有不少市民持反对意见，家住清河北路附近的唐女士表示，即使车队花不了多少钱，如此迎亲也太浪费，而且对交通造成拥堵，“我们当年那么简单，没有婚车没有豪华酒宴，现在不是一样很幸福吗？”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　相关链接</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　婚车的变迁：从花轿到加长林肯</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最传统：花轿、马车</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　虽然现在流行的汽车接亲同婚纱一样，是从西方舶来的婚庆礼仪，可论起婚车，最老资格的可是中国传统婚礼的花轿。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最环保：自行车</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　缝纫机、手表、收音机、自行车，上世纪六七十年代家庭争配的“三响一转”，悄悄弥补了公共交通的不发达，也改变了新娘子的座驾。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最高调：拖拉机、卡车</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　上世纪七十年代，机械化扩大了人们的生活半径。有头有脸的家长会努力跟合作社借台手扶拖拉机，县城和农村的新娘子们也能享受到高科技。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最团结：吉普车、面包车、卡车</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　七十年代初的吉普车，七十年代末的面包车，八十年代前期和中期的面包车，这些公车私用往往会动用了男女双方所有的社会关系，才有望拼出两辆来。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最拉风：老红旗、桑塔纳、老尼桑进口车等等</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　从八十年代末到九十年代，国产的老红旗，进口的桑塔纳，尼桑等车型成为首批婚庆轿车。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　●最常见：加长林肯、奔驰、奥迪</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">　　当选择不再被视为选择，随着私家车市场的繁荣和婚车租赁业的兴起，二十一世纪的婚车故事太多，比如，加长林肯、奔驰、奥迪。</div>', '/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg', '/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg,/uploads/20201012/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg', 1602481348, 1602500661),
(7, 'live', '份子钱', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"沪结婚人数下降 但平均总花费已攀升到34万\" src=\"http://www.520iwed.com/uploadfile/2016/0829/20160829125858183.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 650px; height: 396px;\"></div><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;据数据分析，在未来五年，我国结婚人口总体呈下降趋势。这表明人口红利带来的爆发式增长已经不再，结婚行业也不例外。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 从27日、28日在上海世博展览馆举办的第34届婚芭莎·中国婚博会上获悉，目前上海新人结一次婚的平均总花费（不包括婚房、婚车）已攀升到34万元左右。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">新人结婚平均花34万</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 每年分四季在上海、北京、广州、天津、武汉等五大城市巡回举办的中国婚博会是目前全球最大规模的一站式结婚消费展，仅在上海一地每年就举办四次，单次展出面积在5万-7万平方米，因此也被称为上海新人结婚走势的风向标和晴雨表。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 婚博会相关负责人表示，除去购买婚房、婚车的最大开销外，通常意义上所指的结婚消费主要包括八个方面，即新房装修、婚纱摄影、结婚礼服、婚戒首饰、喜糖喜酒、婚庆服务、酒店喜宴、蜜月旅行等。在八大消费中，支出最大头是婚宴。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 从在沪举办的最近四届中国婚博会统计情况来看，总到场人数分别为103500人、115100人、147600人和135100人，总订单数分别为36600份、42500份、53500份和48900份，下单总额分别达到7.94亿、8.68亿、11.6亿和10.623亿元。约有近六成观展新人选择在婚博会现场下订单。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 总体来看，婚纱摄影、婚戒首饰和婚宴已经成为上海人的“结婚标配”，80%以上的结婚用户都会选择这三样；其次为婚庆服务和婚纱礼服，各有平均58%和42%的用户会选择；受中国传统“酒桌文化”影响，婚宴消费成为结婚“最大头”，占到整体花费的65%以上。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 今年1-6月，本次婚博会主办方对近年来5万名上海准新人进行了调查，调查对象包括最近几届参加婚博会的新人和部分上海地区线上会员，调查结果显示，目前上海新人结一次婚的平均总花费（不包括婚房、婚车）已攀升到34万元左右，比武汉、成都、苏州等二三线城市贵出将近一倍。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">租婚纱定制妈妈装成潮流</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 走进本次中国婚博会婚纱礼服的主展区，记者看见有很多观展新人驻足。一对在父母陪同下前来参观的准新人告诉记者，如今作为结婚主力人群的85、90后上海新人眼光更高，从出门纱、结婚礼服到敬酒装起码要三四套，在迎接宾客、给亲朋好友敬酒时都是近距离接触，所以他们更倾向于花五六千租几套设计感和面料都上档次的婚纱。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 在面料和款式上，采用进口面料和手工定制的重工蕾丝以及大胆追求性感的露背婚纱成为主流，而新郎礼服则普遍以定制为主，“妈妈装”也开始大行其道。一位参展商表示，这是因为如今的上海新人越来越重视为父母装扮，考虑到妈妈们的体形普遍比较富态，很难租到合适的礼服。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">酒店纷免“婚庆进场费”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 今年婚博会传递出的另一大变化就是婚车和旅拍的走红，一辆辆挂着沪牌的宝马五系、七系、劳斯莱斯婚车成为很多婚庆公司和酒店展台上的“重量级展品”。“上海人结婚喜欢讲排场。”某婚庆公司参展人员告诉记者，现在车况好、带沪牌的婚车最抢手，因为可上高架还不受早晚高峰的限制，而且色彩统一，组成车队很气派。而旅拍则以东南亚海岛和欧洲最受欢迎。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 记者注意到，根据我国每年出生人口的数据和结婚年龄分布综合分析，2015年全国结婚人口在1200万对左右，至2020年时这一数字会下滑至1000万对以下。在未来五年，结婚人口总体呈下降趋势。这表明人口红利带来的爆发式增长已经不再，结婚行业也不例外。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 据主办方透露，参加本届中国婚博会的新人预计在3.5万对左右，其中首日参观新人为2.5万对，首日下单总额为4.23亿元，要比前两年高峰时的5万对下降不少。上海新人初婚年龄的推迟也是结婚人数下降的一个原因，加上近两年沪上涌现出20多家一站式婚礼会所，对星级酒店稳固的婚宴市场带来不小的冲击。受此影响，沪上20多家知名的高星级酒店首次在婚博会上宣布对结婚新人免收2000-5000元的“婚庆进场费”，在黄金档期上的选择面也更加宽裕。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">“小蜜蜂”现场拉客要被罚</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 历届婚博会上导购人员（俗称“小蜜蜂”）强行拉客甚至大打出手的现象也屡受诟病。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; 婚博会相关负责人陈赟表示，主办方今年对婚博会的所有观众、参展商和400多位展商雇佣的“小蜜蜂”全部采取实名制登记报备制度，每一名报备过的导购人员的身份信息都有存档，且需佩戴导购证上岗。主办方划出明确的导购框和点位，导购人员不得逾越导购框拉拽观众。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp; &nbsp; &nbsp; &nbsp; “我们这次在展馆内配备了300多名戴着 值勤 字样红袖章的保安，负责驻点值守和流动巡视。”一旦发生违规情况，“除了清除出馆外，还将通过身份证登记信息对相关参展商开具警告单，遭到多次警告的商家将被罚款。”陈赟透露。</div>', '/uploads/20201012/4a2d3208e64899131da88a57601094bb.jpg', '/uploads/20201012/cd823cf8e3fa1980bec30cb4e8fa3cdc.jpg,/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg,/uploads/20201012/f2fe55b616856a0930d71f64f6299857.jpg', 1602481386, 1602500629),
(8, 'live', '婚礼致辞', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"两新人隧道内办“特殊婚礼” 陌生人祝福中甜蜜相拥\" src=\"http://www.520iwed.com/uploadfile/2016/0921/20160921124741103.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 530px; height: 297px;\"></div><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">高速隧道办婚礼引关注！据了解，9月20日上午10时31分，包茂高速安康段凤凰山隧道出口100米处发生一起单方交通事故。车祸致高速路双向拥堵，于是一对新人为赶吉时，在隧道内办“特殊婚礼”。据悉，为保障安全以及不影响他人的出行，婚礼整个仪式只进行了十分钟，过路司机纷纷当起了婚礼嘉宾。有网友表示仪式十分钟感动一辈子！</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">高速路上遭遇堵车，想必每个人都不会有好心情。如果是遇到结婚这样的人生大事，堵车更是让人糟心。9月20日，安康一对新人因堵车担心错过良辰吉时，在高速隧道内举办的一场“温情”婚礼，却让不少网友大呼“浪漫”，并纷纷送上祝福。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">车祸致高速路双向拥堵</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">两新人隧道内办“特殊婚礼”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">9月20日上午10时31分，包茂高速安康段凤凰山隧道出口100米处发生一起单方交通事故。辆由紫阳驶往安康方向的重型半挂车与中央护栏碰撞后横在了路中间，致使车头和车尾分离，车内人员受伤。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">随后高速交警赶赴现场处理，由于事故导致该处路段两个方向以及应急车道彻底被堵死，施救难度比较大，直至12时10分，救援人员清理出一个车道单边放行。下午2时20分，该处路段彻底恢复畅通。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">11时30分左右，凤凰山隧道内已堵了近100辆车，见苦等一个小时仍未通车，加上隧道内空气污浊，让不少司机都非常苦闷。可接下来的一幕，让大家抓走了眼球，也让堵在现场的司机赵先生觉得既新奇又感动。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">“当时我在后面堵着，见前面有不少人围在一起，便凑了上去原来是一队婚车也困在隧道车流中，眼看马上快到约定的婚礼开场时间，一对新人迫于无奈，便在司仪以及朋友见证下临时办了一场婚礼，现场不少过往司机也被邀请参加了这场特别的婚礼，并用手机记录下他们的幸福瞬间。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">赵先生感慨地说， 他是头一次遇到这样特别的婚礼，“我想告诉他们，两个人过一生肯定会有许多坎坷，这点小小的堵车不算什么，反倒会让他们今生难忘，就当是迈向婚姻生活的第一步，祝福他们能够白头到老。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">过路司机当起婚礼嘉宾</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">陌生人祝福中甜蜜相拥</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">随后，华商报记者辗转联系到负责此次婚礼策划的安康市艾纱婚礼策划公司负责人小吴，据当时在现场的他介绍，自己曾策划过许多次婚礼，但在高速路隧道内举办婚礼还是头一遭。新郎是安康市汉滨区人，新娘子是紫阳县人。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">当日早上五点多，新郎便乘坐婚车去接新娘，不料返程时因前方发生车祸，被堵在了隧道内。当时觉得距离12点办仪式的时间还早，大家心态都比较轻松，新郎还和几个朋友蹲在隧道内玩了一会扑克。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">不料一个多小时后，隧道仍未疏通，按照当地习俗，新人必须在中午12点前办仪式，于是他们商量并征得新人同意后，便举行了一个简单婚礼仪式，“当时为了保证安全，以及不影响其他车辆，仪式缩短为10多分钟了。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">在司仪主持下，新郎从婚车上将新娘接下。“无论贫穷与富贵，无论疾病与健康，你是否都愿意一生守护着他(她)?”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">在司仪的询问下，新郎新娘“我愿意”的承诺清脆又响亮。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">随后，两人在四五十位亲朋以及受邀作为嘉宾的过往司机祝福和掌声中，紧紧地拥抱在一起。12时10分，经过紧张疏通，救援人员清理出一个车道单边放行，这对新人成功赶到位于安康市高新区的酒店成功举办了婚礼。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">“当时我在办仪式的酒店，没赶上婚礼现场有些遗憾，但从网上看到朋友发的现场照片以及网友祝福后非常感动，太有纪念意义了。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">下午4时许，新郎的朋友小田告诉华商报记者，两位新人看到网友的祝福后都很激动，也很感谢大家，但由于办完婚礼后比较劳累，因此婉拒了记者的采访要求。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">网友评论：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@897有话说：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">有一句话叫做 “只要心中有海，哪里都是马尔代夫”，但今天这场特别的婚礼让我们看到——“只要心中有爱，在哪里都能与子偕老”。因故堵住的隧道里，婚礼虽然简陋，但这一份爱未必简单。一群人，两行车，无数闪光灯，就是对这两位新人最大的祝福!</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@刻骨铭心：最浪漫最美的最有纪念意义的婚礼，祝福你们!</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@老吴：在困难当中接下的海誓山盟，才显得更加难能可贵!相信在未来的日子，新娘一定会记得隧道灯光下，新郎的甜蜜誓言。</div>', '/uploads/20201012/f2fe55b616856a0930d71f64f6299857.jpg', '/uploads/20201012/efbb65a3030ef826d8cd77cc6bbe3766.jpg,/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg,/uploads/20201012/f2fe55b616856a0930d71f64f6299857.jpg', 1602481442, 1602500593),
(9, 'live', '完美婚姻', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"婚礼舞台架突然倒塌 安全意识低酿悲剧\" src=\"http://www.520iwed.com/uploadfile/2016/1114/20161114104447357.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 359px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">作为新郎新娘至亲入座的主桌位于离舞台最近的区域，恰巧在这个钢架正下方，坐在主桌上的7人因躲闪不及被倒下的钢架砸中，由于钢架非常重，待公安、消防、安检、120急救等部门赶到现场后，伤者才被救出送往医院，据了解有3名伤者被送往了无锡市人民医院，其余4名则被送至第四人民医院。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">随后，记者来到四院，此时几名伤者正在急诊室内接受治疗，其中两名伤者头部受伤，不过幸运的是没有直接砸中头部，都是被架子蹭到。据一名伤者说，他所坐的位置是背对着舞台的，因此并不知道发生了何事，听到有人发出尖叫抬起头，才发现架子倒下来砸在面前的餐桌上，而自己也被蹭到受了伤。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"婚礼舞台架突然倒塌 安全意识低酿悲剧\" src=\"http://www.520iwed.com/uploadfile/2016/1114/20161114104550462.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 373px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">伤者的家属小明（化名）告诉记者，当晚自己和妈妈，外婆，外公四人一起作为新娘的亲眷坐在了主桌，当时婚礼接近尾声，婚庆公司在收拾舞台设备时不慎发生了事故，当时因为自己和外公坐的离舞台稍远的位置因此未被钢架砸中，而母亲和外婆却没能逃过一劫，新娘也因此事件当场情绪失控。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据了解，此次事故共造成七人受伤，其中三人骨折，其余四人均为皮外伤，所幸七人都无生命危险，事故具体原因正在进一步调查中。记者从医院了解到，事故发生后有多人受伤，伤势最重的被砸骨折，大多数人是皮外伤，并无生命危险。目前，事故原因仍在调查中。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">婚礼执行过程中的安全问题，一直都牵动着婚礼人的神经，特别是使用了众多器材设备的大型婚礼中，稍微出点马虎大意，都会酿出不可挽回的损失。对此我们希望婚庆公司一定要有安全意识。酒店与婚庆要各司其职，安全问题，重于泰山！</div>', '/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg', '/uploads/20201012/aa4db94b8d3ba21079908f472972ae0b.jpg,/uploads/20201012/7612bbee3314b71b6ef32eddbb14883f.jpg,/uploads/20201012/f2fe55b616856a0930d71f64f6299857.jpg', 1602491947, 1602500564);

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_order`
--

CREATE TABLE `fa_sdcmarry_order` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `service_id` int(11) UNSIGNED NOT NULL COMMENT '服务id',
  `title` varchar(120) NOT NULL COMMENT '服务名',
  `username` varchar(30) NOT NULL COMMENT '姓名',
  `mobile` varchar(20) NOT NULL COMMENT '联系电话',
  `remark` text NOT NULL COMMENT '备注',
  `price` decimal(8,2) NOT NULL COMMENT '价格',
  `orderno` varchar(14) NOT NULL COMMENT '订单号',
  `ordertime` int(13) NOT NULL COMMENT '预约时间',
  `createtime` int(13) UNSIGNED NOT NULL COMMENT '创建时间',
  `updatetime` int(13) UNSIGNED NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

--
-- Dumping data for table `fa_sdcmarry_order`
--

INSERT INTO `fa_sdcmarry_order` (`id`, `user_id`, `service_id`, `title`, `username`, `mobile`, `remark`, `price`, `orderno`, `ordertime`, `createtime`, `updatetime`) VALUES
(14, 6, 1, '爱的承诺-预约大礼包', '郑兴', '15500785170', '', '2000.00', '710289144906', 1603814400, 1602905669, 1602905669);

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_post`
--

CREATE TABLE `fa_sdcmarry_post` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `category` varchar(30) NOT NULL COMMENT '分类',
  `title` varchar(90) NOT NULL COMMENT '标题',
  `main_image` varchar(191) NOT NULL COMMENT '主图',
  `description` text NOT NULL COMMENT '简介',
  `author` varchar(30) DEFAULT NULL COMMENT '作者',
  `content` text NOT NULL COMMENT '内容',
  `is_post` enum('0','1') NOT NULL COMMENT '状态:0=未发布,1=已发布',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `updatetime` int(11) NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章';

--
-- Dumping data for table `fa_sdcmarry_post`
--

INSERT INTO `fa_sdcmarry_post` (`id`, `category`, `title`, `main_image`, `description`, `author`, `content`, `is_post`, `createtime`, `updatetime`) VALUES
(1, 'news', '2020年婚纱流行趋势 现在看还来得及！', '/uploads/20201009/6fb06c1b10982d5eae781f1748ada5fe.jpg', '对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。', 'DanceLynx', '<p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">对于很多女生而言，无论想要结婚与否，婚纱对于她们来说都有着一种难以抗拒的魔力。在你穿上圣洁白纱的那一刻，整个人仿佛都会焕然一新。</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_match/0/10947768214/0\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">虽然婚纱看起来都是万变不离其宗，不管什么时候都是白白的，会有很多薄纱与蕾丝。但其实如果你仔细观察就会发现，表面看似相近的婚纱，其实每一件都有其独到的不同。</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_match/0/2364123860/0\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">今天就来为大家解读一下2020的婚纱流行趋势，照着这些款式和元素去挑选，你绝对是朋友圈里最酷的新娘！</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_match/0/10774763051/0\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"><strong style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">流行趋势之披风婚纱</strong></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768218/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">Monique Lhuillier Bridal</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">其实将披风的设计运用到婚纱上，很多大牌婚纱都推出过带有披风的婚礼服。虽然款式不尽相同，但它们每一件都能让穿上它的新娘顿时气场骤升。</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768219/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768220/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">Zuhair Murad</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">披风婚纱也被设计为两种类型，一种是披肩式的拖地长披风，非常帅气。如果你是气质女王型，可以通过这种大披风来增强视觉存在感，让新娘整体气势更强。</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768221/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768222/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">Pronovias</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">不过在选择这种婚纱时，除了需要你强大的气场外，更重要的是要瘦!否则上身后宽宽的线条会让新娘的身材看上去无比魁梧，而这种形容词应该没有人会想用在自己的婚礼上吧。</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768223/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">Celia Kritharioti In love</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768224/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\"></p><p align=\"center\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"><img src=\"https://inews.gtimg.com/newsapp_bt/0/10947768225/641\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; max-width: 640px; display: block;\"></p><p style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;;\"></p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">Jenny Packham</p><p class=\"text\" style=\"margin-bottom: 28px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; overflow-wrap: break-word; font-family: &quot;Microsoft YaHei&quot;; text-indent: 2em;\">当然，如果你担心过长的披风会拉低你的比例，或者对自己身材不自信无法驾驭上面的婚纱，那么较短的或者材质轻薄的披风肯定能满足你的要求。</p>', '1', 1602256564, 1602257557),
(2, 'news', '张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘', '/uploads/20201009/4dc1d65de0be85512daaa45f52c2e384.jpg', '日前，网曝张若昀与唐艺昕将于6月26、27日在爱尔兰结婚。张若昀与唐艺昕将于本月底在国外举办婚礼，伴娘团由宋茜、沈梦辰等组成。随后张若昀父亲张健向媒体证实：张若昀唐艺昕月底办婚礼！', 'DanceLynx', '<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">日前，网曝张若昀与唐艺昕将于6月26、27日在爱尔兰结婚。张若昀与唐艺昕将于本月底在国外举办婚礼，伴娘团由宋茜、沈梦辰等组成。随后张若昀父亲张健向媒体证实：张若昀唐艺昕月底办婚礼！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><br>盼星星盼月亮，终于盼到这对情侣给我等吃瓜群众发糖啦！更重要的是，他们连结婚地点的选择都选择了不允许离婚的国家——爱尔兰。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><br>毕竟他们的爱情真的如爱尔兰的婚姻寓意一样，纵使外界言语纷纷，我仍爱你如初。公开撒糖的唯两次，一次公布恋情，一次结婚。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084234220.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 700px; height: 317px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">2017年8月，张若昀发文高调公开恋情，表白唐艺昕：“时光赐给我们盗不走的爱人，而你赐给我时光。”并且附上唐艺昕亲吻自己侧脸的亲密照片。唐艺昕则甜蜜回复：“时光往复，爱你如初。”此前二人多次同行被拍，热爱后也多次大方公开撒糖，如今终于收成正果，恭喜恭喜！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084314925.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 437px; height: 545px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">说起娱乐圈的情侣，张若昀唐艺昕这一对可以说是十分让人羡慕了！两人在一起这么多年感情一直很稳定，颜值也超高，并且在娱乐圈也很低调，不少朋友都说十分看好这对情侣。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084336299.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 692px; height: 922px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">他俩人虽然是在2017年8月公开的恋情，但早在2011年张若昀就在微博上晒出关于“她”的备忘录，看来两人的恋情在那个时候就已确定，至今已经过去8年。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084359922.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 724px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">月末临近，也就是快到了张若昀和唐艺昕结婚的时间，其实在去年的8月份，也就是差不多是他们相识的时间8月2号，有网友就曝出了，二人已经领证结婚，但是双方都没有给予正面回应，这次看来终于是石锤了。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084429831.png\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 900px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">此次还有个亮点就是，此次的伴娘团是由宋茜和沈梦辰等人组成，宋茜和沈梦辰也是唐艺昕的好朋友，两人甜美形象和高颜值也将会成为婚礼的看点。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084457380.png\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 1003px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">对此有网友纷纷开始催婚两人，沈梦辰还好一点，因为沈梦辰毕竟和杜海涛已经传出了恋情。结婚喜讯也将提上日程，至于宋茜在婚姻上面就显得很安静了。网友开始疯狂催婚了宋茜了，这届网友真的很皮呢！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><br>说起来，唐艺昕跟张若昀的缘分还是来自于一个误会，张若昀曾在微博公开过与唐艺昕的相识经过，里面有些情节就像偶像剧一样，张若昀唐艺昕的遇见是在一个加油站，因为大雨张若昀的车在红绿灯不小心追尾唐艺昕的车。俩人的缘分就此展开，并且一跑就是八年时光。让彼此都对对方的一切了如指掌。如今他们的好事临近，相信婚礼也一定会很隆重。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"张若昀唐艺昕月底结婚 ，沈梦辰、宋茜当伴娘\" src=\"https://www.520iwed.com/uploadfile/2019/0622/20190622084529412.png\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 837px;\"></div>', '1', 1602257654, 1602257654),
(3, 'news', '年末节日大大增加情侣分手率，父母该“插手”孩子婚姻？', '/uploads/20201009/325b6975698a34756cb4ce43ab4255e0.jpg', '小可爱们，大家看了今天的热搜吗？#年末节日会大大增加情侣分手的几率# 好阔怕！\r\n \r\n其实事情是这样的：\r\n恋爱6年，26岁的小叶与比她大2岁的学长小李，计划今年春节步入婚姻殿堂。\r\n但双方家长见面后，在聘礼和婚房问题上出现了矛盾，小李甚至坚定地站在了他母亲那边，小叶也因此\"不想嫁了\"……', 'DanceLynx', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">小可爱们，大家看了今天的热搜吗？#年末节日会大大增加情侣分手的几率# 好阔怕！</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">其实事情是这样的：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">恋爱6年，26岁的小叶与比她大2岁的学长小李，计划今年春节步入婚姻殿堂。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">但双方家长见面后，在聘礼和婚房问题上出现了矛盾，小李甚至坚定地站在了他母亲那边，小叶也因此\"不想嫁了\"……</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">对此，专家表示，春节这个特殊节点，会促使小情侣重新审视两人的关系，再加上旁人的意见，就容易导致情侣感情破裂而分手。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"年末节日大大增加情侣分手率，父母该“插手”孩子婚姻？\" src=\"http://www.520iwed.com/uploadfile/2018/0126/20180126101757759.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 545px; height: 379px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">网友们则说：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">现在很大一部分的男生啊，真是没能耐没本事还会为自己的无能，找各种花花借口。妈宝男啊？</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">真真的，你们这些男人，穷的时候女人跟了你们有几个以后不出轨的。呵呵，穷的时候嫌弃女人物质，有钱以后又看花眼了！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">这个不一定吧，我男友也没房没车，他家里无法满足这些物质条件。我们也快大学毕业了，不过未来都比较有计划。他考了很多证书，很努力很上进而且懂事。现在穷不可怕，可怕的是穷还堕落如果毕业他向我求婚，我会嫁给他的！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">就因为一个</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">“去谁家过年”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">的问题就拆散了成千上万对情侣……</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">庆幸分手了！每每想起来都觉得真是无比正确的选择，这样的人，真的是在找保姆。他会要求你牺牲所有利益，满足他的家人，让他的家人得到很好的照顾。但就是不会考虑你的感受。嫁过去，你能做的只有不断的付出，不断的付出付出付出</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">看完这则新闻，小艾只想说</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">春节就是小型异地恋，不仅要防着对方的青梅竹马妖艳贱货，还得小心三大姨六大婶的“危言耸听”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">但，年末节日真的会大大增加情侣分手的几率吗？经不住考验的不是节日，是爱情本身</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">同时，我们能理解父母长辈为孩子的幸福考虑，可是，长辈们在当孩子“第三者”前，真的不担心孩子以后找不着对象吗？</div>', '1', 1602257721, 1602257721),
(4, 'news', '幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！', '/uploads/20201009/b0cfdf3fb1e6e005825ab098f71bb3a4.jpg', '国庆这几天，当大家的朋友圈都在晒各种大型旅行摄影展时，婚礼人的朋友圈晒图可能是这样\r\n', 'DanceLynx', '<p><div class=\"share-collect clearfix\" style=\"padding: 0px; margin: 0px 0px 30px; vertical-align: baseline; display: block; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;\"><div class=\"pull-left\" style=\"padding: 0px; margin: 0px; vertical-align: baseline; float: left;\"></div></div></p><div class=\"article-content\" style=\"padding: 0px; margin: 0px; vertical-align: baseline; font-size: 18px; line-height: 35px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;\">国庆这几天，当大家的朋友圈都在晒各种大型旅行摄影展时，婚礼人的朋友圈晒图可能是这样<div style=\"padding: 0px; margin: 0px; vertical-align: baseline; text-align: center;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004081813623.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 639px; height: 637px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; text-align: center;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004081836148.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 477px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; text-align: center;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004081850642.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 637px; height: 479px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">在婚礼界做了许久，每当看到伙伴们早出晚归，辛勤忙碌，就想有一种画出他们日常工作状态的灵感。婚礼人是一个新兴职业，对于大众而言也许仍然陌生，而我们需要让客人们与社会大众了解这是一个需要分工缜密、专业精细、体力充沛的职业，体会到我们有一颗成人之美的心。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">于是，一幅真实描绘婚礼人一天的极限挑战图诞生了</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; text-align: center;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082008719.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 631px; height: 375px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082029885.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 635px; height: 448px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082042748.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 641px; height: 670px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082055764.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 638px; height: 439px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082108187.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 640px; height: 418px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082120318.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 631px; height: 641px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082133638.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 635px; height: 664px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082146191.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 638px; height: 698px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082158204.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 637px; height: 667px;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082214474.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 637px; height: 209px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\"><br>做婚礼，外界看来似乎开心快乐，其实肩负重重压力，经常需要通宵达旦，疲惫不堪，但是一看到自己的付出，换来新人们及其家庭满意的答谢与笑容，任何辛劳便会一扫而尽，一种满满幸福与成就感油然而生。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">有人曾说过，一个好的结婚典礼代表着一段美好婚姻的开始。我们在做的是一个家庭刚开头的事，这项工作也许不仅仅是一个典礼，而是开启了一段美丽人生。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">每每到了一年的婚礼旺季，婚礼人熬夜是必须的。熬夜避免不了，但素材君偷偷告诉你，熬夜有技巧！献给最近经常熬夜身体透支的婚礼人们！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">熬夜的您，千万记住：</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">1、晚饭不能吃太饱。熬夜时要吃热的东西，但不要吃泡面来填饱肚子，以免火气太大。最好尽量以清粥小菜、水果、土司来充饥。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">2、开始熬夜前，来一颗维他命B群营养丸。维他命B能够解除疲劳，增强人体抗压力。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">3、注意保暖，不要冻着肚子。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">4、一定要有足够多的白开水。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">5、熬夜时，应时时做深长呼吸。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">6、提神饮料，最好以绿茶为主，可以提神，又可以消除体内多余的自由基，让您神清气爽。但是胃肠不好的人，最好改喝枸杞子泡水的茶，可以解压，还可以明目呢！但要注意应饮热的，浓度不要太高，以免伤胃。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">7、早饭一定要吃饱，一定不要吃凉的食物。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">8、凌晨7、8点钟，若要睡觉，一定要收心，即使不睡觉，也要坐在椅子上收心。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline;\">婚礼诚可贵，身体更重要。也祝各位婚礼人国庆愉快！您辛苦了！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; text-align: center;\"><img alt=\"幸苦了！致国庆期间奋斗在各个婚礼现场的婚礼人！\" src=\"http://www.520iwed.com/uploadfile/2017/1004/20171004082319703.jpg\" style=\"border: 0px; display: block; max-width: 100%; margin: 10px auto; width: 638px; height: 706px;\"></div><div id=\"PagesBar\" class=\"pages_wrap\" style=\"padding: 0px; margin: 50px; vertical-align: baseline; font-size: 15px; font-family: &quot;Microsoft Yahei&quot;;\"><p id=\"pages\" class=\"page\" style=\"padding: 0px; margin: 0px 0px 32px; vertical-align: baseline; display: block; text-align: center; color: rgb(153, 153, 153);\"></p></div></div>', '1', 1602257788, 1602257788),
(5, 'news', '800元请的跟妆师，居然只值50块？', '/uploads/20201009/f2b8591a6ca7ab1af0ca9d9900950c25.jpg', '『 先声明，这篇文章可能会得罪一些婚庆商家，但以下套路，全部来自真实报道和新人反馈。行业鱼龙混杂，我们见过很多非常优秀、有创造力、有行业道德的婚庆公司，但黑暗的一面也不能闭眼漠视。我们杜绝的是不良婚庆公司的忽悠，并非抹黑行业。无脑喷的，一概不理，谢谢。』', 'DanceLynx', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">『 先声明，这篇文章可能会得罪一些婚庆商家，但以下套路，全部来自真实报道和新人反馈。行业鱼龙混杂，我们见过很多非常优秀、有创造力、有行业道德的婚庆公司，但黑暗的一面也不能闭眼漠视。我们杜绝的是不良婚庆公司的忽悠，并非抹黑行业。无脑喷的，一概不理，谢谢。』</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">马上“十一”长假来了，很多妹子们都选择了在长假举办婚礼，小编也每天积极的为你们收集备婚干货，最近在逛iWed婚礼网新娘论坛的时候，看到了很多妹子吐槽自己被婚庆公司坑，看的小编十分的揪心啊。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@不P图的人妻：跟我们在初期接洽的时候说的特别好，会给我们做个方案，还有布置的效果图，还有提前一两周的时候提醒我们，给我们流程表和策划，balabala的，然而……什么都没有，在婚礼前两天的时候，我整整催了三十多次，才发了个当天时间安排给我。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@微笑：婚期五个月前就定了，眼看婚礼就四十几天了，婚庆公司一点动静也没有，每次都是我联系他，他说忙，第二天联系我，然后第二天又没有联系，还要我去联系他，什么鬼啊？很想发火了！</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">甚至有妹子在婚庆套路后忍无可忍，婚前40天宁可牺牲意向金也要换婚庆。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">@KCc：筹备婚礼初期遇到了不靠谱婚庆公司，被套路了，套路1：被拖到离婚礼还有两个月的时间才看到一个所谓的“婚礼全案”。套路2：给的两版方案严重超预算，基础版方案超70%，升级版方案超110%，还好没有超200%，是不是算手下留情了。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">沟通几次无果，期间也纠结了两周左右时间，曾考虑是否要妥协，两眼一摸黑，把报价单给签了，可是想到这个团队整个过程中如此不专业且敷衍的态度，给付意向金前后承诺的东西完全对不上号，这样的团队实在无法让自己放心把婚礼托付于他们，最终只能忍痛舍弃意向金更换了婚庆。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">并且在昨日，小编看到一篇来自《北京晨报》的报道——《支付800元聘请化妆师，婚庆公司花50元找来外包人员》。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">一看标题小编就知道，又有妹子被不良婚庆公司给坑了，小编今天就拿报道中的一些案例，为你们盘点不良婚庆公司的“七宗罪”。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">希望你把这篇分享给同样在备婚的准新娘们。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>外聘人员需谨慎</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">吴女士在婚礼当天还遭遇了“魔鬼化妆师”。吴女士说，结婚当天，婚庆公司带来了“专业”化妆师，来给她化妆。本以为化妆师会让她更加漂亮，睁开眼却吓了她一跳。“我当时睁眼后都要崩溃了，绿色和蓝色相间的眼影，还有一个血红血红的嘴唇，真的很土，还不如我自己化的。”吴女士称自己和化妆师沟通后才知道，她出了800元请化妆师，但是这名化妆师是婚庆公司花了50元找来的外包人员。“我当时气得都不知道说什么好了，只能把脸洗了自己重新化妆。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"800元请的跟妆师，居然只值50块？\" src=\"http://www.520iwed.com/uploadfile/2017/0919/20170919093731313.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 633px; height: 420px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">一定要提前咨询哪些人员是外聘的，有些公司很可能部门不齐全，只有策划师、执行、业务人员，其余人员都需要临时从外面聘请，婚庆公司其中一块收入就是赚取这些人员的差价。更离谱的公司是策划、执行都是一个人，刚拉完业务就要去采购，再从外面聘请司仪、摄影摄像，效果可想而知。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">小编建议你们自己另外请四大金刚，很多独立的四大金刚工作室都发展得很成熟，这样不仅避免了婚庆公司从中赚差价，还能选到自己喜欢风格的司仪、摄影摄像。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>个性化婚礼却都是亲兄弟</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">准备在假期举办婚礼的林先生说，近一个月已经去了多家婚庆公司，但发现他们的方案都差不多，越来越多雷同的婚礼被照搬，“几家婚庆公司给的方案都雷同，换汤不换药，后来找了懂行的朋友一问，原来几家公司的灯光和舞台搭建都是承包给同一家公司做的，怪不得看着都像是兄弟姐妹，长得差不多。这样一来，所谓个性化婚礼，根本就是无稽之谈。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">优秀的婚庆公司，会在和新人的交流过程中，观察新人的喜好和特点，找到适合新人的婚礼主题，及时给到新人以切实可行的一些婚礼策划及筹备建议。而不是照搬照抄，如果你想要一场属于你们的个性化婚礼，一定要多沟通，有经验的婚庆公司是可以给你创意想法的。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>做盗图党很容易</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">同样是上面的案例，小编还见过盗图党。所谓盗图党就是直接抠掉别人的logo说是自家作品，有些还堂而皇之加上自己logo。不但网上挂出来的案例都是盗图，在实体店里也说这些案例就是他们做的，说地天花乱坠头头是道。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"800元请的跟妆师，居然只值50块？\" src=\"http://www.520iwed.com/uploadfile/2017/0919/20170919093757513.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 636px; height: 419px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">这里放一个抠掉别人logo的案例，去水印真没你想象的复杂。所以不要轻易相信所谓的现场图。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">有原创实力的婚礼设计公司，有着完整的方案设计，像现在婚庆公司一般都会给你手绘或者电脑做出的现场效果图。但有些坑爹公司连效果图也能盗来，所以，可以提出看婚庆公司工作人员朋友圈中的婚礼视频，更真实的看到他们的作品效果。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>套餐 = 套路？</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">小强向记者感叹，自己被婚庆公司坑得很惨。“当时合同中只写明了‘背景布置’四个字和5000元套餐价格，没有写清楚背景里面都包括什么。我当时想当然地认为，这个5000元就是我们图片上的布置内容，包括鲜花、气球等物品，老板也说可以做成这种效果。可是，等到合同签好了，老板才跟我们说这个价格的背景就只是一块红色的布和上面的飘纱，如果想要加灯光、气球鲜花等装饰，就得加钱，而且还不便宜。为了达到效果，我又多花了3900元。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">一般婚庆公司给的场地布置的套餐费用看起来都是很划算的，因为可以反复利用很多布景、签到台等材料。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">但现在用的最多的则是定制+套餐的模式，会先给你一个套餐，然后会和你说这个套餐对于你的婚礼是不够的，为了更好的效果，可能你要在灯光上面加射灯，你要加一些什么什么，来让你选择。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">有很多姑娘很纠结，我到底是不是真的要加这些材料，那么这时你要做的就是问的更细致一些，让他给你看两种案例，一种是加这个材料和不加这个材料的效果，主要的区别在哪里？主要用在哪里？让自己知道这钱到底花的值不值得。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>策划和现场大不同</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">周女士说，布置当天呈现出的花门、花亭和策划时看的照片完全都不一样。“原本计划的是圆形花门，上面飘丝带。但实物出来就是一个塑料门，上面摆了一些塑料花束夹杂着几根鲜花，几根包装用的彩带用胶条贴在上面，显得特别廉价。我和爱人都非常着急，找到婚庆公司负责人让他们马上整改。但是，这名负责人却说，合同上只写明了花门花亭的价格，没有写样式，他没办法改。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"800元请的跟妆师，居然只值50块？\" src=\"http://www.520iwed.com/uploadfile/2017/0919/20170919093810693.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 639px; height: 366px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">周女士十分无奈，“当时签合同时，婚庆公司老板说样式没办法写在合同上，请我们放心，绝对不会出错。结果婚礼当天看到的结果竟然是假花夹杂着鲜花。如果使用假花，我们婚礼的费用会减少将近7000元。虽然我们很气愤，但是没办法，这婚还得结啊。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">小编想和你们想和你们强调一点，合同是唯一可以保障你们权益的凭证。无论婚庆公司签合同前如何服务周到贴心，也不要对细节有所松懈。小编建议，在签合同时要有示例图片为证或在合同中明确鲜花数量、面积等细节☟</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">花艺是真花还是假花？真花花量多少？是进口花还是国产？背景板是泡雕还是kt板材还是雪弗板还是喷绘？舞台是镜面板还是镜面地毯？搭建是龙门架还是普通桁架？布艺是普通布还是绒布？灯光是染色灯par灯摇头灯还是光束灯？可能搞花样的地方太多。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">合同写清楚，你再放心的去准备别的事情。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>折扣！折扣！</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">许多商家会经常做折扣活动，赶上一些大促活动也是“乱花渐欲迷人眼”，小编并不是说做折扣的一定是不好的，而是你要在其中“货比三家”，多看细节，多比较，多询问，不要为了贪小便宜吃大亏。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\"><strong>小钱也要算计你</strong></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">“婚庆推荐的摄像师很健谈，我们看了他拿出来以前拍的成品感觉也很好，而且双机位的价格相当划算，所以当场就敲定下来了。没想到等到取光盘的时候，婚庆告诉我们后期制作刻光盘要加800块钱才能给我们。虽然结婚大钱都花了那么多，800块钱也不算特别多，但让我们心里多少有点被人算计的感觉，总觉得不舒服。”</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">结婚本来就是一件烧钱事，无良商家也是利用了新人的这个心理，让很多新人吃了哑巴亏，所以小编要再次强调：合！同！写！清！楚！除了合同上的钱，还有哪些是需要额外付款的，别大婚当天高兴过头了，让这些“小钱”变“巨款”。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">最后小编想说，选一家靠谱的婚庆公司太重要了，如果新人有预算，还是找口碑比较好的中大型婚庆策划公司，这些公司价格虽然高，但最起码有保证。如果预算不够多，那就要在合同上下足功夫，一时的仔细，会为你的婚礼省心不少。还是那句话，合同是唯一能够保障你们权益的凭证。</div>', '1', 1602257841, 1602257841);
INSERT INTO `fa_sdcmarry_post` (`id`, `category`, `title`, `main_image`, `description`, `author`, `content`, `is_post`, `createtime`, `updatetime`) VALUES
(6, 'news', '中日婚姻观大不同:中国看重车房 日女性重价值观', '/uploads/20201009/ff8d87fabf6805571f76f00bc5d64017.jpg', '日媒称，在中国达到适婚年龄即20岁左右时，基本都会马上结婚生子。中国人比日本人还要重视家庭，不过在结婚生子方面，中国人也饱受来自“四面八方”的压力，因为中国男性在结婚时要有房和车，这是“必须条件”，而日本女性更重视的是男性的卫生和价值观。', 'DanceLynx', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">日媒称，在中国达到适婚年龄即20岁左右时，基本都会马上结婚生子。中国人比日本人还要重视家庭，不过在结婚生子方面，中国人也饱受来自“四面八方”的压力，因为中国男性在结婚时要有房和车，这是“必须条件”，而日本女性更重视的是男性的卫生和价值观。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"中日婚姻观大不同:中国看重车房 日女性重价值观\" src=\"http://www.520iwed.com/uploadfile/2017/0829/20170829100832886.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 324px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据日本research网站报道，中日男女在交往时所重视的东西大有不同。当下，日本社会过度追求物质享受的风气已“偃旗息鼓”。由于经济不景气，在这种环境下成长的日本年轻人并不过分追求物质，买车的话则首选耗油量较小的小型车，买房也选择贷款。一般来说，年轻人不会在车房方面向家里提出购买要求。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">日媒称，在中国，结婚时男方要有房车，这是“必须条件”。也就是说，经济能力是女性结婚时非常看重的条件，时常比男性的性格或者双方是否拥有共同价值观等方面还要重要。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">但是日本女性重视的东西和中国女性大不相同。比起男性到底拥有多少财产，日本女性更重视的是男性的卫生和价值观，比如男性应每天整理发型和胡须。虽然日本女性也认为稳定的收入很重要，但是对于一个日本职场的女性来说，“人生价值观、性格和金钱观念同自己比较契合才是最重要的。”两人有共同语言，生活才能安定。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"中日婚姻观大不同:中国看重车房 日女性重价值观\" src=\"http://www.520iwed.com/uploadfile/2017/0829/20170829100812206.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 480px; height: 314px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据日本CCC网站的一个调查结果显示，当日本女性动结婚想法时主要考虑三个因素。第1，时机成熟（33.6%）；第2，年龄不饶人（10.2%）；第3，非他莫属（9.5%）。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"中日婚姻观大不同:中国看重车房 日女性重价值观\" src=\"http://www.520iwed.com/uploadfile/2017/0829/20170829100920412.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 480px; height: 214px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\">结婚条件前3名分别是：性格、价值观、脾气好。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">日媒称，确实日本男性注重自身卫生的意识比中国男性高，但也并不是说中国男性不卫生。但是中国男性结婚时要确保拥有房和车，一般婚后还要还高额贷款，生活并不那么轻松。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"中日婚姻观大不同:中国看重车房 日女性重价值观\" src=\"http://www.520iwed.com/uploadfile/2017/0829/20170829100937544.png\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 491px; height: 301px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\">数据来源：中国产业信息网站</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据此前数据调查显示，2016年中国女性择偶收入要求占比为：67.06%的单身女性要求男性收入在5000-10000元，25.02%的单身女性要求男性收入过万元。相比之下，男性对于女性的收入要求普遍较低，80%的单身男性表示对方收入低于5000元也可以接受。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据此前报道，某知名婚恋网站发布中国人婚恋状况调查报告，这份收集了7万余份样本的调查报告显示，独处时间太长以及圈子太小，是单身主要原因。报告显示，71.8%的女性认为男性有房才适合结婚，需要有车的比例为17.8%。男性关心前三位的是外在相貌、身体健康、情感经历，女性则更看重对方的经济条件、身体健康和职业。</div>', '1', 1602257969, 1602257969),
(7, 'news', '移动互联网给85后婚礼带来了哪些改变', '/uploads/20201009/b1f80c0e7260e037d2cee61ff107eeef.jpg', '结婚季来袭，85后的年轻人们，结婚时最流行什么? 记者调查并采访了北京地区的100对新人，调查发现随着互联网技术及文化的发展，请柬、礼金、婚礼形式方面显现出新技术、新手段、新途径的趋势。其中，移动支付产品因其便捷性和潮流性备受推崇，成为现象级“婚礼利器”。', 'DanceLynx', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">结婚季来袭，85后的年轻人们，结婚时最流行什么? 记者调查并采访了北京地区的100对新人，调查发现随着互联网技术及文化的发展，请柬、礼金、婚礼形式方面显现出新技术、新手段、新途径的趋势。其中，移动支付产品因其便捷性和潮流性备受推崇，成为现象级“婚礼利器”。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"移动互联网给85后婚礼带来了哪些改变\" src=\"http://www.520iwed.com/uploadfile/2016/1208/20161208105305718.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 672px; height: 423px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">现象一：超八成新人采用微信电子请柬 快递伴手礼渐成新时尚</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">相较于过去的纸质版大红色请柬，承载内容全、个性色彩浓的iWed婚礼网<a href=\"http://www.520iwed.com/shoujiqingjian/\" target=\"_blank\" style=\"transition: color 0.3s ease-in-out 0s; color: rgb(255, 102, 0); position: relative; padding-bottom: 2px;\">微信请柬</a>成为婚礼请柬的“新宠”。调查数据显示，100对新人中，83对新人选择了电子请柬和纸质版两种版请柬形式，13对新人只寄出请柬，其余被访新人则选择寄出喜糖或伴手礼等实物请柬。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">(婚礼请柬形式发生改变)</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">除请柬外，随请柬一起寄出伴手礼也逐渐成为新时尚，伴手礼也不局限于当地特产，而引申为精心为亲友准备的礼物。随婚礼请柬寄出的伴手礼包括喜糖、当地特产、永生花等有婚礼特色的喜庆物品。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">现象二：“份子钱”通过移动支付转账 礼金也能去理财</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">说到婚礼就不得不提“份子钱”，来宾包个红包并在名册上登记以象征给新人带去祝福，不同城市对礼金金额也有“约定俗成”的标准。而随着移动支付的发展， “份子钱”的接、收方式正在发生改变。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">在采访中记者发现，100对被访新人中，64对新人都收到过通过线上转账而来的“份子钱”。而提到希望通过哪种方式收到礼金时，21%的新人提到社交属性更强的微信，22%的新人仍然会根据习惯选择支付宝，32%的新人希望收到现金，1%的新人希望银行转账或其他形式。显然，移动支付已经成为“份子钱”往来的主力。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">(不同礼金收入方式在新人心中的认可度)</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">现象三：婚礼直播陡然兴起 消费理念再度升级</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">在全民直播的当下，明星婚礼直播也提供了一种创新思路。记者发现，司仪兼职主播已经悄然成风，甚至有新人利用打赏功能再创造一笔收入。婚礼直播不仅可以弥补无法到场的亲友观看婚礼仪式的遗憾，直播的回放功能还可以支持他们日后在手机端观看全程，是一种美好的体验和纪念。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">(视频直播APP种类繁多)</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">综上所述，在互联网和年轻群体的双重影响下，和结婚有关的诸多传统消费都有了新的发展。其中最颠覆传统的当属移动支付对“份子钱”的改变，而这也是85后、90后婚礼相对于老一辈最大的不同之处。此外，新技术、新形式的发展也促进婚礼向科技范发展，而互联网带来的便捷同时影响新人们的婚礼消费。</div>', '1', 1602258021, 1602258021),
(8, 'news', '婚礼舞台架突然倒塌 安全意识低酿悲剧', '/uploads/20201009/c09c430c465f97531f581113611b2899.jpg', '不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。', 'DanceLynx', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">不少人都希望能有一个难忘、梦幻的婚礼，11月12日晚上无锡一酒店宴会厅，有一场盛大的婚礼，晚上八点半左右，婚礼接近尾声，宾客们也都悉数离场，婚庆公司的工作人员正在拆卸各类装饰工具，清理现场，可就在这时，婚礼主舞台上方装载气球、照明灯光和led大屏的钢架突然倒塌。</span></p><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"婚礼舞台架突然倒塌 安全意识低酿悲剧\" src=\"http://www.520iwed.com/uploadfile/2016/1114/20161114104447357.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 359px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">作为新郎新娘至亲入座的主桌位于离舞台最近的区域，恰巧在这个钢架正下方，坐在主桌上的7人因躲闪不及被倒下的钢架砸中，由于钢架非常重，待公安、消防、安检、120急救等部门赶到现场后，伤者才被救出送往医院，据了解有3名伤者被送往了无锡市人民医院，其余4名则被送至第四人民医院。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">随后，记者来到四院，此时几名伤者正在急诊室内接受治疗，其中两名伤者头部受伤，不过幸运的是没有直接砸中头部，都是被架子蹭到。据一名伤者说，他所坐的位置是背对着舞台的，因此并不知道发生了何事，听到有人发出尖叫抬起头，才发现架子倒下来砸在面前的餐桌上，而自己也被蹭到受了伤。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px; text-align: center;\"><img alt=\"婚礼舞台架突然倒塌 安全意识低酿悲剧\" src=\"http://www.520iwed.com/uploadfile/2016/1114/20161114104550462.jpg\" style=\"display: block; max-width: 100%; margin: 10px auto; width: 500px; height: 373px;\"></div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">伤者的家属小明（化名）告诉记者，当晚自己和妈妈，外婆，外公四人一起作为新娘的亲眷坐在了主桌，当时婚礼接近尾声，婚庆公司在收拾舞台设备时不慎发生了事故，当时因为自己和外公坐的离舞台稍远的位置因此未被钢架砸中，而母亲和外婆却没能逃过一劫，新娘也因此事件当场情绪失控。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">据了解，此次事故共造成七人受伤，其中三人骨折，其余四人均为皮外伤，所幸七人都无生命危险，事故具体原因正在进一步调查中。记者从医院了解到，事故发生后有多人受伤，伤势最重的被砸骨折，大多数人是皮外伤，并无生命危险。目前，事故原因仍在调查中。</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">&nbsp;</div><div style=\"padding: 0px; margin: 0px; vertical-align: baseline; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, tahoma, arial, 宋体; font-size: 18px;\">婚礼执行过程中的安全问题，一直都牵动着婚礼人的神经，特别是使用了众多器材设备的大型婚礼中，稍微出点马虎大意，都会酿出不可挽回的损失。对此我们希望婚庆公司一定要有安全意识。酒店与婚庆要各司其职，安全问题，重于泰山！</div>', '1', 1602258063, 1602258063);

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_service`
--

CREATE TABLE `fa_sdcmarry_service` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `category` varchar(30) NOT NULL COMMENT '服务分类',
  `title` varchar(120) NOT NULL COMMENT '标题',
  `desc_images` varchar(191) NOT NULL COMMENT '多图介绍',
  `content` text NOT NULL COMMENT '内容',
  `description` varchar(191) NOT NULL COMMENT '简介',
  `status` enum('0','1') NOT NULL COMMENT '状态:0=隐藏,1=正常',
  `main_image` varchar(191) NOT NULL COMMENT '主图',
  `old_price` decimal(8,2) UNSIGNED NOT NULL COMMENT '原价',
  `new_price` decimal(8,2) UNSIGNED NOT NULL COMMENT '现价',
  `createtime` int(10) UNSIGNED NOT NULL COMMENT '创建时间',
  `updatetime` int(10) UNSIGNED NOT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务';

--
-- Dumping data for table `fa_sdcmarry_service`
--

INSERT INTO `fa_sdcmarry_service` (`id`, `category`, `title`, `desc_images`, `content`, `description`, `status`, `main_image`, `old_price`, `new_price`, `createtime`, `updatetime`) VALUES
(1, 'main', '爱的承诺-预约大礼包', '/uploads/20201009/8890254019c5c3b8c9e9c99ca4269dbc.jpg', '<p>123</p>', '场地布置、主持人、蛋糕婚车10辆', '1', '/uploads/20201009/8890254019c5c3b8c9e9c99ca4269dbc.jpg', '2600.00', '2000.00', 1602212826, 1602212837),
(2, 'main', '教堂婚礼套餐', '/uploads/20201009/890dda29f738474d23f8a8ce10b2b056.jpg', '<p>123</p>', '场地布置、主持人、蛋糕婚车10辆', '1', '/uploads/20201009/890dda29f738474d23f8a8ce10b2b056.jpg', '2000.00', '1600.00', 1602212925, 1602212925),
(3, 'main', '豪华预约服务', '/uploads/20201009/fca931712e97cef40e68a99ffa142f80.jpg', '<p>123</p>', '场地布置、主持人、蛋糕婚车10辆', '1', '/uploads/20201009/fca931712e97cef40e68a99ffa142f80.jpg', '3888.00', '1888.00', 1602212994, 1602212994);

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_tab`
--

CREATE TABLE `fa_sdcmarry_tab` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Id',
  `category` varchar(30) NOT NULL COMMENT '分类',
  `title` varchar(40) NOT NULL COMMENT '标题',
  `image` varchar(191) NOT NULL COMMENT '图标',
  `path` varchar(191) NOT NULL COMMENT '路径',
  `options` varchar(40) NOT NULL COMMENT '附加选项'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='标签导航';

--
-- Dumping data for table `fa_sdcmarry_tab`
--

INSERT INTO `fa_sdcmarry_tab` (`id`, `category`, `title`, `image`, `path`, `options`) VALUES
(5, 'home', '摄像拍照', '/uploads/20201006/69653bb996a590296507e4e1c45c9b5a.png', '', ''),
(6, 'home', '现场布置', '/uploads/20201006/12fa23594787473531b746cd26af98a9.png', '', ''),
(7, 'home', '婚车服饰', '/uploads/20201006/bbf11d4a95d99120ad0ca6c04ae431fd.png', '', ''),
(8, 'home', '婚礼服装', '/uploads/20201006/6be7a0b21482725b247ac7d0eaf07dce.png', '', ''),
(9, 'home', '最新动态', '/uploads/20201006/aeff78cc2662833f583ec84ee2644afe.png', '', ''),
(10, 'home', '服务预约', '/uploads/20201006/bc833438bebd09b84aad4d873f3caaaf.png', '', ''),
(11, 'home', '留言咨询', '/uploads/20201006/94a7f5ff63784391ab5e90572c26051a.png', '', ''),
(12, 'home', '联系我们', '/uploads/20201006/08aa32ce64e40aee2987c226d67034ad.png', '/pages/about/index', ''),
(13, 'post', '婚姻百科', '', '', 'hybk'),
(14, 'post', '结婚攻略', '', '', 'news'),
(15, 'example', '婚礼案例', '', '', 'marry'),
(16, 'example', '现场案例', '', '', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `fa_sdcmarry_third`
--

CREATE TABLE `fa_sdcmarry_third` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '会员ID',
  `platform` varchar(30) DEFAULT '' COMMENT '第三方应用',
  `unionid` varchar(100) DEFAULT '' COMMENT '第三方UNIONID',
  `openid` varchar(100) DEFAULT '' COMMENT '第三方OPENID',
  `openname` varchar(100) DEFAULT '' COMMENT '第三方会员昵称',
  `access_token` varchar(255) DEFAULT '' COMMENT 'AccessToken',
  `refresh_token` varchar(255) DEFAULT 'RefreshToken',
  `expires_in` int(10) UNSIGNED DEFAULT '0' COMMENT '有效期',
  `createtime` int(10) UNSIGNED DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) UNSIGNED DEFAULT NULL COMMENT '更新时间',
  `logintime` int(10) UNSIGNED DEFAULT NULL COMMENT '登录时间',
  `expiretime` int(10) UNSIGNED DEFAULT NULL COMMENT '过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方登录表';

--
-- Dumping data for table `fa_sdcmarry_third`
--

INSERT INTO `fa_sdcmarry_third` (`id`, `user_id`, `platform`, `unionid`, `openid`, `openname`, `access_token`, `refresh_token`, `expires_in`, `createtime`, `updatetime`, `logintime`, `expiretime`) VALUES
(4, 6, 'mp', '', 'olW2X5G0u8tHLDISIz0LvlsGLboo', 'summer', '38_XZgv8knivX-gEngYtv0XRvl8QSyMfJTSc5Of06hEb_zp1XiwQgIn_RdhS5GG3ZKYkNDdWCxdvF46w-zZc81h24pepVpK-wixvhbrRf2NKwjmNlUEwUkN8J1P7YwBYlsd8M5neF_H1F4vqN7sTOTgADAEZE', '', 7200, 1602778732, 1602905297, 1602905297, 1602912497);

-- --------------------------------------------------------

--
-- Table structure for table `fa_sms`
--

CREATE TABLE `fa_sms` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) UNSIGNED DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='短信验证码表';

-- --------------------------------------------------------

--
-- Table structure for table `fa_test`
--

CREATE TABLE `fa_test` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '分类ID(单选)',
  `category_ids` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `images` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '省市',
  `json` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `price` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '价格',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '点击',
  `startdate` date DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year(4) DEFAULT NULL COMMENT '年',
  `times` time DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) DEFAULT NULL COMMENT '刷新时间(int)',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='测试表';

--
-- Dumping data for table `fa_test`
--

INSERT INTO `fa_test` (`id`, `admin_id`, `category_id`, `category_ids`, `week`, `flag`, `genderdata`, `hobbydata`, `title`, `content`, `image`, `images`, `attachfile`, `keywords`, `description`, `city`, `json`, `price`, `views`, `startdate`, `activitytime`, `year`, `times`, `refreshtime`, `createtime`, `updatetime`, `deletetime`, `weigh`, `switch`, `status`, `state`) VALUES
(1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', '{\"a\":\"1\",\"b\":\"2\"}', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1499682285, 1499682526, 1499682526, NULL, 0, 1, 'normal', '1');

-- --------------------------------------------------------

--
-- Table structure for table `fa_third`
--

CREATE TABLE `fa_third` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '会员ID',
  `platform` varchar(30) DEFAULT '' COMMENT '第三方应用',
  `unionid` varchar(100) DEFAULT '' COMMENT '第三方UNIONID',
  `openid` varchar(100) DEFAULT '' COMMENT '第三方OPENID',
  `openname` varchar(100) DEFAULT '' COMMENT '第三方会员昵称',
  `access_token` varchar(255) DEFAULT '' COMMENT 'AccessToken',
  `refresh_token` varchar(255) DEFAULT 'RefreshToken',
  `expires_in` int(10) UNSIGNED DEFAULT '0' COMMENT '有效期',
  `createtime` int(10) UNSIGNED DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) UNSIGNED DEFAULT NULL COMMENT '更新时间',
  `logintime` int(10) UNSIGNED DEFAULT NULL COMMENT '登录时间',
  `expiretime` int(10) UNSIGNED DEFAULT NULL COMMENT '过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方登录表';

--
-- Dumping data for table `fa_third`
--

INSERT INTO `fa_third` (`id`, `user_id`, `platform`, `unionid`, `openid`, `openname`, `access_token`, `refresh_token`, `expires_in`, `createtime`, `updatetime`, `logintime`, `expiretime`) VALUES
(1, 2, 'mp', '', 'olW2X5G0u8tHLDISIz0LvlsGLboo', 'summer', '38_TcmHPKJ1xqmsQ3bB0xpEElc_wxqTvgU33LX_Vg9T6v-OA6ONt-q-UBqiflQqrfan3SyN7VSvbiRAFJ_nlTe6_73zBkS7p8dBGuqRcas9CiHgF4dTsSFavsfJ5YkUMCbg50wnIPWbzHYXNdq8NAIcAFAREM', '', 7200, 1602734941, 1602734941, 1602734941, 1602742141);

-- --------------------------------------------------------

--
-- Table structure for table `fa_user`
--

CREATE TABLE `fa_user` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Token',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  `verification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员表';

--
-- Dumping data for table `fa_user`
--

INSERT INTO `fa_user` (`id`, `group_id`, `username`, `nickname`, `password`, `salt`, `email`, `mobile`, `avatar`, `level`, `gender`, `birthday`, `bio`, `money`, `score`, `successions`, `maxsuccessions`, `prevtime`, `logintime`, `loginip`, `loginfailure`, `joinip`, `jointime`, `createtime`, `updatetime`, `token`, `status`, `verification`) VALUES
(6, 0, '1pEXeQybfl', 'summer', '57460bd2aedf1f62f0c82f180d11ee71', 'fIenPq', '1pEXeQybfl@hunqing.sdc-dev.top', '15500785170', 'https://thirdwx.qlogo.cn/mmopen/vi_32/ricV7H8iayA8eFqsPyRjAcdEZnzibbfq44ibPIDWDdo37Aa792N85uaPrZ6refgSMNuJCxAAK1YIztyZWiaIrDSwrug/132', 1, 0, NULL, '', '0.00', 0, 2, 2, 1602904011, 1602905297, '127.0.0.1', 0, '127.0.0.1', 1602778732, 1602778732, 1602905297, '', 'normal', '');

-- --------------------------------------------------------

--
-- Table structure for table `fa_user_group`
--

CREATE TABLE `fa_user_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员组表';

--
-- Dumping data for table `fa_user_group`
--

INSERT INTO `fa_user_group` (`id`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1515386468, 1516168298, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_user_money_log`
--

CREATE TABLE `fa_user_money_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员余额变动表';

-- --------------------------------------------------------

--
-- Table structure for table `fa_user_rule`
--

CREATE TABLE `fa_user_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `remark` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员规则表';

--
-- Dumping data for table `fa_user_rule`
--

INSERT INTO `fa_user_rule` (`id`, `pid`, `name`, `title`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 0, 'index', 'Frontend', '', 1, 1516168079, 1516168079, 1, 'normal'),
(2, 0, 'api', 'API Interface', '', 1, 1516168062, 1516168062, 2, 'normal'),
(3, 1, 'user', 'User Module', '', 1, 1515386221, 1516168103, 12, 'normal'),
(4, 2, 'user', 'User Module', '', 1, 1515386221, 1516168092, 11, 'normal'),
(5, 3, 'index/user/login', 'Login', '', 0, 1515386247, 1515386247, 5, 'normal'),
(6, 3, 'index/user/register', 'Register', '', 0, 1515386262, 1516015236, 7, 'normal'),
(7, 3, 'index/user/index', 'User Center', '', 0, 1516015012, 1516015012, 9, 'normal'),
(8, 3, 'index/user/profile', 'Profile', '', 0, 1516015012, 1516015012, 4, 'normal'),
(9, 4, 'api/user/login', 'Login', '', 0, 1515386247, 1515386247, 6, 'normal'),
(10, 4, 'api/user/register', 'Register', '', 0, 1515386262, 1516015236, 8, 'normal'),
(11, 4, 'api/user/index', 'User Center', '', 0, 1516015012, 1516015012, 10, 'normal'),
(12, 4, 'api/user/profile', 'Profile', '', 0, 1516015012, 1516015012, 3, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fa_user_score_log`
--

CREATE TABLE `fa_user_score_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT '0' COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT '0' COMMENT '变更后积分',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员积分变动表';

--
-- Dumping data for table `fa_user_score_log`
--

INSERT INTO `fa_user_score_log` (`id`, `user_id`, `score`, `before`, `after`, `memo`, `createtime`) VALUES
(1, 3, 0, 0, 0, '管理员变更积分', 1602777034),
(2, 3, 0, 0, 0, '管理员变更积分', 1602777046);

-- --------------------------------------------------------

--
-- Table structure for table `fa_user_token`
--

CREATE TABLE `fa_user_token` (
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(10) DEFAULT NULL COMMENT '过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员Token表';

--
-- Dumping data for table `fa_user_token`
--

INSERT INTO `fa_user_token` (`token`, `user_id`, `createtime`, `expiretime`) VALUES
('2bc275af818346dd12eff08270f0436b12da2334', 6, 1602905297, 0),
('35df669c9dbb9d1f9ff7d05c1ab629e838692b41', 6, 1602778859, 0),
('4465f8e2f2258dc7a06de6f71bcaa9116d67e74b', 6, 1602849644, 0),
('489ed94a276db6883acbe582dc75cfcdbcb85527', 6, 1602816144, 0),
('66f389d4c0e6154d972109e004c9ebb9709cb21d', 6, 1602778732, 0),
('8764f7d4bd8822be700871273a3267556071352f', 6, 1602778732, 0),
('99319c1c8cc2cef50ba8c320b42217f3366dd901', 6, 1602904011, 0),
('c32681f273a0728094aa2c006eaad1a23af441d2', 6, 1602812955, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fa_version`
--

CREATE TABLE `fa_version` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `oldversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '包大小',
  `content` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='版本表';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fa_admin`
--
ALTER TABLE `fa_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `fa_admin_log`
--
ALTER TABLE `fa_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`username`);

--
-- Indexes for table `fa_area`
--
ALTER TABLE `fa_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fa_attachment`
--
ALTER TABLE `fa_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_auth_group`
--
ALTER TABLE `fa_auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_auth_group_access`
--
ALTER TABLE `fa_auth_group_access`
  ADD UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `fa_auth_rule`
--
ALTER TABLE `fa_auth_rule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`) USING BTREE,
  ADD KEY `pid` (`pid`),
  ADD KEY `weigh` (`weigh`);

--
-- Indexes for table `fa_category`
--
ALTER TABLE `fa_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `weigh` (`weigh`,`id`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fa_command`
--
ALTER TABLE `fa_command`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `fa_config`
--
ALTER TABLE `fa_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `fa_ems`
--
ALTER TABLE `fa_ems`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `fa_sdcmarry_carousel`
--
ALTER TABLE `fa_sdcmarry_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_example`
--
ALTER TABLE `fa_sdcmarry_example`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_order`
--
ALTER TABLE `fa_sdcmarry_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_post`
--
ALTER TABLE `fa_sdcmarry_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_service`
--
ALTER TABLE `fa_sdcmarry_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_tab`
--
ALTER TABLE `fa_sdcmarry_tab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sdcmarry_third`
--
ALTER TABLE `fa_sdcmarry_third`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `platform` (`platform`,`openid`),
  ADD KEY `user_id` (`user_id`,`platform`),
  ADD KEY `unionid` (`platform`,`unionid`);

--
-- Indexes for table `fa_sms`
--
ALTER TABLE `fa_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_test`
--
ALTER TABLE `fa_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_third`
--
ALTER TABLE `fa_third`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `platform` (`platform`,`openid`),
  ADD KEY `user_id` (`user_id`,`platform`),
  ADD KEY `unionid` (`platform`,`unionid`);

--
-- Indexes for table `fa_user`
--
ALTER TABLE `fa_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `fa_user_group`
--
ALTER TABLE `fa_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_money_log`
--
ALTER TABLE `fa_user_money_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_rule`
--
ALTER TABLE `fa_user_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_score_log`
--
ALTER TABLE `fa_user_score_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_token`
--
ALTER TABLE `fa_user_token`
  ADD PRIMARY KEY (`token`);

--
-- Indexes for table `fa_version`
--
ALTER TABLE `fa_version`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fa_admin`
--
ALTER TABLE `fa_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fa_admin_log`
--
ALTER TABLE `fa_admin_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `fa_area`
--
ALTER TABLE `fa_area`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `fa_attachment`
--
ALTER TABLE `fa_attachment`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `fa_auth_group`
--
ALTER TABLE `fa_auth_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fa_auth_rule`
--
ALTER TABLE `fa_auth_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `fa_category`
--
ALTER TABLE `fa_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `fa_command`
--
ALTER TABLE `fa_command`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `fa_config`
--
ALTER TABLE `fa_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `fa_ems`
--
ALTER TABLE `fa_ems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `fa_sdcmarry_carousel`
--
ALTER TABLE `fa_sdcmarry_carousel`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_example`
--
ALTER TABLE `fa_sdcmarry_example`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_order`
--
ALTER TABLE `fa_sdcmarry_order`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_post`
--
ALTER TABLE `fa_sdcmarry_post`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_service`
--
ALTER TABLE `fa_sdcmarry_service`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_tab`
--
ALTER TABLE `fa_sdcmarry_tab`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `fa_sdcmarry_third`
--
ALTER TABLE `fa_sdcmarry_third`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fa_sms`
--
ALTER TABLE `fa_sms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `fa_test`
--
ALTER TABLE `fa_test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fa_third`
--
ALTER TABLE `fa_third`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fa_user`
--
ALTER TABLE `fa_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fa_user_group`
--
ALTER TABLE `fa_user_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fa_user_money_log`
--
ALTER TABLE `fa_user_money_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fa_user_rule`
--
ALTER TABLE `fa_user_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fa_user_score_log`
--
ALTER TABLE `fa_user_score_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fa_version`
--
ALTER TABLE `fa_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
